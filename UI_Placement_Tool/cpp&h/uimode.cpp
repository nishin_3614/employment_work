// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// UIモード処理 [uimode.cpp]
// Author : KOKI NISHIYAMA
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "uimode.h"
/* 描画 */
#include "fade.h"
#include "ui.h"
#include <algorithm>

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数宣言
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int CUimode::m_nBG_TexType = 53;				// 背景のテクスチャータイプ

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CUimode::CUimode()
{
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CUimode::~CUimode()
{
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 初期化
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUimode::Init(void)
{
	/* 作成 */
	// 背景生成
	m_pBg = CScene_TWO::Create(CScene_TWO::OFFSET_TYPE_CENTER, { SCREEN_WIDTH * 0.5f,SCREEN_HEIGHT * 0.5f,0.0f }, { SCREEN_WIDTH,SCREEN_HEIGHT });
	m_pBg->BindTexture(CTexture_manager::GetTexture(m_nBG_TexType));
	// UI生成
	m_vecUi = std::move(CUi::LoadCreate_Self(0));
	// 選択時の枠線生成
	m_pSelect = std::move(CScene_TWO::Create_Uni(
		CScene_TWO::OFFSET_TYPE_CENTER,
		{ 640.0f,360.0f,0.0f },
		{ 1280.0f,720.0f },
		0.0f,
		D3DXCOLOR(1.0f,0.0f,0.0f,0.2f)
	));
	m_pSelect->BindTexture(NULL);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 終了
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUimode::Uninit(void)
{
	CScene::ReleaseAll();
	// シーンストップ解除
	CScene::SetStop(false);
	// 背景の終了
	if (m_pBg != NULL)
	{
		m_pBg = NULL;
	}
	// UIの終了
	for (int nCntUi = 0; nCntUi < (signed)m_vecUi.size(); nCntUi++)
	{
		m_vecUi[nCntUi]->Uninit();
		m_vecUi[nCntUi].reset();
	}
	// vector型のメモリ開放
	m_vecUi.clear();
	m_vecUi.shrink_to_fit();
	// 選択時の枠線開放
	if (m_pSelect != NULL)
	{
		m_pSelect->Uninit();
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 更新
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUimode::Update(void)
{
	// メインメニュー
	ShowMainMenuBar();
	// 変数宣言
	string sName;
	string sNum;
	ImGuiTabBarFlags flag;

	// UIのパス名
	sName = "UIPoss:";
	sName += CUi::GetString_One();
	// flagの設定
	flag |= ImGuiTabBarFlags_TabListPopupButton;			// タブの一番左端にドロップダウンリストが表示される下向き三角形のクリックエリアを作成し、そこからタブを選択できるようになります。
	flag |= ImGuiTabBarFlags_AutoSelectNewTabs;				// タブを新しく作成した時に自動でそのタブを選択状態にします。
	flag |= ImGuiTabBarFlags_NoCloseWithMiddleMouseButton;	// タブの中でマウス中央ボタンクリックすることでタブを閉じることができる機能を無効にします。
	flag |= ImGuiTabBarFlags_NoTooltip;						// タブ上にマウスオーバーした場合に表示されるタブ名のポップアップ表示を無効にします。
	// UIの更新
	for (int nCntUi = 0; nCntUi < (signed)m_vecUi.size(); nCntUi++)
	{
		m_vecUi[nCntUi]->Update();
	}
	// UI情報の表示開始
	if (ImGui::Begin("UIInfo"))
	{
		// 背景の更新
		if (m_pBg != NULL)
		{
			// テクスチャータイプ情報
			if (ImGui::TreeNode("BG_TextureType"))
			{
				ImGui::InputInt("BG_TextureType", &m_nBG_TexType, 1, CTexture_manager::GetTextureMax() - 1);
				ImGui::TreePop();
				// テクスチャー更新
				m_pBg->BindTexture(CTexture_manager::GetTexture(m_nBG_TexType));
			}
			// 更新
			m_pBg->Update();
		}

		// タブバーの生成
		if (ImGui::BeginTabBar(sName.c_str(), flag))
		{
			// UIの更新
			for (int nCntUi = 0; nCntUi < (signed)m_vecUi.size(); nCntUi++)
			{
				// 番号代入
				sNum = "[" + std::to_string(nCntUi) + "]";
				// タブの生成
				if (ImGui::BeginTabItem(sNum.c_str()))
				{
					// やること
					// 選択されているUIを枠線で囲む
					// 選択しているUI
					CUi::SetSelectUiSelect(nCntUi);
					// デバッグ
					m_vecUi[nCntUi]->Setting();
					// タブ終了
					ImGui::EndTabItem();

					// 選択時の枠線更新
					if (m_pSelect != NULL)
					{
						// 位置設定
						m_pSelect->SetPosition(m_vecUi[nCntUi]->GetPresents()->GetPosition());
						// 位置設定
						m_pSelect->SetSize(m_vecUi[nCntUi]->GetPresents()->GetSize());
						// 頂点座標設定
						m_pSelect->Set_Vtx_Pos();
						// 更新
						m_pSelect->Update();
					}
				}
			}
			// タブバー終了
			ImGui::EndTabBar();
		}
	}
	// UI情報の表示終了
	ImGui::End();
	// すべて保存
	if (CManager::GetKeyboard()->GetKeyboardPress(DIK_LCONTROL) &&
		CManager::GetKeyboard()->GetKeyboardTrigger(DIK_S))
	{
		CUi::Save();
	}
	// 削除
	if (CManager::GetKeyboard()->GetKeyboardTrigger(DIK_DELETE))
	{
		DeleteUi();
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUimode::Draw(void)
{
	// UIの更新
	for (int nCntUi = 0; nCntUi < (signed)m_vecUi.size(); nCntUi++)
	{
		m_vecUi[nCntUi]->Draw();
	}
	// 選択時の枠線描画
	if (m_pSelect != NULL)
	{
		// 描画
		m_pSelect->Draw();
	}
}

#ifdef _DEBUG
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デバッグ表示
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUimode::Debug(void)
{

}
#endif // _DEBUG

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 切り替えUI
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUimode::ChangeUi(int const & nType)
{
	// UIの終了
	for (int nCntUi = 0; nCntUi < (signed)m_vecUi.size(); nCntUi++)
	{
		m_vecUi[nCntUi]->Uninit();
		m_vecUi[nCntUi].reset();
	}
	// vector型のメモリ開放
	m_vecUi.clear();
	m_vecUi.shrink_to_fit();

	/* 作成 */
	m_vecUi = std::move(CUi::LoadCreate_Self(nType));
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// UI削除
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUimode::DeleteUi(int const &nUi)
{
	// 終了処理
	m_vecUi[nUi]->Uninit();
	// 情報の開放
	m_vecUi[nUi].reset();
	m_vecUi.erase(m_vecUi.begin() + nUi);
	m_vecUi.shrink_to_fit();
	// UI番号を整理する
	for (int nCntUi = 0; nCntUi < (signed)m_vecUi.size(); nCntUi++)
	{
		m_vecUi[nCntUi]->SetUi(nCntUi);
	}
	// UI削除
	CUi::DeleteUi(CUi::GetSelectUiType(), nUi);
	// 選択している番号
	CUi::SetSelectUiSelect(-1);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// メインメニュー
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUimode::ShowMainMenuBar()
{
	if (ImGui::BeginMainMenuBar())
	{
		// メインメニューを表示している時の処理をここに書きます。
		if (ImGui::BeginMenu("File"))
		{
			// 新しいファイルを作成する
			if (ImGui::BeginMenu("FileNew"))
			{
				// 変巣宣言
				static char cName[64];
				// ファイル名を入力
				ImGui::InputText("Name", cName, 64);
				// ボタン押したときに入る
				// ->新規ファイルを作成
				if (ImGui::Button("New"))
				{
					// メッセージ表示
					CCalculation::Messanger("ファイル作成します", MB_ICONINFORMATION);
					// 新規ファイル作成
					CUi::FileNew(cName);
					// 切り替えUI
					ChangeUi(CUi::GetUiNumAll() - 1);
					// 名前の初期化
					cName[0] = '\0';
				}
				// メニュー終了
				ImGui::EndMenu();
			}
			// 新しいUIを作成する
			if (ImGui::MenuItem("UiNew"))
			{
				m_vecUi.push_back(std::move(CUi::CreateUi(CUi::GetSelectUiType())));
			}
			// ファイルの選択Windowを表示
			if (ImGui::BeginMenu("OpenFile"))
			{
				// 変数宣言
				vector<string> vec_string = CUi::GetString();	// ファイル名を取得
				// ファイル選択
				// UIリスト
				for (int nCntString = 0; nCntString < (signed)vec_string.size(); nCntString++)
				{
					// ファイル名表示
					if (ImGui::MenuItem(vec_string[nCntString].c_str()))
					{
						// 使用状態ならループスキップ
						if (CUi::GetUse(nCntString))
						{
							continue;
						}
						// 切り替えUI
						ChangeUi(nCntString);
					}
				}
				// メニュー終了
				ImGui::EndMenu();
			}
			/*
			// ファイルの選択Windowを表示
			if (ImGui::BeginMenu("DeleteFile"))
			{
				// 変数宣言
				vector<string> vec_string = CUi::GetString();	// ファイル名を取得
				// ファイル選択
				// UIリスト
				for (int nCntString = 0; nCntString < (signed)vec_string.size(); nCntString++)
				{
					// ファイル名表示
					if (ImGui::MenuItem(vec_string[nCntString].c_str()))
					{
						// 使用状態ならループスキップ
						if (CUi::GetUse(nCntString))
						{
							// ファイル削除
							CUi::FileDelete(nCntString);
							// サイズ内に押された番号が入っていれば
							// ->UIを切り替える
							if (nCntString < (signed)CUi::GetString().size())
							{
								// 切り替えUI
								ChangeUi(nCntString);
							}
							else
							{
								if (nCntString - 1 >= 0)
								{
									// 切り替えUI
									ChangeUi(nCntString - 1);
								}
							}
						}
						// 使用状態以外のものなら
						// ->ファイル削除
						else
						{
							CUi::FileDelete(nCntString);
						}
					}
				}
				// メニュー終了
				ImGui::EndMenu();
			}
			*/
			// 選択しているUIを消す
			if (ImGui::MenuItem("DeleteUi"))
			{
				DeleteUi();
			}
			// すべて保存
			if (ImGui::MenuItem("Save", "Ctrl+S"))
			{
				CUi::Save();
			}
			// ラインを引く
			ImGui::Separator();
			// 終了する
			if (ImGui::MenuItem("Quit", "ESC"))
			{
				SetDestWind(true);
			}
			// メニュー終了
			ImGui::EndMenu();
		}
		// アニメーション
		if (ImGui::BeginMenu("Animation"))
		{
			// フェードアウトスタート
			if (ImGui::Button("FadeOutStart"))
			{
				for (int nCntUi = 0; nCntUi < (signed)m_vecUi.size(); nCntUi++)
				{
					m_vecUi[nCntUi]->GetPresents()->Start_FadeOut();
				}
			}
			// フェードインスタート
			if (ImGui::Button("FadeInStart"))
			{
				for (int nCntUi = 0; nCntUi < (signed)m_vecUi.size(); nCntUi++)
				{
					m_vecUi[nCntUi]->GetPresents()->Start_FadeIn();
				}
			}
			// 強調スタート
			if (ImGui::Button("CooperationStart"))
			{
				for (int nCntUi = 0; nCntUi < (signed)m_vecUi.size(); nCntUi++)
				{
					m_vecUi[nCntUi]->GetPresents()->Start_Cooperation();
				}
			}
			// メニュー終了
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}
}