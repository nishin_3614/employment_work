// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// ユーザーインターフェース処理 [ui.cpp]
// Author : KOKI NISHIYAMA
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "ui.h"
/* 描画 */
#include "fade.h"
#include "2Dpresents.h"
#include "rectcollision.h"
#include <iomanip>

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#define UI_MANAGER_FILE "data/LOAD/UI/ui_manager.txt"	// UIマネージャーファイルのパス

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数宣言
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vector<vector<CUi::UI_LOAD>> CUi::m_vec_UiLoad = {};	// Ui読み込み用変数
vector<bool> CUi::m_sta_UiUse = {};						// このUIの使用状態
vector<string> CUi::m_vec_String = {};					// ファイル情報読み書き用
int CUi::m_nUitype = 0;									// 選択中のUIスクリプト
int CUi::m_nUiSelect = 0;								// UIの選択

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CUi::CUi()
{
	m_pPresents = NULL;			// 2Dプレゼンツ
	m_nType = 0;				// UIタイプ
	m_nUi = 0;					// UI
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CUi::~CUi()
{
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 初期化
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::Init(void)
{
	// プレゼン情報作成
	m_pPresents = C2DPresents::Create_Self(
		(CScene_TWO::OFFSET_TYPE)m_vec_UiLoad[m_nType].at(m_nUi).nOffsetType,
		m_vec_UiLoad[m_nType].at(m_nUi).pos,
		m_vec_UiLoad[m_nType].at(m_nUi).size,
		m_vec_UiLoad[m_nType].at(m_nUi).fRot,
		m_vec_UiLoad[m_nType].at(m_nUi).col
	);
	// テクスチャー座標設定
	m_pPresents->SetTex(m_vec_UiLoad[m_nType].at(m_nUi).tex_first, m_vec_UiLoad[m_nType].at(m_nUi).tex_last);
	// テクスチャー設定
	m_pPresents->BindTexture(CTexture_manager::GetTexture(m_vec_UiLoad[m_nType].at(m_nUi).nTexType));
	// 当たり判定をつける
	p2DRectShape = std::move(C2DRectShape::Create(
		D3DXVECTOR2(m_pPresents->GetSize().x, m_pPresents->GetSize().y),
		m_pPresents->GetPosition()
	));
	// 初期位置設定
	m_pPresents->OriginPos(m_vec_UiLoad[m_nType][m_nUi].pos);
	// 初期色設定
	m_pPresents->OriginCol(m_vec_UiLoad[m_nType][m_nUi].col);
	// 初期サイズ設定
	m_pPresents->OriginSize(m_vec_UiLoad[m_nType][m_nUi].size);

	// 2Dプレゼンツがヌルなら関数を抜ける
	if (m_vec_UiLoad[m_nType].at(m_nUi).bPresents == false)
	{
		return;
	}
	// 強調情報がヌルではない場合
	if (m_vec_UiLoad[m_nType].at(m_nUi).Presents.bCooperation == true)
	{
		// 色情報がヌルではない場合
		if (m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.bCol == true)
		{
			// 色の設定
			m_pPresents->SetCooperation(
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Col.changecol,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Col.nTimeSwitch,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Col.nTimeChange
			);
		}
		// 拡大率情報がヌルではない場合
		if (m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.bScal == true)
		{
			// 拡大率の設定
			m_pPresents->SetCooperation(
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Scal.fChangeScal,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Scal.nTimeChange
			);
		}
	}
	// フェードイン情報がヌルではない場合
	if (m_vec_UiLoad[m_nType].at(m_nUi).Presents.bFadeIn == true)
	{
		// フェードイン(位置指定)情報がヌルではない場合
		if (m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.bPos == true)
		{
			// フェードイン(位置指定)の設定
			m_pPresents->SetFadeIn(
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadePos.BeginPos,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadePos.EndPos,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadePos.nMaxfram
			);
		}
		// フェードイン(色指定)情報がヌルではない場合
		if (m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.bCol == true)
		{
			// フェードイン(色指定)の設定
			m_pPresents->SetFadeIn(
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadeCol.BeginCol,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadeCol.EndCol,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadeCol.nMaxfram
			);
		}
		// フェードイン強制終了情報設定
		m_pPresents->SetFadeIn_Compulsion(m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.bSkip);
		// フェードイン(スタート時間指定)の設定
		m_pPresents->SetFadeIn_StarAnim(
			m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadeStart
		);
	}
	// フェードアウト情報がヌルではない場合
	if (m_vec_UiLoad[m_nType].at(m_nUi).Presents.bFadeOut == true)
	{
		// フェードアウト(位置指定)情報がヌルではない場合
		if (m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.bPos == true)
		{
			// フェードアウト(位置指定)の設定
			m_pPresents->SetFadeOut(
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadePos.BeginPos,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadePos.EndPos,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadePos.nMaxfram
			);
		}
		// フェードアウト(色指定)情報がヌルではない場合
		if (m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.bCol == true)
		{
			// フェードアウト(色指定)の設定
			m_pPresents->SetFadeOut(
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadeCol.BeginCol,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadeCol.EndCol,
				m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadeCol.nMaxfram
			);
		}
		// フェードアウト強制終了情報設定
		m_pPresents->SetFadeOut_Compulsion(m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.bSkip);
		// フェードアウト(スタート時間指定)の設定
		m_pPresents->SetFadeOut_StarAnim(
			m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadeStart
		);
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 終了
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::Uninit(void)
{
	// プレゼン情報がヌルではない場合
	if (m_pPresents != NULL)
	{
		// 終了処理
		m_pPresents->Uninit();
		// メモリ開放
		delete m_pPresents;
		m_pPresents = NULL;
	}
	// 使用状態がtrueであればfalseに
	if (m_sta_UiUse[m_nType])
	{
		m_sta_UiUse[m_nType] = false;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 更新
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::Update(void)
{
	// プレゼン情報がヌルではない場合
	if (m_pPresents != NULL)
	{
		// 更新処理
		m_pPresents->Update();
		// 当たり判定のサイズの設定
		p2DRectShape->SetSize(m_pPresents->GetSize());
		// 当たり判定の位置設定
		p2DRectShape->SetPos(m_pPresents->GetPosition());
		// 当たり判定の最大値と最小値の設定
		p2DRectShape->PassPos(m_pPresents->GetPosition());
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::Draw(void)
{
	// プレゼン情報がヌルではない場合
	if (m_pPresents != NULL)
	{
		// 描画処理
		m_pPresents->Draw();
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// UIの設定
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::Setting(void)
{
	// プレゼン情報がヌルではない場合
	if (m_pPresents != NULL)
	{
		// フェードエンドではないなら
		// ->関数を抜ける
		if (m_pPresents->GetFadetype() != C2DPresents::FADETYPE_END)
		{
			return;
		}
		// 位置情報
		if (ImGui::TreeNode("Pos"))
		{
			ImGui::DragFloat("X", &m_vec_UiLoad[m_nType][m_nUi].pos.x, 1.0f);
			ImGui::DragFloat("Y", &m_vec_UiLoad[m_nType][m_nUi].pos.y, 1.0f);
			ImGui::TreePop();
		}
		// サイズ情報
		if (ImGui::TreeNode("Size"))
		{
			ImGui::DragFloat("X", &m_vec_UiLoad[m_nType][m_nUi].size.x, 1.0f);
			ImGui::DragFloat("Y", &m_vec_UiLoad[m_nType][m_nUi].size.y, 1.0f);
			ImGui::TreePop();
		}
		// 色情報
		if (ImGui::TreeNode("Col"))
		{
			// 変数宣言
			float Col[4] = {
				m_vec_UiLoad[m_nType][m_nUi].col.r,
				m_vec_UiLoad[m_nType][m_nUi].col.g,
				m_vec_UiLoad[m_nType][m_nUi].col.b,
				m_vec_UiLoad[m_nType][m_nUi].col.a
			};
			// 色情報設定
			ImGui::ColorEdit4("色情報", Col);
			// 変化した値を代入する
			m_vec_UiLoad[m_nType][m_nUi].col = Col;
			ImGui::TreePop();
		}
		// 角度情報
		if (ImGui::TreeNode("ROT"))
		{
			ImGui::DragFloat("rot", &m_vec_UiLoad[m_nType][m_nUi].fRot, 0.01f, -D3DX_PI, D3DX_PI);
			ImGui::TreePop();
		}
		// テクスチャー座標情報
		if (ImGui::TreeNode("TEX_POS"))
		{
			// 最初のテクスチャー座標
			if (ImGui::TreeNode("FIRST"))
			{
				ImGui::DragFloat("X", &m_vec_UiLoad[m_nType][m_nUi].tex_first.x, 0.01f, 0.0f);
				ImGui::DragFloat("Y", &m_vec_UiLoad[m_nType][m_nUi].tex_first.y, 0.01f, 0.0f);
				ImGui::TreePop();
			}
			// 最後のテクスチャー座標
			if (ImGui::TreeNode("LAST"))
			{
				ImGui::DragFloat("X", &m_vec_UiLoad[m_nType][m_nUi].tex_last.x, 0.01f, 0.0f);
				ImGui::DragFloat("Y", &m_vec_UiLoad[m_nType][m_nUi].tex_last.y, 0.01f, 0.0f);
				ImGui::TreePop();
			}
			ImGui::TreePop();
		}
		// テクスチャータイプ情報
		if (ImGui::TreeNode("TextureType"))
		{
			ImGui::InputInt("TextureType", &m_vec_UiLoad[m_nType][m_nUi].nTexType, 1);
			// テクスチャーの上限を超えたら
			if (m_vec_UiLoad[m_nType][m_nUi].nTexType > CTexture_manager::GetTextureMax() - 1)
			{
				m_vec_UiLoad[m_nType][m_nUi].nTexType = CTexture_manager::GetTextureMax() - 1;
			}
			ImGui::TreePop();
			// テクスチャー更新
			m_pPresents->BindTexture(CTexture_manager::GetTexture(m_vec_UiLoad[m_nType][m_nUi].nTexType));
		}
		// オフセットタイプ情報
		if (ImGui::TreeNode("OffsetType"))
		{
			ImGui::InputInt("OffsetType", &m_vec_UiLoad[m_nType][m_nUi].nOffsetType, 1, CScene_TWO::OFFSET_TYPE_MAX - 1);
			ImGui::TreePop();
		}
		// 2Dプレゼンツ情報のオンオフ
		ImGui::Checkbox("Chack_2DPresents", &m_vec_UiLoad[m_nType][m_nUi].bPresents);

		if (m_vec_UiLoad[m_nType][m_nUi].bPresents)
		{
			// プレゼンツ情報
			if (ImGui::TreeNode("2DPresents"))
			{
				// 強調情報のオンオフ
				ImGui::Checkbox("Chack_Cooperation", &m_vec_UiLoad[m_nType][m_nUi].Presents.bCooperation);
				// 強調がオンだったら
				// ->強調情報設定可能
				if (m_vec_UiLoad[m_nType][m_nUi].Presents.bCooperation)
				{
					// 協調情報
					if (ImGui::TreeNode("Cooperation"))
					{
						// 色情報のオンオフ
						ImGui::Checkbox("Chack_COL", &m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.bCol);
						// 色がオンだったら
						// ->色情報設定可能
						if (m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.bCol)
						{
							// 色情報
							if (ImGui::TreeNode("COL"))
							{
								// 変化色
								if (ImGui::TreeNode("ChangeCol"))
								{
									// 変数宣言
									float Col[4] = {
										m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.Col.changecol.r,
										m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.Col.changecol.g,
										m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.Col.changecol.b,
										m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.Col.changecol.a
									};
									// 色情報設定
									ImGui::ColorEdit4("変化色", Col);
									// 変化した値を代入する
									m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.Col.changecol = Col;
									ImGui::TreePop();
								}
								// タイムスイッチ
								if (ImGui::TreeNode("TimeSwitch"))
								{
									ImGui::DragInt("TimeSwitch", &m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.Col.nTimeSwitch, 1, 0);
									ImGui::TreePop();
								}
								// タイムチェンジ
								if (ImGui::TreeNode("TimeChange"))
								{
									ImGui::DragInt("TimeChange", &m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.Col.nTimeChange, 1, 0);
									ImGui::TreePop();
								}
								ImGui::TreePop();
							}
							// 色の設定
							m_pPresents->SetCooperation(
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Col.changecol,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Col.nTimeSwitch,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Col.nTimeChange
							);
						}
						// 色の情報をオフにする
						else
						{
							m_pPresents->DeleteCooperation_Col();
						}
						// 拡大率情報のオンオフ
						ImGui::Checkbox("Chack_SCAL", &m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.bScal);
						// 拡大率がオンだったら
						// ->拡大率情報設定可能
						if (m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.bScal)
						{
							// 拡大率情報
							if (ImGui::TreeNode("SCAL"))
							{

								// 変化拡大率
								if (ImGui::TreeNode("ChangeScal"))
								{
									ImGui::DragFloat("ChangeScal", &m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.Scal.fChangeScal, 0.1f, 0.0f);
									ImGui::TreePop();
								}
								// 変化するのにかかる時間
								if (ImGui::TreeNode("TimeChange"))
								{
									ImGui::DragInt("TimeChange", &m_vec_UiLoad[m_nType][m_nUi].Presents.Cooperation.Scal.nTimeChange, 1, 0);
									ImGui::TreePop();
								}
								ImGui::TreePop();
							}
							// 拡大率の設定
							m_pPresents->SetCooperation(
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Scal.fChangeScal,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.Cooperation.Scal.nTimeChange
							);
						}
						// 拡大率の情報をオフにする
						else
						{
							m_pPresents->DeleteCooperation_Scal();
						}
						ImGui::TreePop();
					}
					// 強調スタート
					if (ImGui::Button("CooperationStart"))
					{
						m_pPresents->Start_Cooperation();
					}
				}

				// フェードイン情報のオンオフ
				ImGui::Checkbox("Chack_FadeIn", &m_vec_UiLoad[m_nType][m_nUi].Presents.bFadeIn);
				// フェードインがオンだったら
				// ->フェードイン情報設定可能
				if (m_vec_UiLoad[m_nType][m_nUi].Presents.bFadeIn)
				{
					// フェードイン情報
					if (ImGui::TreeNode("FadeIn"))
					{
						// 色情報のオンオフ
						ImGui::Checkbox("Chack_COL", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.bCol);
						// 色オンだったら
						// ->色情報設定可能
						if (m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.bCol)
						{
							// 色情報
							if (ImGui::TreeNode("COL"))
							{
								// 初期色
								if (ImGui::TreeNode("BeginCol"))
								{
									// 変数宣言
									float Col[4] = {
										m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadeCol.BeginCol.r,
										m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadeCol.BeginCol.g,
										m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadeCol.BeginCol.b,
										m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadeCol.BeginCol.a
									};
									// 色情報設定
									ImGui::ColorEdit4("初期色", Col);
									// 変化した値を代入する
									m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadeCol.BeginCol = Col;
									ImGui::TreePop();
								}
								// 末期色
								m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadeCol.EndCol = m_vec_UiLoad[m_nType][m_nUi].col;
								// 最大フレーム
								if (ImGui::TreeNode("MaxFram"))
								{
									ImGui::DragInt("MaxFram", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadeCol.nMaxfram, 1, 0);
									ImGui::TreePop();
								}
								ImGui::TreePop();
							}
							// フェードイン(色指定)の設定
							m_pPresents->SetFadeIn(
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadeCol.BeginCol,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadeCol.EndCol,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadeCol.nMaxfram
							);
						}
						// フェードイン(色指定)情報をオフにする
						else
						{
							m_pPresents->DeleteFadeIn_Col();
						}
						// 位置情報のオンオフ
						ImGui::Checkbox("Chack_Pos", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.bPos);
						// 位置オンだったら
						// ->位置情報設定可能
						if (m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.bPos)
						{
							// 位置情報
							if (ImGui::TreeNode("POS"))
							{
								// 初期位置
								if (ImGui::TreeNode("BeginPos"))
								{
									ImGui::DragFloat("x", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadePos.BeginPos.x, 0.1f);
									ImGui::DragFloat("y", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadePos.BeginPos.y, 0.1f);
									ImGui::TreePop();
								}
								// 末期位置
								m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadePos.EndPos = m_vec_UiLoad[m_nType][m_nUi].pos;
								// 最大フレーム
								if (ImGui::TreeNode("MaxFram"))
								{
									ImGui::DragInt("MaxFram", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadePos.nMaxfram, 1, 0);
									ImGui::TreePop();
								}
								ImGui::TreePop();
							}
							// フェードイン(位置指定)の設定
							m_pPresents->SetFadeIn(
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadePos.BeginPos,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadePos.EndPos,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadePos.nMaxfram
							);
						}
						// フェードイン(位置)情報をオフにする
						else
						{
							m_pPresents->DeleteFadeIn_Pos();
						}
						// アニメーション開始時間
						if (ImGui::TreeNode("StartAnim"))
						{
							ImGui::DragInt("StartAnim", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.FadeStart, 1, 0);
							ImGui::TreePop();
							// フェードイン(スタート時間指定)の設定
							m_pPresents->SetFadeIn_StarAnim(
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.FadeStart
							);
						}
						// スキップの有無情報のオンオフ
						ImGui::Checkbox("Chack_Skip", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeIn.bSkip);
						// フェードイン強制終了情報設定
						m_pPresents->SetFadeIn_Compulsion(m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeIn.bSkip);

						ImGui::TreePop();
					}
					// フェードインスタート
					if (ImGui::Button("FadeInStart"))
					{
						m_pPresents->Start_FadeIn();
					}
				}

				// フェードアウト情報のオンオフ
				ImGui::Checkbox("Chack_FadeOut", &m_vec_UiLoad[m_nType][m_nUi].Presents.bFadeOut);
				// フェードアウトがオンだったら
				// ->フェードアウト情報設定可能
				if (m_vec_UiLoad[m_nType][m_nUi].Presents.bFadeOut)
				{
					// フェードアウト情報
					if (ImGui::TreeNode("FadeOut"))
					{
						// 色情報のオンオフ
						ImGui::Checkbox("Chack_COL", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.bCol);
						// 色オンだったら
						// ->色情報設定可能
						if (m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.bCol)
						{
							// 色情報
							if (ImGui::TreeNode("COL"))
							{
								// 初期色
								m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadeCol.BeginCol = m_vec_UiLoad[m_nType][m_nUi].col;
								// 末期色
								if (ImGui::TreeNode("EndCol"))
								{
									// 変数宣言
									float Col[4] = {
										m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadeCol.EndCol.r,
										m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadeCol.EndCol.g,
										m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadeCol.EndCol.b,
										m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadeCol.EndCol.a
									};
									// 色情報設定
									ImGui::ColorEdit4("末期色", Col);
									// 変化した値を代入する
									m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadeCol.EndCol = Col;
									ImGui::TreePop();
								}
								// 最大フレーム
								if (ImGui::TreeNode("MaxFram"))
								{
									ImGui::DragInt("MaxFram", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadeCol.nMaxfram, 1, 0);
									ImGui::TreePop();
								}
								ImGui::TreePop();
							}
							// フェードアウト(色指定)の設定
							m_pPresents->SetFadeOut(
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadeCol.BeginCol,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadeCol.EndCol,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadeCol.nMaxfram
							);
						}
						// フェードアウト(色指定)情報をオフにする
						else
						{
							m_pPresents->DeleteFadeOut_Col();
						}
						// 位置情報のオンオフ
						ImGui::Checkbox("Chack_Pos", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.bPos);
						// 位置オンだったら
						// ->位置情報設定可能
						if (m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.bPos)
						{
							// 位置情報
							if (ImGui::TreeNode("POS"))
							{
								// 初期位置
								m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadePos.BeginPos = m_vec_UiLoad[m_nType][m_nUi].pos;
								// 末期位置
								if (ImGui::TreeNode("EndPos"))
								{
									ImGui::DragFloat("x", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadePos.EndPos.x, 0.1f);
									ImGui::DragFloat("y", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadePos.EndPos.y, 0.1f);
									ImGui::TreePop();
								}
								// 最大フレーム
								if (ImGui::TreeNode("MaxFram"))
								{
									ImGui::DragInt("MaxFram", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadePos.nMaxfram, 1, 0);
									ImGui::TreePop();
								}
								ImGui::TreePop();
							}
							// フェードアウト(位置指定)の設定
							m_pPresents->SetFadeOut(
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadePos.BeginPos,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadePos.EndPos,
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadePos.nMaxfram
							);
						}
						// フェードイン(位置)情報をオフにする
						else
						{
							m_pPresents->DeleteFadeOut_Pos();
						}
						// アニメーション開始時間
						if (ImGui::TreeNode("StartAnim"))
						{
							ImGui::DragInt("StartAnim", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.FadeStart, 1, 0);
							ImGui::TreePop();
							// フェードアウト(スタート時間指定)の設定
							m_pPresents->SetFadeOut_StarAnim(
								m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.FadeStart
							);
						}
						// スキップの有無情報のオンオフ
						ImGui::Checkbox("Chack_Skip", &m_vec_UiLoad[m_nType][m_nUi].Presents.FadeOut.bSkip);

						// フェードアウト強制終了情報設定
						m_pPresents->SetFadeOut_Compulsion(m_vec_UiLoad[m_nType].at(m_nUi).Presents.FadeOut.bSkip);
						ImGui::TreePop();
					}
					// フェードアウトスタート
					if (ImGui::Button("FadeOutStart"))
					{
						m_pPresents->Start_FadeOut();
					}
				}
				ImGui::TreePop();
			}

		}
		// 位置更新
		m_pPresents->SetPosition(m_vec_UiLoad[m_nType][m_nUi].pos);
		// サイズ更新
		m_pPresents->SetSize(m_vec_UiLoad[m_nType][m_nUi].size);
		// 角度更新
		m_pPresents->SetRot(m_vec_UiLoad[m_nType][m_nUi].fRot);
		// 色更新
		m_pPresents->SetCol(m_vec_UiLoad[m_nType][m_nUi].col);
		// オフセット更新
		m_pPresents->SetOffset((CScene_TWO::OFFSET_TYPE)m_vec_UiLoad[m_nType][m_nUi].nOffsetType);
		// 頂点座標情報更新
		m_pPresents->Set_Vtx_Pos((CScene_TWO::OFFSET_TYPE)m_vec_UiLoad[m_nType][m_nUi].nOffsetType);
		// 頂点カラー情報更新
		m_pPresents->Set_Vtx_Col();
		// テクスチャー座標情報更新
		m_pPresents->SetTex(m_vec_UiLoad[m_nType][m_nUi].tex_first, m_vec_UiLoad[m_nType][m_nUi].tex_last);
		// 当たり判定の位置設定
		p2DRectShape->SetSize(m_pPresents->GetSize());
		// 初期位置設定
		m_pPresents->OriginPos(m_vec_UiLoad[m_nType][m_nUi].pos);
		// 初期色設定
		m_pPresents->OriginCol(m_vec_UiLoad[m_nType][m_nUi].col);
		// 初期サイズ設定
		m_pPresents->OriginSize(m_vec_UiLoad[m_nType][m_nUi].size);
	}
}

#ifdef _DEBUG
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デバッグ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::Debug(void)
{
}
#endif // _DEBUG

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// テキストの読み込み処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CUi::Load(void)
{
	// UIマネージャー読み込み
	if (UiManagerLoad() != S_OK)
	{
#ifdef _DEBUG
		CCalculation::Messanger("UIマネージャー読み込み失敗");
#endif // _DEBUG
	};
	// UIスクリプトの読み込み
	if (UiScriptLoad() != S_OK)
	{
#ifdef _DEBUG
		CCalculation::Messanger("UIスクリプト読み込み失敗");
#endif // _DEBUG
	};
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// UIマネージャーの読み込み
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CUi::UiManagerLoad(void)
{
	// 変数宣言
	// ファイルの中身格納用
	vector<vector<string>> vsvec_Contens;
	// ファイルの中身を取得する
	vsvec_Contens = CCalculation::FileContens(UI_MANAGER_FILE,'\0');
	// 行ごとに回す
	for (int nCntLine = 0; nCntLine < (signed)vsvec_Contens.size(); nCntLine++)
	{
		// 項目ごとに回す
		for (int nCntItem = 0; nCntItem < (signed)vsvec_Contens.at(nCntLine).size(); nCntItem++)
		{
			switch (nCntItem)
			{
				// パス情報
			case 0:
				m_vec_String.emplace_back(vsvec_Contens.at(nCntLine).at(nCntItem));
				break;
			default:
				break;
			}
		}
	}
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// UIスクリプトの読み込み
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CUi::UiScriptLoad(void)
{
	// ファイルポイント
	FILE *pFile;

	// 変数宣言
	int  nCntError = 0;				// カウントロード
	int	nCntUi = 0;					// カウントUI
	char cComp[128];				// 特定の文字列の比較
	char cRaedText[128];			// 文字として読み取り用
	char cHeadText[128];			// 比較するよう
	char cDie[128];					// 不必要な文字
	vector<UI_LOAD> vec_Ui;			// UI情報の格納

	for (int nCntLoad = 0; nCntLoad < (signed)m_vec_String.size(); nCntLoad++)
	{
		nCntUi = 0;
		cHeadText[0] = '\0';
		cHeadText[0] = '\0';
		// ファイルが開かれていなかったら
		if ((pFile = fopen(m_vec_String[nCntLoad].c_str(), "r")) == NULL)
		{// メッセージの表示
#ifdef _DEBUG
			CCalculation::Messanger("UIのテキストファイルが見つかりませんでした");
#endif // _DEBUG
			continue;
		}
		// スクリプトが存在しているかチェック
		if (!CCalculation::Existenceofscript(pFile))
		{
#ifdef _DEBUG
			CCalculation::Messanger("UIの[SCRIP]が見つかりません\n終了してください");
#endif // _DEBUG
			fclose(pFile);
			continue;
		}
		// エンドスクリプトが来るまでループ
		while (strcmp(cHeadText, "END_SCRIPT") != 0)
		{
			fgets(cRaedText, sizeof(cRaedText), pFile);
			sscanf(cRaedText, "%s", &cHeadText);

			// 改行だったら
			if (strcmp(cHeadText, "\n") == 0)
			{
			}

			// モデルセットが来たら
			else if (strcmp(cHeadText, "SET") == 0)
			{
				vec_Ui.emplace_back(UI_LOAD());
				// UIの読み込み未完成
				// 比較の初期化
				cComp[0] = '\0';
				// 読み込んど文字列代入
				sscanf(cRaedText, "%s %s", &cDie, &cComp);
				// プレゼンツ用
				if (strcmp(cComp, "PRESENTS") == 0)
				{
					// 2Dプレゼンツ使用中
					vec_Ui[nCntUi].bPresents = true;
				}
				// エンドモデルセットが来るまでループ
				while (strcmp(cHeadText, "END_SET") != 0)
				{
					fgets(cRaedText, sizeof(cRaedText), pFile);
					sscanf(cRaedText, "%s", &cHeadText);

					// 位置情報読み込み
					if (strcmp(cHeadText, "POS") == 0)
					{
						sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
							&vec_Ui[nCntUi].pos.x,
							&vec_Ui[nCntUi].pos.y,
							&vec_Ui[nCntUi].pos.z);
					}
					// 位置情報読み込み
					else if (strcmp(cHeadText, "COL") == 0)
					{
						sscanf(cRaedText, "%s %s %f %f %f %f", &cDie, &cDie,
							&vec_Ui[nCntUi].col.r,
							&vec_Ui[nCntUi].col.b,
							&vec_Ui[nCntUi].col.g,
							&vec_Ui[nCntUi].col.a
						);
					}
					// サイズ情報読み込み
					else if (strcmp(cHeadText, "SIZE") == 0)
					{
						sscanf(cRaedText, "%s %s %f %f", &cDie, &cDie,
							&vec_Ui[nCntUi].size.x,
							&vec_Ui[nCntUi].size.y);
					}
					// 角度情報読み込み
					else if (strcmp(cHeadText, "ROT") == 0)
					{
						sscanf(cRaedText, "%s %s %f", &cDie, &cDie,
							&vec_Ui[nCntUi].fRot);
					}
					// テクスチャー座標(最初)情報読み込み
					else if (strcmp(cHeadText, "FIRST_TEX") == 0)
					{
						sscanf(cRaedText, "%s %s %f %f", &cDie, &cDie,
							&vec_Ui[nCntUi].tex_first.x,
							&vec_Ui[nCntUi].tex_first.y);
					}
					// テクスチャー座標(最後)情報読み込み
					else if (strcmp(cHeadText, "LAST_TEX") == 0)
					{
						sscanf(cRaedText, "%s %s %f %f", &cDie, &cDie,
							&vec_Ui[nCntUi].tex_last.x,
							&vec_Ui[nCntUi].tex_last.y);
					}
					// テクスチャータイプ情報読み込み
					else if (strcmp(cHeadText, "TEXTYPE") == 0)
					{
						sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
							&vec_Ui[nCntUi].nTexType);
					}
					// オフセットタイプ情報読み込み
					else if (strcmp(cHeadText, "OFFSET") == 0)
					{
						sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
							&vec_Ui[nCntUi].nOffsetType);
					}
					// プレゼンツ用の変数が使用状態なら入る
					else if (vec_Ui[nCntUi].bPresents == true)
					{
						// フェードイン情報読み込み
						if (strcmp(cHeadText, "FADE_IN") == 0)
						{
							// フェードイン使用中
							vec_Ui[nCntUi].Presents.bFadeIn = true;
							// エンドフェードインが来るまでループ
							while (strcmp(cHeadText, "END_FADE_IN") != 0)
							{
								fgets(cRaedText, sizeof(cRaedText), pFile);
								sscanf(cRaedText, "%s", &cHeadText);
								// スタートフェードタイム情報読み込み
								if (strcmp(cHeadText, "STARTFADETIME") == 0)
								{
									sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
										&vec_Ui[nCntUi].Presents.FadeIn.FadeStart);
								}
								// フェードスキップ情報読み込み
								else if (strcmp(cHeadText, "FADESKIP") == 0)
								{
									vec_Ui[nCntUi].Presents.FadeIn.bSkip = true;
								}
								// フェード色情報読み込み
								else if (strcmp(cHeadText, "FADE_COL") == 0)
								{
									// フェードカラー使用中
									vec_Ui[nCntUi].Presents.FadeIn.bCol = true;
									// エンドフェードカラーが来るまでループ
									while (strcmp(cHeadText, "END_FADE_COL") != 0)
									{
										fgets(cRaedText, sizeof(cRaedText), pFile);
										sscanf(cRaedText, "%s", &cHeadText);
										// 初期色情報読み込み
										if (strcmp(cHeadText, "BEGIN_COL") == 0)
										{
											sscanf(cRaedText, "%s %s %f %f %f %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeIn.FadeCol.BeginCol.r,
												&vec_Ui[nCntUi].Presents.FadeIn.FadeCol.BeginCol.g,
												&vec_Ui[nCntUi].Presents.FadeIn.FadeCol.BeginCol.b,
												&vec_Ui[nCntUi].Presents.FadeIn.FadeCol.BeginCol.a);
										}
										// 終了色情報読み込み
										else if (strcmp(cHeadText, "END_COL") == 0)
										{
											sscanf(cRaedText, "%s %s %f %f %f %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeIn.FadeCol.EndCol.r,
												&vec_Ui[nCntUi].Presents.FadeIn.FadeCol.EndCol.g,
												&vec_Ui[nCntUi].Presents.FadeIn.FadeCol.EndCol.b,
												&vec_Ui[nCntUi].Presents.FadeIn.FadeCol.EndCol.a);
										}
										// 最大フレーム情報読み込み
										else if (strcmp(cHeadText, "MAXFRAME") == 0)
										{
											sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeIn.FadeCol.nMaxfram);
										}
									}
								}
								// フェード位置情報読み込み
								else if (strcmp(cHeadText, "FADE_POS") == 0)
								{
									// フェード位置使用中
									vec_Ui[nCntUi].Presents.FadeIn.bPos = true;
									// エンドフェード位置が来るまでループ
									while (strcmp(cHeadText, "END_FADE_POS") != 0)
									{
										fgets(cRaedText, sizeof(cRaedText), pFile);
										sscanf(cRaedText, "%s", &cHeadText);
										// 初期位置情報読み込み
										if (strcmp(cHeadText, "BEGIN_POS") == 0)
										{
											sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeIn.FadePos.BeginPos.x,
												&vec_Ui[nCntUi].Presents.FadeIn.FadePos.BeginPos.y,
												&vec_Ui[nCntUi].Presents.FadeIn.FadePos.BeginPos.z);
										}
										// 終了位置情報読み込み
										else if (strcmp(cHeadText, "END_POS") == 0)
										{
											sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeIn.FadePos.EndPos.x,
												&vec_Ui[nCntUi].Presents.FadeIn.FadePos.EndPos.y,
												&vec_Ui[nCntUi].Presents.FadeIn.FadePos.EndPos.z);
										}
										// 最大フレーム情報読み込み
										else if (strcmp(cHeadText, "MAXFRAME") == 0)
										{
											sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeIn.FadePos.nMaxfram);
										}
									}
								}
							}
						}
						// フェードアウト情報読み込み
						else if (strcmp(cHeadText, "FADE_OUT") == 0)
						{
							// フェードアウト使用中
							vec_Ui[nCntUi].Presents.bFadeOut = true;
							// エンドフェードアウトが来るまでループ
							while (strcmp(cHeadText, "END_FADE_OUT") != 0)
							{
								fgets(cRaedText, sizeof(cRaedText), pFile);
								sscanf(cRaedText, "%s", &cHeadText);
								// スタートフェードタイム情報読み込み
								if (strcmp(cHeadText, "STARTFADETIME") == 0)
								{
									sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
										&vec_Ui[nCntUi].Presents.FadeOut.FadeStart);
								}
								// フェードスキップ情報読み込み
								else if (strcmp(cHeadText, "FADESKIP") == 0)
								{
									vec_Ui[nCntUi].Presents.FadeOut.bSkip = true;
								}
								// フェード色情報読み込み
								else if (strcmp(cHeadText, "FADE_COL") == 0)
								{
									// フェードカラー使用中
									vec_Ui[nCntUi].Presents.FadeOut.bCol = true;
									// エンドフェードカラーが来るまでループ
									while (strcmp(cHeadText, "END_FADE_COL") != 0)
									{
										fgets(cRaedText, sizeof(cRaedText), pFile);
										sscanf(cRaedText, "%s", &cHeadText);
										// 初期色情報読み込み
										if (strcmp(cHeadText, "BEGIN_COL") == 0)
										{
											sscanf(cRaedText, "%s %s %f %f %f %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeOut.FadeCol.BeginCol.r,
												&vec_Ui[nCntUi].Presents.FadeOut.FadeCol.BeginCol.g,
												&vec_Ui[nCntUi].Presents.FadeOut.FadeCol.BeginCol.b,
												&vec_Ui[nCntUi].Presents.FadeOut.FadeCol.BeginCol.a);
										}
										// 終了色情報読み込み
										else if (strcmp(cHeadText, "END_COL") == 0)
										{
											sscanf(cRaedText, "%s %s %f %f %f %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeOut.FadeCol.EndCol.r,
												&vec_Ui[nCntUi].Presents.FadeOut.FadeCol.EndCol.g,
												&vec_Ui[nCntUi].Presents.FadeOut.FadeCol.EndCol.b,
												&vec_Ui[nCntUi].Presents.FadeOut.FadeCol.EndCol.a);
										}
										// 最大フレーム情報読み込み
										else if (strcmp(cHeadText, "MAXFRAME") == 0)
										{
											sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeOut.FadeCol.nMaxfram);
										}
									}
								}
								// フェード位置情報読み込み
								else if (strcmp(cHeadText, "FADE_POS") == 0)
								{
									// フェードカラー使用中
									vec_Ui[nCntUi].Presents.FadeOut.bPos = true;
									// エンドフェード位置が来るまでループ
									while (strcmp(cHeadText, "END_FADE_POS") != 0)
									{
										fgets(cRaedText, sizeof(cRaedText), pFile);
										sscanf(cRaedText, "%s", &cHeadText);
										// 初期位置情報読み込み
										if (strcmp(cHeadText, "BEGIN_POS") == 0)
										{
											sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeOut.FadePos.BeginPos.x,
												&vec_Ui[nCntUi].Presents.FadeOut.FadePos.BeginPos.y,
												&vec_Ui[nCntUi].Presents.FadeOut.FadePos.BeginPos.z);
										}
										// 終了位置情報読み込み
										else if (strcmp(cHeadText, "END_POS") == 0)
										{
											sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeOut.FadePos.EndPos.x,
												&vec_Ui[nCntUi].Presents.FadeOut.FadePos.EndPos.y,
												&vec_Ui[nCntUi].Presents.FadeOut.FadePos.EndPos.z);
										}
										// 最大フレーム情報読み込み
										else if (strcmp(cHeadText, "MAXFRAME") == 0)
										{
											sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.FadeOut.FadePos.nMaxfram);
										}
									}
								}
							}
						}
						// 強調情報読み込み
						else if (strcmp(cHeadText, "COOPERATION") == 0)
						{
							// 強調使用中
							vec_Ui[nCntUi].Presents.bCooperation = true;
							// エンド強調が来るまでループ
							while (strcmp(cHeadText, "END_COOPERATION") != 0)
							{
								fgets(cRaedText, sizeof(cRaedText), pFile);
								sscanf(cRaedText, "%s", &cHeadText);
								// 拡大率情報読み込み
								if (strcmp(cHeadText, "SCAL") == 0)
								{
									// 拡大率使用中
									vec_Ui[nCntUi].Presents.Cooperation.bScal = true;
									// エンド拡大率が来るまでループ
									while (strcmp(cHeadText, "END_SCAL") != 0)
									{
										fgets(cRaedText, sizeof(cRaedText), pFile);
										sscanf(cRaedText, "%s", &cHeadText);
										// 拡大率情報読み込み
										if (strcmp(cHeadText, "CHANGESCAL") == 0)
										{
											sscanf(cRaedText, "%s %s %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.Cooperation.Scal.fChangeScal);
										}
										// 変化の時間情報読み込み
										else if (strcmp(cHeadText, "TIMECHANGE") == 0)
										{
											sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.Cooperation.Scal.nTimeChange);
										}
									}
								}
								// 色情報読み込み
								else if (strcmp(cHeadText, "COL") == 0)
								{
									// 色使用中
									vec_Ui[nCntUi].Presents.Cooperation.bCol = true;
									// エンド色が来るまでループ
									while (strcmp(cHeadText, "END_COL") != 0)
									{
										fgets(cRaedText, sizeof(cRaedText), pFile);
										sscanf(cRaedText, "%s", &cHeadText);
										// 変化する色情報読み込み
										if (strcmp(cHeadText, "CHANGECOL") == 0)
										{
											sscanf(cRaedText, "%s %s %f %f %f %f", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.Cooperation.Col.changecol.r,
												&vec_Ui[nCntUi].Presents.Cooperation.Col.changecol.g,
												&vec_Ui[nCntUi].Presents.Cooperation.Col.changecol.b,
												&vec_Ui[nCntUi].Presents.Cooperation.Col.changecol.a);
										}
										// 切り替え時間情報読み込み
										else if (strcmp(cHeadText, "TIMESWITCH") == 0)
										{
											sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.Cooperation.Col.nTimeSwitch);
										}
										// 切り替えにかかる時間情報読み込み
										else if (strcmp(cHeadText, "TIMECHANGE") == 0)
										{
											sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
												&vec_Ui[nCntUi].Presents.Cooperation.Col.nTimeChange);
										}
									}
								}
							}
						}
					}
					// エラーカウントをインクリメント
					nCntError++;
					if (nCntError > FILELINE_ERROW)
					{// エラー
						nCntError = 0;
						fclose(pFile);
						CCalculation::Messanger("UIセットがありません");
						return E_FAIL;
					}
				}

				// UIカウントアップ
				nCntUi++;
			}
			// エラーカウントをインクリメント
			nCntError++;
			if (nCntError > FILELINE_ERROW)
			{// エラー
				nCntError = 0;
				fclose(pFile);
				CCalculation::Messanger("エンドスクリプトがありません");
				return E_FAIL;
			}
		}
		// UI情報格納
		m_vec_UiLoad.emplace_back(vec_Ui);
		m_sta_UiUse.push_back(false);

		vec_Ui.clear();
		vec_Ui.shrink_to_fit();
		fclose(pFile);
	}
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ソースの書き込み処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CUi::Save(void)
{
	// テキストデータの書き込み
	UiManagerSave();
	// スクリプトの書き込み
	UiScriptSave();
	return E_NOTIMPL;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// テキストデータの書き込み処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CUi::UiManagerSave(void)
{
	// 書き込みファイル変数
	std::ofstream writing_file;
	// ファイル名を開く
	writing_file.open(UI_MANAGER_FILE, std::ios::out);
	if (!writing_file)
	{
		return E_FAIL;
	}
	// 全ファイル名の書き込み
	for (int nCntFile = 0; nCntFile < (signed)m_vec_String.size(); nCntFile++)
	{
		writing_file << m_vec_String[nCntFile] + "\n";
	}
	// ファイルを閉じる
	writing_file.close();
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// スクリプトの書き込み処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CUi::UiScriptSave(void)
{
	// 書き込みファイル変数
	std::ofstream writing_file;

	// 全ファイルの書き込み
	for (int nCntFile = 0; nCntFile < (signed)m_vec_String.size(); nCntFile++)
	{
		// ファイル名を開く
		writing_file.open(m_vec_String[nCntFile], std::ios::out);
		// 開いていなかったらループスキップ
		if (!writing_file)
		{
			continue;
		}
		// メインコメントの追加
		writing_file << CCalculation::FileMainComment(
			"UIエディター",
			m_vec_String[nCntFile],
			"KOKI NISHIYAMA"
		);
		// スクリプトの追加
		writing_file << "SCRIPT\n";
		// UI情報の書き込み
		for (int nCntUi = 0; nCntUi < (signed)m_vec_UiLoad[nCntFile].size(); nCntUi++)
		{
			// サブコメントの追加
			writing_file << CCalculation::FileSubComment(
				nCntUi,
				1
			);
			// UIの初期タイプの追加 //
			// 2Dプレゼンツが使用状態なら |
			// ->SET PRESENTSの分を追加する
			if (m_vec_UiLoad[nCntFile][nCntUi].bPresents)
			{
				writing_file << "\tSET PRESENTS\n";
			}
			// ->SETの文を追加する
			else
			{
				writing_file << "\tSET\n";
			}
			// ----------基本設定の追加---------- //
			{
				// 基本設定
				writing_file << CCalculation::FileSmallComment(
					"基本設定",
					2
				);
				// 位置追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\tPOS = %.1f %.1f %.1f\n",
					m_vec_UiLoad[nCntFile][nCntUi].pos.x,
					m_vec_UiLoad[nCntFile][nCntUi].pos.y,
					m_vec_UiLoad[nCntFile][nCntUi].pos.z
				);
				// 色の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\tCOL = %.1f %.1f %.1f %.1f\n",
					m_vec_UiLoad[nCntFile][nCntUi].col.r,
					m_vec_UiLoad[nCntFile][nCntUi].col.g,
					m_vec_UiLoad[nCntFile][nCntUi].col.b,
					m_vec_UiLoad[nCntFile][nCntUi].col.a
				);
				// サイズの追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\tSIZE = %.1f %.1f\n",
					m_vec_UiLoad[nCntFile][nCntUi].size.x,
					m_vec_UiLoad[nCntFile][nCntUi].size.y
				);
				// 角度の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\tROT = %.1f\n",
					m_vec_UiLoad[nCntFile][nCntUi].fRot
				);
				// テクスチャー座標(最初)の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\tFIRST_TEX = %.1f %.1f\n",
					m_vec_UiLoad[nCntFile][nCntUi].tex_first.x,
					m_vec_UiLoad[nCntFile][nCntUi].tex_first.y
				);
				// テクスチャー座標(最後)の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\tLAST_TEX = %.1f %.1f\n",
					m_vec_UiLoad[nCntFile][nCntUi].tex_last.x,
					m_vec_UiLoad[nCntFile][nCntUi].tex_last.y
				);
				// テクスチャータイプの追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\tTEXTYPE = %d\n",
					m_vec_UiLoad[nCntFile][nCntUi].nTexType
				);
				// オフセットの追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\tOFFSET = %d\n",
					m_vec_UiLoad[nCntFile][nCntUi].nOffsetType
				);
			}
			// 2Dプレゼンツの情報がない場合
			// ->ループスキップ
			if (!m_vec_UiLoad[nCntFile][nCntUi].bPresents)
			{
				// END_SET追加
				writing_file << "\tEND_SET\n";
				continue;
			}
			// ----------フェードインの追加---------- //
			if(m_vec_UiLoad[nCntFile][nCntUi].Presents.bFadeIn)
			{
				// フェードイン
				writing_file << CCalculation::FileSmallComment(
					"フェードイン",
					2
				);
				// FADE_INの追加
				writing_file << "\t\tFADE_IN\n";

				// フェード開始時間コメントの追加
				writing_file << CCalculation::FileNormalComment(
					"フェード開始時間",
					3
				);
				// スタートフェード時間情報追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\tSTARTFADETIME = %d\n",
					m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeStart
				);

				// スタートフェード時間情報追加
				if (m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.bSkip)
				{
					// フェード強制終了コメントの追加
					writing_file << CCalculation::FileNormalComment(
						"フェード強制終了",
						3
					);
					writing_file << "\t\t\tFADESKIP\n";
				}
				// 色指定追加
				if (m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.bCol)
				{
					// フェード強制終了コメントの追加
					writing_file << CCalculation::FileNormalComment(
						"色指定",
						3
					);
					// FADE_COL追加
					writing_file << "\t\t\tFADE_COL\n";
					// 初期色の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tBEGIN_COL = %.1f %.1f %.1f %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeCol.BeginCol.r,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeCol.BeginCol.g,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeCol.BeginCol.b,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeCol.BeginCol.a
					);
					// 末期色の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tEND_COL = %.1f %.1f %.1f %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeCol.EndCol.r,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeCol.EndCol.g,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeCol.EndCol.b,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeCol.EndCol.a
					);
					// 最大フレームの追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tMAXFRAME = %d\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadeCol.nMaxfram
					);
					// END_FADE_COL追加
					writing_file << "\t\t\tEND_FADE_COL\n";
				}
				// 位置指定追加
				if (m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.bPos)
				{
					// フェード強制終了コメントの追加
					writing_file << CCalculation::FileNormalComment(
						"位置指定",
						3
					);
					// FADE_COL追加
					writing_file << "\t\t\tFADE_POS\n";
					// 初期位置の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tBEGIN_POS = %.1f %.1f %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadePos.BeginPos.x,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadePos.BeginPos.y,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadePos.BeginPos.z
					);
					// 末期位置の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tEND_POS = %.1f %.1f %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadePos.EndPos.x,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadePos.EndPos.y,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadePos.EndPos.z
					);
					// 最大フレームの追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tMAXFRAME = %d\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeIn.FadePos.nMaxfram
					);
					// END_FADE_COL追加
					writing_file << "\t\t\tEND_FADE_POS\n";
				}
				// END_FADE_INの追加
				writing_file << "\t\tEND_FADE_IN\n";
			}
			// ----------フェードアウトの追加---------- //
			if (m_vec_UiLoad[nCntFile][nCntUi].Presents.bFadeOut)
			{
				// フェードアウト
				writing_file << CCalculation::FileSmallComment(
					"フェードアウト",
					2
				);
				// FADE_OUTの追加
				writing_file << "\t\tFADE_OUT\n";

				// フェード開始時間コメントの追加
				writing_file << CCalculation::FileNormalComment(
					"フェード開始時間",
					3
				);
				// スタートフェード時間情報追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\tSTARTFADETIME = %d\n",
					m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeStart
				);

				// スタートフェード時間情報追加
				if (m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.bSkip)
				{
					// フェード強制終了コメントの追加
					writing_file << CCalculation::FileNormalComment(
						"フェード強制終了",
						3
					);
					writing_file << "\t\t\tFADESKIP\n";
				}
				// 色指定追加
				if (m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.bCol)
				{
					// フェード強制終了コメントの追加
					writing_file << CCalculation::FileNormalComment(
						"色指定",
						3
					);
					// FADE_COL追加
					writing_file << "\t\t\tFADE_COL\n";
					// 初期色の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tBEGIN_COL = %.1f %.1f %.1f %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeCol.BeginCol.r,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeCol.BeginCol.g,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeCol.BeginCol.b,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeCol.BeginCol.a
					);
					// 末期色の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tEND_COL = %.1f %.1f %.1f %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeCol.EndCol.r,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeCol.EndCol.g,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeCol.EndCol.b,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeCol.EndCol.a
					);
					// 最大フレームの追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tMAXFRAME = %d\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadeCol.nMaxfram
					);
					// END_FADE_COL追加
					writing_file << "\t\t\tEND_FADE_COL\n";
				}
				// 位置指定追加
				if (m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.bPos)
				{
					// フェード強制終了コメントの追加
					writing_file << CCalculation::FileNormalComment(
						"位置指定",
						3
					);
					// FADE_COL追加
					writing_file << "\t\t\tFADE_POS\n";
					// 初期位置の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tBEGIN_POS = %.1f %.1f %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadePos.BeginPos.x,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadePos.BeginPos.y,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadePos.BeginPos.z
					);
					// 末期位置の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tEND_POS = %.1f %.1f %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadePos.EndPos.x,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadePos.EndPos.y,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadePos.EndPos.z
					);
					// 最大フレームの追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tMAXFRAME = %d\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.FadeOut.FadePos.nMaxfram
					);
					// END_FADE_COL追加
					writing_file << "\t\t\tEND_FADE_POS\n";
				}
				// END_FADE_INの追加
				writing_file << "\t\tEND_FADE_OUT\n";
			}
			// ----------強調の追加---------- //
			if (m_vec_UiLoad[nCntFile][nCntUi].Presents.bCooperation)
			{
				// 強調
				writing_file << CCalculation::FileSmallComment(
					"強調",
					2
				);
				// FADE_OUTの追加
				writing_file << "\t\tCOOPERATION\n";
				// 色の追加
				if (m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.bCol)
				{
					// 色の追加
					writing_file << CCalculation::FileNormalComment(
						"色変化",
						3
					);
					// COL追加
					writing_file << "\t\t\tCOL\n";
					// 変化色の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tCHANGECOL = %.1f %.1f %.1f %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.Col.changecol.r,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.Col.changecol.g,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.Col.changecol.b,
						m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.Col.changecol.a
					);
					// タイムスイッチの追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tTIMESWITCH = %d\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.Col.nTimeSwitch
					);
					// タイム変化の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tTIMECHANGE = %d\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.Col.nTimeChange
					);
					// END_COL追加
					writing_file << "\t\t\tEND_COL\n";
				}
				// 拡大率の追加
				if (m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.bScal)
				{
					// 拡大率の追加
					writing_file << CCalculation::FileNormalComment(
						"拡大率",
						3
					);
					// SCAL追加
					writing_file << "\t\t\tSCAL\n";
					// 拡大率の変化の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tCHANGESCAL = %.1f\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.Scal.fChangeScal
					);
					// タイム変化の追加
					writing_file << CCalculation::FileInfoWriting(
						"\t\t\t\tTIMECHANGE = %d\n",
						m_vec_UiLoad[nCntFile][nCntUi].Presents.Cooperation.Scal.nTimeChange
					);
					// END_SCAL追加
					writing_file << "\t\t\tEND_SCAL\n";
				}
				// END_COOPERATIONの追加
				writing_file << "\t\tEND_COOPERATION\n";
			}
			// END_SET追加
			writing_file << "\tEND_SET\n";
		}
		// スクリプトの追加
		writing_file << "END_SCRIPT\n";
		writing_file.close();
	}
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// スクリプトの書き込み処理(選択)
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CUi::UiScriptSave(int const & nType)
{
	// 書き込みファイル変数
	std::ofstream writing_file;
	// ファイル名を開く
	writing_file.open(m_vec_String[nType], std::ios::out);
	// 開いていなかったらループスキップ
	if (!writing_file)
	{
		return E_FAIL;
	}
	// メインコメントの追加
	writing_file << CCalculation::FileMainComment(
		"UIエディター",
		m_vec_String[nType],
		"KOKI NISHIYAMA"
	);
	// スクリプトの追加
	writing_file << "SCRIPT\n";
	// UI情報の書き込み
	for (int nCntUi = 0; nCntUi < (signed)m_vec_UiLoad[nType].size(); nCntUi++)
	{
		// サブコメントの追加
		writing_file << CCalculation::FileSubComment(
			nCntUi,
			1
		);
		// UIの初期タイプの追加 //
		// 2Dプレゼンツが使用状態なら |
		// ->SET PRESENTSの分を追加する
		if (m_vec_UiLoad[nType][nCntUi].bPresents)
		{
			writing_file << "\tSET PRESENTS\n";
		}
		// ->SETの文を追加する
		else
		{
			writing_file << "\tSET\n";
		}
		// ----------基本設定の追加---------- //
		{
			// 基本設定
			writing_file << CCalculation::FileSmallComment(
				"基本設定",
				2
			);
			// 位置追加
			writing_file << CCalculation::FileInfoWriting(
				"\t\tPOS = %.1f %.1f %.1f\n",
				m_vec_UiLoad[nType][nCntUi].pos.x,
				m_vec_UiLoad[nType][nCntUi].pos.y,
				m_vec_UiLoad[nType][nCntUi].pos.z
			);
			// 色の追加
			writing_file << CCalculation::FileInfoWriting(
				"\t\tCOL = %.1f %.1f %.1f %.1f\n",
				m_vec_UiLoad[nType][nCntUi].col.r,
				m_vec_UiLoad[nType][nCntUi].col.g,
				m_vec_UiLoad[nType][nCntUi].col.b,
				m_vec_UiLoad[nType][nCntUi].col.a
			);
			// サイズの追加
			writing_file << CCalculation::FileInfoWriting(
				"\t\tSIZE = %.1f %.1f\n",
				m_vec_UiLoad[nType][nCntUi].size.x,
				m_vec_UiLoad[nType][nCntUi].size.y
			);
			// テクスチャー座標(最初)の追加
			writing_file << CCalculation::FileInfoWriting(
				"\t\tFIRST_TEX = %.1f %.1f\n",
				m_vec_UiLoad[nType][nCntUi].tex_first.x,
				m_vec_UiLoad[nType][nCntUi].tex_first.y
			);
			// テクスチャー座標(最後)の追加
			writing_file << CCalculation::FileInfoWriting(
				"\t\tLAST_TEX = %.1f %.1f\n",
				m_vec_UiLoad[nType][nCntUi].tex_last.x,
				m_vec_UiLoad[nType][nCntUi].tex_last.y
			);
			// テクスチャータイプの追加
			writing_file << CCalculation::FileInfoWriting(
				"\t\tTEXTYPE = %d\n",
				m_vec_UiLoad[nType][nCntUi].nTexType
			);
			// オフセットの追加
			writing_file << CCalculation::FileInfoWriting(
				"\t\tOFFSET = %d\n",
				m_vec_UiLoad[nType][nCntUi].nOffsetType
			);
		}
		// 2Dプレゼンツの情報がない場合
		// ->ループスキップ
		if (!m_vec_UiLoad[nType][nCntUi].bPresents)
		{
			// END_SET追加
			writing_file << "\tEND_SET\n";
			continue;
		}
		// ----------フェードインの追加---------- //
		if (m_vec_UiLoad[nType][nCntUi].Presents.bFadeIn)
		{
			// フェードイン
			writing_file << CCalculation::FileSmallComment(
				"フェードイン",
				2
			);
			// FADE_INの追加
			writing_file << "\t\tFADE_IN\n";

			// フェード開始時間コメントの追加
			writing_file << CCalculation::FileNormalComment(
				"フェード開始時間",
				3
			);
			// スタートフェード時間情報追加
			writing_file << CCalculation::FileInfoWriting(
				"\t\t\tSTARTFADETIME = %d\n",
				m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeStart
			);

			// スタートフェード時間情報追加
			if (m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.bSkip)
			{
				// フェード強制終了コメントの追加
				writing_file << CCalculation::FileNormalComment(
					"フェード強制終了",
					3
				);
				writing_file << "\t\t\tFADESKIP\n";
			}
			// 色指定追加
			if (m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.bCol)
			{
				// フェード強制終了コメントの追加
				writing_file << CCalculation::FileNormalComment(
					"色指定",
					3
				);
				// FADE_COL追加
				writing_file << "\t\t\tFADE_COL\n";
				// 初期色の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tBEGIN_COL = %.1f %.1f %.1f %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeCol.BeginCol.r,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeCol.BeginCol.g,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeCol.BeginCol.b,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeCol.BeginCol.a
				);
				// 末期色の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tEND_COL = %.1f %.1f %.1f %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeCol.EndCol.r,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeCol.EndCol.g,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeCol.EndCol.b,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeCol.EndCol.a
				);
				// 最大フレームの追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tMAXFRAME = %d\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadeCol.nMaxfram
				);
				// END_FADE_COL追加
				writing_file << "\t\t\tEND_FADE_COL\n";
			}
			// 位置指定追加
			if (m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.bPos)
			{
				// フェード強制終了コメントの追加
				writing_file << CCalculation::FileNormalComment(
					"位置指定",
					3
				);
				// FADE_COL追加
				writing_file << "\t\t\tFADE_POS\n";
				// 初期位置の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tBEGIN_POS = %.1f %.1f %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadePos.BeginPos.x,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadePos.BeginPos.y,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadePos.BeginPos.z
				);
				// 末期位置の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tEND_POS = %.1f %.1f %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadePos.EndPos.x,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadePos.EndPos.y,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadePos.EndPos.z
				);
				// 最大フレームの追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tMAXFRAME = %d\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeIn.FadePos.nMaxfram
				);
				// END_FADE_COL追加
				writing_file << "\t\t\tEND_FADE_POS\n";
			}
			// END_FADE_INの追加
			writing_file << "\t\tEND_FADE_IN\n";
		}
		// ----------フェードアウトの追加---------- //
		if (m_vec_UiLoad[nType][nCntUi].Presents.bFadeOut)
		{
			// フェードアウト
			writing_file << CCalculation::FileSmallComment(
				"フェードアウト",
				2
			);
			// FADE_OUTの追加
			writing_file << "\t\tFADE_OUT\n";

			// フェード開始時間コメントの追加
			writing_file << CCalculation::FileNormalComment(
				"フェード開始時間",
				3
			);
			// スタートフェード時間情報追加
			writing_file << CCalculation::FileInfoWriting(
				"\t\t\tSTARTFADETIME = %d\n",
				m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeStart
			);

			// スタートフェード時間情報追加
			if (m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.bSkip)
			{
				// フェード強制終了コメントの追加
				writing_file << CCalculation::FileNormalComment(
					"フェード強制終了",
					3
				);
				writing_file << "\t\t\tFADESKIP\n";
			}
			// 色指定追加
			if (m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.bCol)
			{
				// フェード強制終了コメントの追加
				writing_file << CCalculation::FileNormalComment(
					"色指定",
					3
				);
				// FADE_COL追加
				writing_file << "\t\t\tFADE_COL\n";
				// 初期色の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tBEGIN_COL = %.1f %.1f %.1f %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeCol.BeginCol.r,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeCol.BeginCol.g,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeCol.BeginCol.b,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeCol.BeginCol.a
				);
				// 末期色の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tEND_COL = %.1f %.1f %.1f %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeCol.EndCol.r,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeCol.EndCol.g,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeCol.EndCol.b,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeCol.EndCol.a
				);
				// 最大フレームの追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tMAXFRAME = %d\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadeCol.nMaxfram
				);
				// END_FADE_COL追加
				writing_file << "\t\t\tEND_FADE_COL\n";
			}
			// 位置指定追加
			if (m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.bPos)
			{
				// フェード強制終了コメントの追加
				writing_file << CCalculation::FileNormalComment(
					"位置指定",
					3
				);
				// FADE_COL追加
				writing_file << "\t\t\tFADE_POS\n";
				// 初期位置の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tBEGIN_POS = %.1f %.1f %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadePos.BeginPos.x,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadePos.BeginPos.y,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadePos.BeginPos.z
				);
				// 末期位置の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tEND_POS = %.1f %.1f %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadePos.EndPos.x,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadePos.EndPos.y,
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadePos.EndPos.z
				);
				// 最大フレームの追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tMAXFRAME = %d\n",
					m_vec_UiLoad[nType][nCntUi].Presents.FadeOut.FadePos.nMaxfram
				);
				// END_FADE_COL追加
				writing_file << "\t\t\tEND_FADE_POS\n";
			}
			// END_FADE_INの追加
			writing_file << "\t\tEND_FADE_OUT\n";
		}
		// ----------強調の追加---------- //
		if (m_vec_UiLoad[nType][nCntUi].Presents.bCooperation)
		{
			// 強調
			writing_file << CCalculation::FileSmallComment(
				"強調",
				2
			);
			// FADE_OUTの追加
			writing_file << "\t\tCOOPERATION\n";
			// 色の追加
			if (m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.bCol)
			{
				// 色の追加
				writing_file << CCalculation::FileNormalComment(
					"色変化",
					3
				);
				// COL追加
				writing_file << "\t\t\tCOL\n";
				// 変化色の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tCHANGECOL = %.1f %.1f %.1f %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.Col.changecol.r,
					m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.Col.changecol.g,
					m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.Col.changecol.b,
					m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.Col.changecol.a
				);
				// タイムスイッチの追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tTIMESWITCH = %d\n",
					m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.Col.nTimeSwitch
				);
				// タイム変化の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tTIMECHANGE = %d\n",
					m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.Col.nTimeChange
				);
				// END_COL追加
				writing_file << "\t\t\tEND_COL\n";
			}
			// 拡大率の追加
			if (m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.bScal)
			{
				// 拡大率の追加
				writing_file << CCalculation::FileNormalComment(
					"拡大率",
					3
				);
				// SCAL追加
				writing_file << "\t\t\tSCAL\n";
				// 拡大率の変化の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tCHANGESCAL = %.1f\n",
					m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.Scal.fChangeScal
				);
				// タイム変化の追加
				writing_file << CCalculation::FileInfoWriting(
					"\t\t\t\tTIMECHANGE = %d\n",
					m_vec_UiLoad[nType][nCntUi].Presents.Cooperation.Scal.nTimeChange
				);
				// END_SCAL追加
				writing_file << "\t\t\tEND_SCAL\n";
			}
			// END_COOPERATIONの追加
			writing_file << "\t\tEND_COOPERATION\n";
		}
		// END_SET追加
		writing_file << "\tEND_SET\n";
	}
	// スクリプトの追加
	writing_file << "END_SCRIPT\n";
	writing_file.close();
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 全ソースの開放処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::UnLoad(void)
{
	// すべてをセーブする
	Save();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 作成処理(シーン管理)
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CUi * CUi::Create(
	int const &nType,
	int const &nUi
)
{
	// 変数宣言
	CUi * pUi;
	// メモリ確保
	pUi = new CUi;
	// UIタイプの設定
	pUi->m_nType = nType;
	// UIID設定
	pUi->m_nUi = nUi;
	// シーン管理設定
	pUi->ManageSetting(CScene::LAYER_UI);
	// 初期化処理
	pUi->Init();
	return pUi;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 作成処理(個人管理)
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
unique_ptr<CUi> CUi::Create_Self(
	int const &nType,
	int const &nUi
)
{
	// 変数宣言
	unique_ptr<CUi> pUi(new CUi);
	// UIタイプの設定
	pUi->m_nType = nType;
	// UIID設定
	pUi->m_nUi = nUi;
	// 初期化処理
	pUi->Init();
	return pUi;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだものの作成(シーン管理)
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CUi::VEC_P_UI CUi::LoadCreate(
	int const &nType
	)
{
	// 使用状態がtrueなら抜ける
	if (m_sta_UiUse[nType])
	{
		return VEC_P_UI();
	}
	// 変数宣言
	VEC_P_UI vec_pUi;	// UI情報格納用変数
	for (int nCntUi = 0; nCntUi < (signed)m_vec_UiLoad[nType].size(); nCntUi++)
	{
		// 作成処理(シーン管理)
		vec_pUi.push_back(Create(nType, nCntUi));
	}
	// 使用状態をtrueへ
	m_sta_UiUse[nType] = true;
	// UI情報を渡す
	return vec_pUi;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだものの作成(個人管理)
// 返すときはstd::moveを使用すること
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CUi::VEC_UNI_UI CUi::LoadCreate_Self(
	int const &nType
)
{
	// 変数宣言
	VEC_UNI_UI vec_pUi;	// UI情報格納用変数
	// 使用状態がtrueなら抜ける
	if (m_sta_UiUse[nType])
	{
		return vec_pUi;
	}
	for (int nCntUi = 0; nCntUi < (signed)m_vec_UiLoad[nType].size(); nCntUi++)
	{
		// 作成処理(個人管理)
		vec_pUi.push_back(std::move(Create_Self(nType, nCntUi)));
	}
	// 使用状態をtrueへ
	m_sta_UiUse[nType] = true;
	// 現在のUItypeを代入
	m_nUitype = nType;
	// UI情報を渡す
	return vec_pUi;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// UIを削除する
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::DeleteUi(int const & nType, int const & nUi)
{
	m_vec_UiLoad[nType].erase(m_vec_UiLoad[nType].begin() + nUi);
	m_vec_UiLoad[nType].shrink_to_fit();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// UIを作成する
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
unique_ptr<CUi> CUi::CreateUi(int const & nType)
{
	// 変数宣言
	UI_LOAD UiInfo;
	// 初期設定
	UiInfo.pos = { SCREEN_WIDTH * 0.5f,SCREEN_HEIGHT * 0.5f,0.0f };
	UiInfo.size = { 200.0f,200.0f };
	UiInfo.col = D3DXCOLOR_INI;
	// UI情報格納
	m_vec_UiLoad[nType].emplace_back(UiInfo);
	// 変数宣言
	unique_ptr<CUi> pUi(new CUi);
	// UIタイプの設定
	pUi->m_nType = nType;
	// UIID設定
	pUi->m_nUi = (signed)m_vec_UiLoad[nType].size() + -1;
	// 初期化処理
	pUi->Init();
	return pUi;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ファイルの新規作成
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::FileNew(char *cName)
{
	// 変数宣言
	vector<UI_LOAD> vec_Load;
	bool bName = false;
	string PassName = "data/LOAD/UI/";
	// 情報の格納
	vec_Load.emplace_back(UI_LOAD());
	// 設定
	vec_Load.begin()->pos = { SCREEN_WIDTH * 0.5f,SCREEN_HEIGHT * 0.5f,0.0f };
	vec_Load.begin()->size = { 200.0f,200.0f };
	vec_Load.begin()->col = D3DXCOLOR_INI;
	// 情報を追加する
	m_vec_UiLoad.push_back(vec_Load);
	m_sta_UiUse.push_back(false);
	// パスの結合
	PassName += cName;
	PassName += ".txt";
	m_vec_String.push_back(PassName);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ファイル情報の削除
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::FileDelete_Info(int const & nType)
{
	// ファイル名の削除
	m_vec_String.erase(m_vec_String.begin() + nType);
	m_vec_String.shrink_to_fit();
	// 選択した使用状態を削除
	m_sta_UiUse.erase(m_sta_UiUse.begin() + nType);
	m_sta_UiUse.shrink_to_fit();
	// UI情報の削除
	m_vec_UiLoad[nType].clear();
	m_vec_UiLoad[nType].shrink_to_fit();
	m_vec_UiLoad.erase(m_vec_UiLoad.begin() + nType);
	m_vec_UiLoad.shrink_to_fit();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ファイル削除
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CUi::FileDelete(int const & nType)
{
	// ファイルの削除
	DeleteFileA(m_vec_String[nType].c_str());
	// ファイル情報の削除
	FileDelete_Info(nType);
	// UIマネージャーに現存しているファイル名を保存
	UiManagerSave();
}
