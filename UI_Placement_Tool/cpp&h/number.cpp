// ----------------------------------------
//
// 番号処理の説明[number.cpp]
// Author : Koki Nishiyama
//
// ----------------------------------------

// ----------------------------------------
//
// インクルードファイル
//
// ----------------------------------------
/* 描画 */
#include "number.h"

// ----------------------------------------
//
// マクロ定義
//
// ----------------------------------------

// ----------------------------------------
//
// グローバル変数
//
// ----------------------------------------

// ----------------------------------------
//
// 静的変数宣言
//
// ----------------------------------------
LPDIRECT3DTEXTURE9 CNumber::m_pTex[CNumber::TEX_MAX] = {};

// ----------------------------------------
// コンストラクタ処理
// ----------------------------------------
CNumber::CNumber() : CScene_TWO::CScene_TWO()
{
	m_texID = TEX_SCORE;
}

// ----------------------------------------
// デストラクタ処理
// ----------------------------------------
CNumber::~CNumber()
{
}

// ----------------------------------------
// 初期化処理
// ----------------------------------------
void CNumber::Init(void)
{	
	CScene_TWO::Init();
	CScene_TWO::BindTexture(m_pTex[m_texID]);
}

// ----------------------------------------
// 終了処理
// ----------------------------------------
void CNumber::Uninit(void)
{
	CScene_TWO::Uninit();
}

// ----------------------------------------
// 更新処理
// ----------------------------------------
void CNumber::Update(void)
{
}

// ----------------------------------------
// 描画処理
// ----------------------------------------
void CNumber::Draw(void)
{
	// 初期化
	CScene_TWO::Draw();
}

// ----------------------------------------
// 番号設定処理
// ----------------------------------------
void CNumber::SetNum(int const &nNum)
{
	// 変数宣言
	float fTex;			// テクスチャー範囲
	// テクスチャー範囲の計算
	fTex = (float)nNum / 10.0f;
	// テクスチャー設定
	CScene_TWO::SetTex(
		D3DXVECTOR2(fTex, 0.0f),
		D3DXVECTOR2(fTex + 0.1f, 1.0f)
	);
}

// ----------------------------------------
// テクスチャー処理
// ----------------------------------------
void CNumber::SetTex(TEX const &tex)
{
	m_texID = tex;
}

// ----------------------------------------
// 読み込み処理
// ----------------------------------------
HRESULT CNumber::Load(void)
{
	LPDIRECT3DDEVICE9 pDevice =		// デバイスの取得
		CManager::GetRenderer()->GetDevice();

	// テクスチャー情報代入
	char TexData[TEX_MAX][72] =
	{
		{ "data/TEXTURE/UI/00_number.png" },
		{ "data/TEXTURE/UI/01_number.png" },
	};
	// テクスチャーの読み込み
	for (int nCnt = 0; nCnt < TEX_MAX; nCnt++)
	{
		D3DXCreateTextureFromFile(pDevice, TexData[nCnt], &m_pTex[nCnt]);
	}

	return S_OK;
}

// ----------------------------------------
// 読み込んだ情報を破棄
// ----------------------------------------
void CNumber::UnLoad(void)
{
	// テクスチャーの読み込み
	for (int nCnt = 0; nCnt < TEX_MAX; nCnt++)
	{
		if (m_pTex[nCnt] != NULL)
		{
			m_pTex[nCnt]->Release();
			m_pTex[nCnt] = NULL;
		}
	}
}

// ----------------------------------------
// 作成処理
// ----------------------------------------
CNumber * CNumber::Create(
	int			const & nScore,
	D3DXVECTOR3 const & pos,
	TEX			const & tex,			
	D3DXVECTOR2 const & size,
	D3DXCOLOR	const & col
)
{
	// 変数宣言
	CNumber * pNumber;
	// メモリの生成(初め->基本クラス,後->派生クラス)
	pNumber = new CNumber();
	// 位置設定
	pNumber->SetPosition(pos);
	//サイズ設定
	pNumber->SetSize(size);
	// テクスチャータイプ設定
	pNumber->SetTex(tex);
	// 初期化処理
	pNumber->Init();
	// スコア設定
	pNumber->SetNum(nScore);
	// 生成したオブジェクトを返す
	return pNumber;
}