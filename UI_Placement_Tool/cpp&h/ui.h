// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// ユーザーインターフェースヘッダー処理 [ui.h]
// Author : KOKI NISHIYAMA
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _UI_H_
#define _UI_H_

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "scene.h"
#include "shape.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 前方宣言
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CScene_TWO;
class C2DPresents;

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CUi : public CScene
{
public:
	/* 列挙型 */
	/* 構造体*/
	// 点滅ロード
	typedef struct COOPERATION_COL_LOAD
	{
		COOPERATION_COL_LOAD()
		{
			changecol = D3DXCOLOR_INI;
			nTimeSwitch = 0;
			nTimeChange = 0;
		}
		D3DXCOLOR changecol;	// 変わる色
		int nTimeSwitch;		// 切り替え時間
		int nTimeChange;		// 切り替わる時間
	} COOPERATION_COL_LOAD, *P_COOPERATION_COL_LOAD;
	// 拡大率ロード
	typedef struct COOPERATION_SCAL_LOAD
	{
		COOPERATION_SCAL_LOAD()
		{
			fChangeScal = 0.0f;
			nTimeChange = 0;
		}
		float fChangeScal;		// 拡大率
		int nTimeChange;		// 切り替わる時間
	} COOPERATION_SCAL_LOAD, *P_COOPERATION_SCAL_LOAD;

	// 強調ロード
	typedef struct COOPERATION_LOAD
	{
		COOPERATION_LOAD()
		{
			bCol = false;
			Col = COOPERATION_COL_LOAD();
			bScal = false;
			Scal = COOPERATION_SCAL_LOAD();
		}
		// 色
		bool bCol;							// 色の使用状態
		COOPERATION_COL_LOAD Col;		// 色
		// 拡大率
		bool bScal;							// 拡大率の使用状態
		COOPERATION_SCAL_LOAD Scal;		// 拡大率
	} COOPERATION_LOAD, *P_COOPERATION_LOAD;

	// フェードスタート時間
	typedef int NSTARTFADE, *P_NSTARTFADE;
	// フェード(色指定)ロード
	typedef struct FADE_COL_LOAD
	{
		FADE_COL_LOAD()
		{
			BeginCol = D3DXCOLOR_INI;
			EndCol = D3DXCOLOR_INI;
			nMaxfram = 0;
		}
		D3DXCOLOR BeginCol;	// 初期カラー
		D3DXCOLOR EndCol;	// 目的カラー
		int nMaxfram;		// 最大フレーム
	} FADE_COL_LOAD, *P_FADE_COL_LOAD;
	// フェード位置ロード
	typedef struct FADE_POS_LOAD
	{
		FADE_POS_LOAD()
		{
			BeginPos = D3DVECTOR3_ZERO;
			EndPos = D3DVECTOR3_ZERO;
			nMaxfram = 0;
		}
		D3DXVECTOR3 BeginPos;	// 初期位置
		D3DXVECTOR3 EndPos;		// 目的位置
		int nMaxfram;			// 最大フレーム
	} FADE_POS_LOAD, *P_FADE_POS_LOAD;

	// フェードインロード
	typedef struct FADEIN_LOAD
	{
		FADEIN_LOAD()
		{
			FadeStart = 0;
			bCol = false;
			FadeCol = FADE_COL_LOAD();
			bPos = false;
			FadePos = FADE_POS_LOAD();
			bSkip = false;
		}
		NSTARTFADE FadeStart;		// フェードスタート時間
		// カラー用
		bool bCol;					// フェード(色指定)使用状態
		FADE_COL_LOAD FadeCol;	// フェード(色指定)
		// 位置用
		bool bPos;					// フェード(位置)使用状態
		FADE_POS_LOAD FadePos;	// フェード(位置)
		bool bSkip;					// フェードのスキップ
	} FADEIN_LOAD, *P_FADEIN_LOAD;
	// フェードアウトロード
	typedef struct FADEOUT_LOAD
	{
		FADEOUT_LOAD()
		{
			FadeStart = 0;
			bCol = false;
			FadeCol = FADE_COL_LOAD();
			bPos = false;
			FadePos = FADE_POS_LOAD();
			bSkip = false;
		}
		NSTARTFADE FadeStart;	// フェードスタート時間
		// カラー用
		bool bCol;					// フェード(色指定)使用状態
		FADE_COL_LOAD FadeCol;	// フェード(色指定)
		// 位置用
		bool bPos;					// フェード(位置)使用状態
		FADE_POS_LOAD FadePos;	// フェード(位置)

		bool bSkip;					// フェードのスキップ
	} FADEOUT_LOAD, *P_FADEOUT_LOAD;
	// 2DPresentロード
	typedef struct PRESENTS_LOAD
	{
		PRESENTS_LOAD()
		{
			bFadeIn = false;
			FadeIn = FADEIN_LOAD();
			bFadeOut = false;
			FadeOut = FADEOUT_LOAD();
			bCooperation = false;
			Cooperation = COOPERATION_LOAD();
		}		
		// フェードイン用
		bool bFadeIn;						// フェードインの使用状態
		FADEIN_LOAD FadeIn;				// フェードインのロード
		// フェードアウト用
		bool bFadeOut;						// フェードアウトの使用状態
		FADEOUT_LOAD FadeOut;			// フェードアウトのロード
		// 強調用
		bool bCooperation;					// 強調の使用状態
		COOPERATION_LOAD Cooperation;	// 強調のロード
	} PRESENTS_LOAD, *P_PRESENTS_LOAD;


	typedef struct UI_LOAD
	{
		UI_LOAD()
		{
			pos = D3DVECTOR3_ZERO;
			col = D3DXCOLOR_INI;
			size = D3DVECTOR2_ZERO;
			fRot = 0.0f;
			tex_first = D3DVECTOR2_ZERO;	
			tex_last = { 1.0f,1.0f };
			nTexType = 0;
			nOffsetType = 0;
			bPresents = false;
			Presents = PRESENTS_LOAD();
		}
		D3DXVECTOR3 pos;			// 位置
		D3DXCOLOR col;				// 色
		D3DXVECTOR2 size;			// サイズ
		D3DXVECTOR2 tex_first;		// 最初のテクスチャー座標
		D3DXVECTOR2 tex_last;		// 最後のテクスチャー座標
		float fRot;					// 角度
		int nTexType;				// テクスチャータイプ
		int nOffsetType;			// オフセット
		// 2Dプレゼンツ用
		bool bPresents;				// 2Dプレゼンツの使用状態
		PRESENTS_LOAD Presents;	// 2Dプレゼンツのロード
	} UI_LOAD, *P_UI_LOAD;

	// UIの情報格納用型名
	typedef vector<unique_ptr<CUi>> VEC_UNI_UI;
	// UIの情報格納用型名
	typedef vector<CUi*> VEC_P_UI;
	/* 関数 */
	CUi();
	~CUi();
	void Init(void);
	void Uninit(void);
	void Update(void);
	void Draw(void);
	void Setting(void);
#ifdef _DEBUG
	void Debug(void);
#endif // _DEBUG
	// UI番号取得
	int const &GetUi(void) { return m_nUi; };
	// プレゼンツ情報取得
	C2DPresents * GetPresents(void) { return m_pPresents; };
	// UI番号設定
	void SetUi(int const &nUi) { m_nUi = nUi; };
	// ソースの読み込み
	static HRESULT Load(void);
	// UIマネージャーの読み込み
	static HRESULT UiManagerLoad(void);
	// UIスクリプトの読み込み
	static HRESULT UiScriptLoad(void);
	// ソースの書き込み
	static HRESULT Save(void);
	// UIマネージャーの読み込み
	static HRESULT UiManagerSave(void);
	// UIスクリプトの読み込み
	static HRESULT UiScriptSave(void);
	// UIスクリプトの読み込み(選択)
	static HRESULT UiScriptSave(int const &nType);
	static void UnLoad(void);						// UnLoadする
	// 作成処理(シーン管理)
	static CUi * Create(
		int const &nType,
		int const &nUi
	);
	// 作成処理(個人管理)
	static unique_ptr<CUi> Create_Self(
		int const &nType,
		int const &nUi
	);
	// ロードしたものを作成する(シーン管理)
	static CUi::VEC_P_UI LoadCreate(int const &nType);
	// ロードしたものを作成する(個人管理)
	static VEC_UNI_UI LoadCreate_Self(int const &nType);
	// 使用状態を取得
	static bool GetUse(
		int const &nType
	) { return m_sta_UiUse.at(nType); };
	// 使用状態を設定
	static void SetUse(
		bool const &bUse,
		int const &nType
	) { m_sta_UiUse.at(nType) = bUse; };
	// 選択中のUITYPE取得
	static int GetSelectUiType(void) { return m_nUitype; };
	// 選択中のUITYPEの設定
	static void SetSelectUiType(int const &nUiType) { m_nUitype = nUiType; };
	// 選択中のUI取得
	static int GetSelectUiSelect(void) { return m_nUiSelect; };
	// 選択中のUIの設定
	static void SetSelectUiSelect(int const &nUiSelect) { m_nUiSelect = nUiSelect; };
	// UI情報の最大個数
	static int GetUiNumAll(void) { return (signed)m_vec_String.size(); };
	// UIのファイル名(全部)
	static vector<string> GetString(void) { return m_vec_String; };
	// UIのファイル名(一つ)
	// 引数:UITYPE
	static string GetString_One() { return m_vec_String[m_nUitype]; };
	// UIを削除
	static void DeleteUi(
		int const &nType,	// タイプ
		int const &nUi		// UI
	);
	// UIを作成する
	static unique_ptr<CUi> CreateUi(
		int const &nType
	);
	// ファイルの新規作成
	static void FileNew(char *cName);
	// 選択したUIのファイル情報を削除
	static void FileDelete_Info(int const &nType);
	// 選択したUIのファイル削除
	static void FileDelete(int const &nType);
protected:
private:
	/* 列挙型 */

	/* 関数 */

	/* 変数 */
	static vector<vector<UI_LOAD>> m_vec_UiLoad;	// Ui読み込み用変数
	static vector<bool> m_sta_UiUse;				// このUIの使用状態
	static vector<string> m_vec_String;				// ファイル情報読み書き用
	static int m_nUitype;							// 選択中のUIスクリプト
	static int m_nUiSelect;							// UIの選択
	C2DPresents * m_pPresents;						// 2Dプレゼンツ
	int m_nType;									// UIタイプ
	int m_nUi;										// UI
	
	// 処理に必要な変数
	// int型
	vector<int> m_vec_nNumber;						// int情報
	// bool型
	vector<bool> m_vec_Bool;						// bool情報						
	// シーンクラス
	unique_ptr<CScene> m_pScene;					// シーン情報
	// 当たり判定
	unique_ptr<C2DRectShape> p2DRectShape;			// 矩形の当たり判定
};
#endif
