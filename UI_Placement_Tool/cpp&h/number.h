// ----------------------------------------
//
// 番号処理の説明[number.h]
// Author : Koki Nishiyama
//
// ----------------------------------------
#ifndef _NUMBER_H_
#define _NUMBER_H_	 // ファイル名を基準を決める

// ----------------------------------------
//
// インクルードファイル
//
// ----------------------------------------
#include "scene_two.h"

// ----------------------------------------
//
// マクロ定義
//
// ----------------------------------------
#define MAX_NUMBER (10)

// ------------------------------------------
//
// クラス
//
// ------------------------------------------
class CNumber : public CScene_TWO
{
public:
	/* 列挙型 */
	// テクスチャータイプ
	typedef enum
	{
		TEX_SCORE,
		TEX_SCORE_RESULT,
		TEX_MAX
	} TEX;

	/* 関数 */
	CNumber();
	~CNumber();
	void Init(void);
	void Uninit(void);
	void Update(void);
	void Draw(void);
	void SetNum(int const &nNum);								// 番号設定
	void SetTex(TEX const &tex);								// テクスチャー
	static HRESULT Load(void);
	static void UnLoad(void);
	// 作成
	static CNumber * Create(
		int			const & nScore,								// スコア
		D3DXVECTOR3 const & pos,								// 位置
		TEX			const & tex = TEX_SCORE,					// テクスチャータイプ
		D3DXVECTOR2 const & size = D3DXVECTOR2(100.0f,100.0f),	// サイズ
		D3DXCOLOR	const & col = D3DXCOLOR_INI					// 色
	);	// 作成
protected:

private:
	static LPDIRECT3DTEXTURE9 m_pTex[TEX_MAX];					// テクスチャー
	TEX m_texID;												// テクスチャー番号
};

#endif