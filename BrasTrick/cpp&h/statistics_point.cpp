// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// タイトル用UI処理の説明[statistics_point.cpp]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* 描画 */
#include "statistics_point.h"
#include "statistics.h"
#include "ui.h"
#include "score_board.h"
#include "scene_two.h"
#include "enemy.h"
#include "number.h"
#include "character.h"
#include "game.h"
#include "fade.h"
#include "topscore.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#define BONUS_SCORE (10000)
#define ANIMETIME (60)

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// グローバル変数
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数宣言
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CStatistics_point::CStatistics_point() : CScene::CScene()
{
	// 初期化
	for (int nCnt = 0; nCnt < STATISTICS_POINT_MAX; nCnt++)
	{
		m_pscore_board[nCnt] = NULL;	// スコアボード
		m_nScore[nCnt] = 0;
	}
	m_bAnim = true;
	m_nFram = 0;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CStatistics_point::~CStatistics_point()
{
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 初期化処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CStatistics_point::Init(void)
{
	// 変数宣言
	vector<CUi::UI_LOAD> vec_UiLoad = CUi::GetUi(CUi::UITYPE_TOTALUI_NUMBER);
	CScene_TWO *pScene_Two;
	int nCntPoint = 0;	// forループ用カウント
	// スコアの計算
	// 各敵のKOカウント代入
	for (nCntPoint = 0; nCntPoint <= STATISTICS_POINT_KOBOSS; nCntPoint++)
	{
		m_nScore[nCntPoint] = CStatistics::GetEnemy(nCntPoint);
	}
	// それぞれの点数の計算
	m_nScore[STATISTICS_POINT_SCORESOLLIDER] =
		m_nScore[STATISTICS_POINT_KOSOLLIDER] * CCharacter::GetStatus(CCharacter::CHARACTER_SOLDIER).nMaxScore;
	m_nScore[STATISTICS_POINT_SCOREARCHER] =
		m_nScore[STATISTICS_POINT_KOARCHER] * CCharacter::GetStatus(CCharacter::CHARACTER_ARCHER).nMaxScore;
	m_nScore[STATISTICS_POINT_SCOREMERCHANT] =
		m_nScore[STATISTICS_POINT_KOMERCHANT] * CCharacter::GetStatus(CCharacter::CHARACTER_MERCHANT).nMaxScore;
	m_nScore[STATISTICS_POINT_SCOREBOSS] =
		m_nScore[STATISTICS_POINT_KOBOSS] * CCharacter::GetStatus(CCharacter::CHARACTER_GENERAL).nMaxScore;
	// ボーナス1がtrueなら
	// ->ボーナススコアを加算
	if (CGame::GetbBonus_1())
	{
		m_nScore[STATISTICS_POINT_BONUS_1] = BONUS_SCORE;
		// 合格UIを出す
		pScene_Two = CScene_TWO::Create(CScene_TWO::OFFSET_TYPE_CENTER, vec_UiLoad[STATISTICS_CLEAR_BONUS_1].pos, vec_UiLoad[STATISTICS_CLEAR_BONUS_1].size);
		pScene_Two->BindTexture(vec_UiLoad[STATISTICS_CLEAR_BONUS_1].nTexType);
	}
	// ボーナス2がtrueなら
	// ->ボーナススコアを加算
	if (CGame::GetbBonus_2())
	{
		m_nScore[STATISTICS_POINT_BONUS_2] = BONUS_SCORE;
		// 合格UIを出す
		pScene_Two = CScene_TWO::Create(CScene_TWO::OFFSET_TYPE_CENTER, vec_UiLoad[STATISTICS_CLEAR_BONUS_2].pos, vec_UiLoad[STATISTICS_CLEAR_BONUS_2].size);
		pScene_Two->BindTexture(vec_UiLoad[STATISTICS_CLEAR_BONUS_2].nTexType);
	}
	// ボーナス3がtrueなら
	// ->ボーナススコアを加算
	if (CGame::GetbBonus_3())
	{
		m_nScore[STATISTICS_POINT_BONUS_3] = BONUS_SCORE;
		// 合格UIを出す
		pScene_Two = CScene_TWO::Create(CScene_TWO::OFFSET_TYPE_CENTER, vec_UiLoad[STATISTICS_CLEAR_BONUS_3].pos, vec_UiLoad[STATISTICS_CLEAR_BONUS_3].size);
		pScene_Two->BindTexture(vec_UiLoad[STATISTICS_CLEAR_BONUS_3].nTexType);
	}
		// 合計得点
	for (nCntPoint = STATISTICS_POINT_SCORESOLLIDER; nCntPoint <= STATISTICS_POINT_BONUS_3; nCntPoint++)
	{
		m_nScore[STATISTICS_POINT_ALLSCORE] += m_nScore[nCntPoint];
	}
	// スコアボードの生成
	for (nCntPoint = 0; nCntPoint < STATISTICS_POINT_ALLSCORE; nCntPoint++)
	{
		// スコアボードの生成
		m_pscore_board[nCntPoint] = CScore_board::Create(
			vec_UiLoad[nCntPoint].pos,
			vec_UiLoad[nCntPoint].size,
			m_nScore[nCntPoint]
		);
	}
	// スコアボードの生成
	for (nCntPoint = STATISTICS_POINT_SCORESOLLIDER; nCntPoint < STATISTICS_POINT_ALLSCORE; nCntPoint++)
	{
		// アニメーション設定
		m_pscore_board[nCntPoint]->SetAnim(ANIMETIME);
	}
	// スコアボードの生成
	m_pscore_board[STATISTICS_POINT_ALLSCORE] = CScore_board::Create(
		vec_UiLoad[STATISTICS_POINT_ALLSCORE].pos,
		vec_UiLoad[STATISTICS_POINT_ALLSCORE].size,
		m_nScore[STATISTICS_POINT_ALLSCORE],
		CNumber::TEX_KNOCK
	);
	// アニメーション設定
	m_pscore_board[STATISTICS_POINT_ALLSCORE]->SetAnim(ANIMETIME * 6);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 終了処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CStatistics_point::Uninit(void)
{
	for (int nCnt = 0; nCnt < STATISTICS_POINT_MAX; nCnt++)
	{
		if (m_pscore_board[nCnt] != NULL)
		{
			m_pscore_board[nCnt] = NULL;
		}
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 更新処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CStatistics_point::Update(void)
{
	// フレーム
	m_nFram++;
	m_bAnim = m_pscore_board[STATISTICS_POINT_ALLSCORE]->GetAnimBool();
	if (m_bAnim == true)
	{
		// 6秒経過したら
		// ->合計が一斉に開かれる
		if (m_nFram == ANIMETIME * 6)
		{
			// アニメーション設定
			m_pscore_board[STATISTICS_POINT_ALLSCORE]->Complusion();
			CManager::GetSound()->PlaySound(CSound::LABEL_SE_BOMB);
			m_nFram = 0;
			return;
		}
		// １秒ごとに音を鳴らす
		if (m_nFram % ANIMETIME == 0)
		{
			CManager::GetSound()->PlaySound(CSound::LABEL_SE_SWITCH);
		}
		// 決定ボタンを押すと音が出る
		if (CManager::GetKeyConfig()->GetKeyConfigTrigger(CKeyConfig::CONFIG_DECISION))
		{
			CManager::GetSound()->PlaySound(CSound::LABEL_SE_BOMB);
		}
	}
	else
	{
		// ３秒経過したら
		// ->遷移
		if (m_nFram == ANIMETIME * 5)
		{
			// 画面遷移の状態が遷移していない状態だったら
			if (CManager::GetFade()->GetFade() == CFade::FADE_NONE)
			{
				// 上位のスコアに反映
				CTopscore::SetScore(m_nScore[STATISTICS_POINT_ALLSCORE]);
				// ランキングへ
				CManager::GetFade()->SetFade(CManager::MODE_RANKING);
				// エンター音
				CManager::GetSound()->PlaySound(CSound::LABEL_SE_ENTER);
			}
		}
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CStatistics_point::Draw(void)
{
}

#ifdef _DEBUG
//-------------------------------------------------------------------------------------------------------------
// デバッグ表示
//-------------------------------------------------------------------------------------------------------------
void CStatistics_point::Debug(void)
{
}
#endif // _DEBUG

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込み処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CStatistics_point::Load(void)
{
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだ情報を破棄
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CStatistics_point::UnLoad(void)
{
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 作成処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CStatistics_point * CStatistics_point::Create(void)
{
	// 変数宣言
	CStatistics_point * pStatistics_point;
	// メモリの生成(初め->基本クラス,後->派生クラス)
	pStatistics_point = new CStatistics_point();
	// シーン管理設定
	pStatistics_point->ManageSetting(CScene::LAYER_UI);
	// 初期化処理
	pStatistics_point->Init();
	// 生成したオブジェクトを返す
	return pStatistics_point;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 種類別の処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CStatistics_point::Various(int nCnt)
{

}
