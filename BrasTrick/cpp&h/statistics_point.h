// ----------------------------------------
//
// 総計ポイント処理の説明[statistics_point.h]
// Author : Nishiyama Koki
//
// ----------------------------------------
#ifndef _STATISTICS_POINT_H_
#define _STATISTICS_POINT_H_	 // ファイル名を基準を決める

// ----------------------------------------
//
// インクルードファイル
//
// ----------------------------------------
#include "scene.h"

// ----------------------------------------
//
// マクロ定義
//
// ----------------------------------------

// ----------------------------------------
//
// 前方宣言
//
// ----------------------------------------
class CScore_board;

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CStatistics_point : public CScene
{
public:
	/* 列挙型 */
	// ポイント用
	typedef enum
	{
		STATISTICS_POINT_KOSOLLIDER = 0,
		STATISTICS_POINT_KOARCHER,
		STATISTICS_POINT_KOMERCHANT,
		STATISTICS_POINT_KOBOSS,
		STATISTICS_POINT_SCORESOLLIDER,
		STATISTICS_POINT_SCOREARCHER,
		STATISTICS_POINT_SCOREMERCHANT,
		STATISTICS_POINT_SCOREBOSS,
		STATISTICS_POINT_BONUS_1,
		STATISTICS_POINT_BONUS_2,
		STATISTICS_POINT_BONUS_3,
		STATISTICS_POINT_ALLSCORE,
		STATISTICS_POINT_MAX
	} STATISTICS_POINT;
	// 合格用
	typedef enum
	{
		STATISTICS_CLEAR_BONUS_1 = STATISTICS_POINT_MAX,
		STATISTICS_CLEAR_BONUS_2,
		STATISTICS_CLEAR_BONUS_3,
		STATISTICS_CLEAR_MAX
	} STATISTICS_CLEAR;

	/* 関数 */
	CStatistics_point();
	~CStatistics_point();
	void Init(void);
	void Uninit(void);
	void Update(void);
	void Draw(void);
#ifdef _DEBUG
	void Debug(void);
#endif // _DEBUG
	static HRESULT Load(void);					// 読み込み
	static void UnLoad(void);					// UnLoadする
	static CStatistics_point * Create(void);	// 作成
	int GetAllScore(void) const { return m_nScore[STATISTICS_POINT_ALLSCORE]; };	// 合計スコア
	bool GetAnimBool(void) const { return m_bAnim; };
	void Compusion(void);
protected:

private:
	/* 関数 */
	void Various(int nCnt);									// 種類別処理
																	
	/* 変数 */
	int m_nScore[STATISTICS_POINT_MAX];						// スコアの格納
	CScore_board * m_pscore_board[STATISTICS_POINT_MAX];	// スコアボード
	bool m_bAnim;											// アニメ状態
	int m_nFram;											// フレームカウント
};

#endif