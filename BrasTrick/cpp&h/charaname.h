// ----------------------------------------
//
// キャラクター名処理の説明[charaname.h]
// Author : Nishiyama Koki
//
// ----------------------------------------
#ifndef _CHARANAME_H_
#define _CHARANAME_H_	 // ファイル名を基準を決める

// ----------------------------------------
//
// インクルードファイル
//
// ----------------------------------------
#include "scene_three.h"

// ----------------------------------------
//
// マクロ定義
//
// ----------------------------------------
#define CHARANAME_HP (20)
#define CHARANAME_MP (50)

// ------------------------------------------
//
// クラス
//
// ------------------------------------------
class CCharaname : public CScene_THREE
{
public:
	/* 列挙型 */
	// タイプ
	typedef enum
	{
		TYPE_METAL = 0,		// 商人
		TYPE_GENE,			// 大将
		TYPE_MAX
	} TYPE;

	/* 関数 */
	CCharaname();
	~CCharaname();
	void Init(void);
	void Uninit(void);
	void Update(void);
	void Draw(void);
	static CCharaname * Create(
		TYPE type,
		D3DXVECTOR3 const &pos							// 位置
	);					// 作成
	static HRESULT Load(void);							// 読み込み
	static void UnLoad(void);							// 破棄
	// 設定 //

	// 取得 // 
	int GetRecovery(void);								// 回復量
	TYPE GetType(void);									// タイプ
protected:
private:
	/* 関数 */
	/* 変数 */
	static int m_nTexId[TYPE_MAX];						// テクスチャーID
	static D3DXVECTOR3 m_size[TYPE_MAX];				// サイズ
	TYPE			m_Type;								// タイプ
	int				m_nRecovery;						// 回復量
};
#endif