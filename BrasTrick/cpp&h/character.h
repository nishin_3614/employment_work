// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// キャラクター処理 [character.h]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _CHARACTER_H_
#define _CHARACTER_H_

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// インクルードファイル
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "model.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 前方宣言
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CExtrusion;	// 押し出し判定クラス
class CMeshobit;	// 軌跡クラス
class CCollision;	// 当たり判定クラス

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 構造体
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// クラス
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CCharacter : public CScene
{
public:
	/* 列挙型 */
	// ステータス
	typedef enum
	{
		STATE_NORMAL = 0,			// 通常状態
		STATE_ATTACK,				// 攻撃状態
		STATE_WEAKDAMAGE,			// 弱いダメージ状態
		STATE_DAMAGE,				// ダメージ状態
		STATE_DIE,					// 死亡状態
		STATE_MAX
	} STATE;
	// 基本モーションタイプ
	typedef enum
	{
		MOTIONTYPE_NEUTRAL = 0,		// 待機モーション
		MOTIONTYPE_MOVE,			// 移動モーション
		MOTIONTYPE_DAMAGEACTION,	// ダメージモーション
		MOTIONTYPE_DOWNACTION,		// ダウンモーション
		MOTIONTYPE_STANDUP,			// 起き上がるモーション
		MOTIONTYPE_MAX
	} MOTIONTYPE;
	// キャラクター
	typedef enum
	{
		CHARACTER_PLAYER = 0,		// プレイヤー
		CHARACTER_SOLDIER,			// 兵士
		CHARACTER_ARCHER,			// 弓兵
		CHARACTER_MERCHANT,			// 商人
		CHARACTER_GENERAL,			// 大将
		CHARACTER_MAX
	} CHARACTER;
	// 当たった時の反応
	typedef enum
	{
		BLUST_WEAK = 0,				// 弱い吹っ飛び
		BLUST_STOP,					// その場に止まる
		BLUST_SLASH,				// 吹っ飛ぶ
		BLUST_UP,					// 上に上がる
		BLUST_NEAR,					// 近くに来る
		BLUST_MAX
	} BLUST;
	/* 構造体 */
	// ステータス情報
	typedef struct STATUS
	{
		/* 関数 */
		// コンストラクタ
		STATUS()
		{
			nMaxLife = 0;			// 最大HP
			nMaxMP = 0;				// 最大MP
			nMaxScore = 0;			// スコア
		}
		/* 変数 */
		int			nMaxLife;		// 最大HP
		int			nMaxMP;			// 最大MP
		int			nMaxScore;		// 最大スコア
	} STATUS, *PSTATUS;

	/* 関数 */
	// コンストラクタ
	CCharacter();
	// デストラクタ
	virtual ~CCharacter();
	// 初期化処理
	virtual void Init(void);
	// 終了処理
	virtual void Uninit(void);
	// 更新処理
	virtual void Update(void);
	// 描画処理
	virtual void Draw(void);
#ifdef _DEBUG
	// デバッグ処理
	virtual void Debug(void);
#endif // _DEBUG
	// ダメージを受けた処理
	//	pos		: 位置
	//	bBlust	: 当たった後の判定
	//	nDamage	: ダメージ数
	void AplayDamage(
		D3DXVECTOR3 const &pos,		// 位置
		BLUST		const &bBlust,	// 吹き飛び状態
		int			const &nDamage	// ダメージ量
	);
	// 回復処理
	//	nType	: タイプ
	void Recovery(
		int const &nType		// タイプ
	);
	// 設定 //
	// 位置設定
	//	pos	: 位置
	void SetPos(D3DXVECTOR3 const &pos)							{ m_pos = pos; };
	// 移動量設定
	//	move	: 移動量
	void SetMove(D3DXVECTOR3 const &move)						{ m_move = move; };
	// 回転設定
	//	rot	: 回転
	void SetRot(D3DXVECTOR3 const &rot)							{ m_rot = rot; };
	// 状態設定
	//	state	: 状態
	void SetState(STATE const &state)							{ m_State = state; };
	// 攻撃状態設定
	//	bAttack	: 攻撃状態
	void SetAttack(bool const &bAttack)							{ m_bAttack = bAttack; };
	// 無敵状態設定
	//	bInvi	: 無敵状態
	void SetInvi(bool const bInvi)								{ m_bInvincible = bInvi; };
	// 取得 //
	// 位置取得
	D3DXVECTOR3 &GetPos(void)									{ return m_pos; };
	// 移動量取得
	D3DXVECTOR3 &GetMove(void)									{ return m_move; };
	// 回転取得
	D3DXVECTOR3 &GetRot(void)									{ return m_rot; };
	// 親と子の回転量取得
	//	nModelID	: モデル番号
	D3DXVECTOR3 *GetPartsRot(
		int const nModelID		// モデル番号
	);
	// 親と子の位置取得
	//	nModelID	: モデル番号
	D3DXVECTOR3 * GetPartsPos(
		int const nModelID		// モデル番号
	);
	// 親と子の行列取得
	//	nModelID	: モデル番号(-1:キャラクター行列情報,0〜:モデル行列情報)
	D3DXMATRIX *GetMatrix(
		int const nModelID		// モデル番号
	);
	// ステータス(キャラクターのクラスを基底クラスに持っているクラス)取得
	STATUS const &GetStatus(void) const							{ return m_sStatus[m_character]; };
	// ステータス(キャラクター別)
	//	character:キャラクター番号
	static STATUS const &GetStatus(CHARACTER const &character)	{ return m_sStatus[character]; };
	// 状態
	STATE const &GetState(void) const							{ return m_State; };
	// キャラクター
	CHARACTER const &GetCharacter(void) const					{ return m_character; };
	// 攻撃状態
	bool const &GetAttack(void) const							{ return m_bAttack; };
	// 無敵状態
	bool const &GetInvincible(void) const						{ return m_bInvincible; };
	// 床の高さ
	void GetFloorHeight(void);
	// モーションのフレーム情報取得処理
	//	character	: キャラクター
	//	nMotionID	: モーション番号
	//	nNowKeyCnt	: 現在のキーカウント
	int const &GetMaxFrame(
		CHARACTER const &character,	// キャラクター
		int const &nMotionID,		// モーション番号
		int const &nNowKeyCnt = -1	// 現在のキーカウント
	);
	// カメラ追尾しているID
	static int const &GetCameraCharacter(void);
	// 奥義使用
	static bool const &GetSTrick(void)							{ return m_bsTrickNow; };
	// 全リソースの読み込み
	static HRESULT Load(void);
	// キャラクターの情報読み込み
	static HRESULT Load_Character(void);
	// ステータス情報読み込み
	static HRESULT LoadStatus(void);
	// 全リソースの開放
	static void UnLoad(void);
	// あたり判定の取得
	CCollision * GetCollision(void)								{ return m_pCharacterCollision; };
	// HP取得
	int const &GetHP(void) const								{ return m_nLife; };
	// MP取得
	int const &GetMP(void) const								{ return m_nMP; };
protected:
	/* 関数 */
	// 設定 //
	// キャラクター
	//	character	: キャラクター
	void SetCharacter(CHARACTER const &character)				{ m_character = character; };
	// モーション設定
	//	nMotiontype	: モーションタイプ
	void SetMotion(
		int const &nMotiontype	// モーションタイプ
	);
	// 初期モーション設定
	//	nMotiontype	: モーションタイプ
	void BeginMotion(
		int const &nMotiontype	// モーションタイプ
	);
	// 強制モーション設定
	//	nMotiontype	: モーションタイプ
	void ComplusionSetMotion(
		int const &nMotiontype	// モーションタイプ
	);
	// 重力
	void FagGravity(void);
	// 目標回転量設定
	void SetRotDest(D3DXVECTOR3 const &rotDest)					{ m_rotLast = rotDest; };
	// 奥義中に切り替え
	//	bsTrick	: 秘奥義作動中か作動していないか
	static void SetSTrick(
		bool const &bsTrick	// 秘奥義作動中か作動していないか
	);
	// 取得 //
	// 現在のモーション取得
	int const &GetMotion(void) const							{ return m_nMotiontype; };
	// 過去のモーション取得
	int const &GetOldMotion(void) const							{ return m_nMotiontypeOld; };
	// キーカウント取得
	int const &GetKeyInfoCnt(void) const						{ return m_keyinfoCnt; };
	// 目標回転量取得
	D3DXVECTOR3 const &GetRotDest(void) const					{ return m_rotLast; };
	// 吹っ飛び方
	BLUST const &GetBlust(void) const							{ return m_blust; };

	// モーションカメラの更新
	void MotionCamera(void);
	// ダメージ状態処理
	void DamageState(void);
	// 死の状態処理
	void DieState(void);
	/* 変数 */
	static int	m_nCameraCharacter;						// キャラクターに追尾するID
	STATE m_State;										// 現状のステータス
	bool m_bDamage;										// ダメージが当たった判定
	bool m_bTrick;										// 秘奥義状態
	int	m_nCntState;									// カウントステータス
	int	m_nLife;										// HP
	int	m_nMP;											// MP
private:
	/* 関数 */
	// 通常時の更新処理
	void Update_Normal(void);
	// 秘奥義発動中の更新処理
	void Update_TricActive(void);
	// 演出イベント中の更新処理
	void Update_Apperation(void);
	// ストップ状態中の更新処理
	void Update_StopState(void);
	// キャラクターの描画処理
	void Draw_Character(void);
	// 次のモーション
	void NextKeyMotion(void);
	// 移動処理
	void Move(void);
	// モーション処理
	void Motion(void);
	// 攻撃判定処理
	void AttackCollision(void);
	// 攻撃の無敵判定
	void AttackInvi(void);
	// モデル更新処理
	void ModelUpdate(void);
	// MPアップ処理
	void MpUp(void);
	// カメラ追尾処理
	void TrackCamera(void);
	// 制限区域
	void Limit(void);
	// モーションエフェクト処理
	void Motion_Effect(void);
	// モーション軌跡処理
	void Motion_Obit(void);
	/* 変数 */
	/* 構造体のスタティックにする */
	static MODEL_ALL				*m_modelAll[CHARACTER_MAX];		// モデル全体の情報
	static vector<int>				m_modelId[CHARACTER_MAX];		// モデル番号
	static int						m_NumModel[CHARACTER_MAX];		// 最大モデル数
	static int						m_NumParts[CHARACTER_MAX];		// 動かすモデル数
	static int						m_nCntTrickStop;				// 秘奥義ストップカウント
	static STATUS					m_sStatus[CHARACTER_MAX];		// キャラクターすべてのスタータス情報
	static bool						m_bsTrickNow;					// 秘奥義中
	CModel 							*m_pModel;						// モデル
	CHARACTER						m_character;					// キャラクター
	BLUST							m_blust;						// 吹っ飛び方
	D3DXMATRIX						m_mtxWorld;						// 行列
	D3DXVECTOR3						m_pos;							// 位置
	D3DXVECTOR3						m_posold;						// 前の位置
	D3DXVECTOR3						m_move;							// 移動量
	D3DXVECTOR3						m_BlustMove;					// 吹き飛ばし
	D3DXVECTOR3						m_rot;							// 現在回転量
	D3DXVECTOR3						m_rotLast;						// 向きたい方向
	D3DXVECTOR3						m_rotbetween;					// 回転の差分
	int								m_nMotiontype;					// モーションタイプ
	int								m_nMotiontypeOld;				// 前回のモーションタイプ
	int								m_keyinfoCnt;					// キー情報のカウント
	int								m_nFrame;						// フレームカウント
	int								m_nMotionFrame;					// 一つのモーションのカウント
	int								m_nIdAttackKey;					// 攻撃用のキーID
	int								m_nSlow;						// スロー
	int								m_nIDWho;						// 敵か味方か
	int								m_nCntSound;					// サウンド回数
	bool							m_bAttack;						// 攻撃状態
	bool							m_bInvincible;					// 無敵状態
	bool							m_bMotionCamera;				// モーションカメラの切り替えON・OFF
	bool							m_bAttackSound;					// 攻撃音状態
	CExtrusion						*m_pExtrusion;					// 押し出し
	CCollision						*m_pCharacterCollision;			// キャラクターの当たり判定
	vector<unique_ptr<CCollision>>	m_vec_AttackCollision;			// 攻撃当たり判定
	vector<unique_ptr<CMeshobit>>	m_vec_pMeshObit;				// 奇跡
};

#endif
