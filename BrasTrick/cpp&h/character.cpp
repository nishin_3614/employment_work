// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// キャラクター処理 [character.cpp]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "character.h"
#include "input.h"
#include "3Dparticle.h"
#include "score.h"
#include "item.h"
#include "camera.h"
#include "game.h"
#include "meshobit.h"
#include "ui.h"
#include "Extrusion.h"
#include "rectcollision.h"
#include "spherecollision.h"
#include "columncollision.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#define CHARACTER_GRAVITY			(1.0f)												// 重力
#define CHARACTER_RESISTANCE		(0.5f)												// 抵抗力
#define CHARACTER_STATUS_FILE		("data/LOAD/STATUS/status_manager_Character.csv")	// ステータスを管理しているファイルパス
#define CHARACTER_INFO_FILE			("data/LOAD/CHARACTER/CHARACTER_MANAGER.txt")		// キャラクターファイルパス
#define CHARACTER_EXTRUSIONRADIUS	(20.0f)												// 押し出しの半径
#define CHARACTER_LIMITPOS			(950)												// 制限区域の位置

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数宣言
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
MODEL_ALL			*CCharacter::m_modelAll[CHARACTER_MAX] = {};	// モデル全体の情報
CCharacter::STATUS	CCharacter::m_sStatus[CHARACTER_MAX] = {};		// キャラクターすべてのスタータス情報
vector<int>			CCharacter::m_modelId[CHARACTER_MAX];			// モデル番号
bool				CCharacter::m_bsTrickNow = false;				// 秘奥義中
int					CCharacter::m_NumParts[CHARACTER_MAX] = {};		// 動かすモデル数
int					CCharacter::m_NumModel[CHARACTER_MAX] = {};		// 最大モデル数
int					CCharacter::m_nCameraCharacter = 0;				// キャラクターに追尾するID
int					CCharacter::m_nCntTrickStop = 0;				// 秘奥義ストップカウント

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// オーバーローバーコンストラクタ処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CCharacter::CCharacter() : CScene::CScene()
{
	m_pModel = NULL;								// モデル
	m_character = CHARACTER_PLAYER;					// キャラクター
	m_pos = D3DXVECTOR3(0.0f,0.0f,0.0f);			// 位置
	m_posold = D3DXVECTOR3(0.0f, 0.0f, 0.0f);		// 前の位置
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);			// 移動量
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);			// 現在回転量
	m_rotLast = m_rot;								// 向きたい方向
	m_rotbetween = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	// 回転の差分
	m_nMotiontypeOld = 0;							// 前回のモーションタイプ
	m_nMotiontype = 0;								// モーションタイプ
	m_keyinfoCnt = 0;								// キー情報のカウント
	m_nFrame = 0;									// フレームカウント
	m_nMotionFrame = 0;								// モーション一つののフレームカウント
	m_nSlow = 1;									// スロー
	m_blust = BLUST_STOP;							// 吹っ飛び方
	m_nIDWho = 0;									// 敵か味方か
	m_nCntSound = 0;								// サウンド回数初期化
	m_bTrick = false;								// 秘奥義
	m_State = STATE_NORMAL;							// 現状のステータス
	m_nCntState = 0;								// カウントステータス
	m_nLife = 0;									// HP
	m_nMP = 0;										// MP
	m_nCntTrickStop = 0;							// 秘奥義ストップカウント
	m_bAttack = false;								// 攻撃状態
	m_bInvincible = false;							// 無敵状態
	m_bMotionCamera = false;						// モーションカメラの切り替え
	m_bAttackSound = false;							// 攻撃音
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CCharacter::~CCharacter()
{
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 初期化処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Init()
{
	// モデルのメモリ確保
	m_pModel = new CModel[m_NumParts[m_character]];

	// モデル設定
	for (int nCntModel = 0; nCntModel < m_NumParts[m_character]; nCntModel++)
	{
		// キャラクター情報渡し
		m_pModel[nCntModel].Init();
		// モデルの情報を渡す
		m_pModel[nCntModel].BindModel(
			m_modelAll[m_character]->pModel_offset[nCntModel]
		);
		// モデルの番号設定
		m_pModel[nCntModel].SetModelId(m_modelId[m_character][nCntModel]);
		// キャラクターがプレイヤーなら
		// ->シャドウon
		if (m_character == CHARACTER_PLAYER)
		{
			// シャドウon
			m_pModel[nCntModel].SetShadowMap(true);
		}
		// モーション設定
		m_pModel[nCntModel].BeginMotion(
			m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].Key[nCntModel],
			m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].nFrame * m_nSlow);
		// すべての親以外なら
		// ->親(モデル)情報設定
		if (nCntModel != 0)
		{
			// 親情報設定
			m_pModel[nCntModel].SetParentMtx(
				&m_pModel[m_modelAll[m_character]->pModel_offset[nCntModel].nParent].GetMatrix()
			);
		}
		// それ以外(すべてが親)
		// ->親(キャラクター)情報設定
		else
		{
			// 親情報設定
			m_pModel[nCntModel].SetParentMtx(
				&m_mtxWorld
			);
		}
	}
	// キャラクターがプレイヤーなら
	// ->味方ID(0)に指定
	if (m_character == CHARACTER_PLAYER)
	{
		m_nIDWho = 0;
	}
	// それ以外なら
	// ->敵ID(1)に指定
	else
	{
		m_nIDWho = 1;
	}

	// ステータスの反映
	m_nLife = m_sStatus[m_character].nMaxLife;
	// 初期化
	m_nMP = 0;

	// 軌跡の設定
	for (int nCntObit_Basic = 0; nCntObit_Basic < (signed)m_modelAll[m_character]->v_MeshObitLoad.size(); nCntObit_Basic++)
	{
		// 変数宣言
		MESHOBIT_BASIC * MeshObit_Basic = &m_modelAll[m_character]->v_MeshObitLoad[nCntObit_Basic];	// 軌跡の基本情報を代入
		// 軌跡の生成
		m_vec_pMeshObit.push_back(std::move(CMeshobit::Create_Self(
			MeshObit_Basic->nLine,
			MeshObit_Basic->BeginOffset,
			MeshObit_Basic->EndOffset,
			(CMeshobit::TEX)MeshObit_Basic->nTexType
			)));
	}

	// 押し出し生成
	m_pExtrusion = CExtrusion::Create(
		&m_pos,
		CHARACTER_EXTRUSIONRADIUS
	);
	// 押し出し状態をtrueへ
	m_pExtrusion->SetUse(true);

	// 攻撃当たり判定設定
	for (int nCntAttackCollision = 0; nCntAttackCollision < (signed)m_modelAll[m_character]->v_AttackCollision.size(); nCntAttackCollision++)
	{
		// 変数宣言
		D3DXVECTOR3 pos;															// 位置
		ATTACKCOLLISION * AttackCollision =
			&m_modelAll[m_character]->v_AttackCollision[nCntAttackCollision];	// 攻撃当たり判定の設定を代入
		// 当たり判定の位置の設定
		D3DXVec3TransformCoord(
			&pos,
			&AttackCollision->Offset,
			&m_pModel[AttackCollision->nParts].GetMatrix()
		);
		// 矩形の当たり判定
		if (AttackCollision->p_uni_RectInfo)
		{
			// 矩形の当たり判定
			m_vec_AttackCollision.push_back(std::move(CRectCollision::Create_Self(
				AttackCollision->Offset,
				AttackCollision->p_uni_RectInfo->size
				)));
		}
		// 球の当たり判定
		else if (AttackCollision->p_uni_SphereInfo)
		{
			// 球の当たり判定
			m_vec_AttackCollision.push_back(std::move(CSphereCollision::Create_Self(
				AttackCollision->Offset,
				pos,
				AttackCollision->p_uni_SphereInfo->fRadius
			)));
		}
		// 円柱の当たり判定
		else if (AttackCollision->p_uni_ColumnInfo)
		{
			// 円柱の当たり判定
			m_vec_AttackCollision.push_back(std::move(CColumnCollision::Create_Self(
				AttackCollision->Offset,
				pos,
				AttackCollision->p_uni_ColumnInfo->fRadius,
				AttackCollision->p_uni_ColumnInfo->fVertical
			)));
		}
	}

	// キャラクター当たり判定設定
	if (m_modelAll[m_character]->pCharacterCollision != NULL)
	{
		// 矩形の当たり判定
		m_pCharacterCollision = CRectCollision::Create(
			m_modelAll[m_character]->pCharacterCollision->Offset,
			m_pos + m_modelAll[m_character]->pCharacterCollision->RectInfo->size
		);
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 終了処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Uninit(void)
{
	// モデル情報ヌルチェック
	// ->モデル情報開放
	if (m_pModel != NULL)
	{
		delete[] m_pModel;
		m_pModel = NULL;
	}
	// 押し出し情報ヌルチェック
	// ->押し出し情報開放
	if (m_pExtrusion != NULL)
	{
		m_pExtrusion->Release();
		m_pExtrusion = NULL;
	}
	// キャラクターの当たり判定情報ヌルチェック
	// ->キャラクターの当たり判定情報開放
	if (m_pCharacterCollision != NULL)
	{
		m_pCharacterCollision->Release();
		m_pCharacterCollision = NULL;
	}
	// 軌跡情報開放
	for (int nCntMotionObit = 0; nCntMotionObit < (signed)m_vec_pMeshObit.size(); nCntMotionObit++)
	{
		m_vec_pMeshObit[nCntMotionObit]->Uninit();
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 更新処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Update(void)
{
	// カメラ追尾処理
	TrackCamera();
	// 特殊イベントが商人なら |
	// 特殊イベントが大将なら
	// ->演出更新処理
	if (CGame::GetPerfom() == CGame::PERFOM_MERCHANT ||
		CGame::GetPerfom() == CGame::PERFOM_GENERAL)
	{
		// 演出中更新処理
		Update_Apperation();
	}
	// 秘奥義が発動中なら
	// ->秘奥義発動中更新処理
	else if (m_bsTrickNow == true)
	{
		// 秘奥義発動中更新処理
		Update_TricActive();
	}
	// 状態がゲームオーバーなら |
	// 状態がゲームクリアなら
	// ->状態ストップ更新
	else if (CGame::GetState() == CGame::STATE_GAMEOVER ||
		CGame::GetState() == CGame::STATE_CLEAR)
	{
		// 状態ストップ更新処理
		Update_StopState();
	}
	// 特殊演出が通常なら
	// ->通常時の更新処理
	else if (CGame::GetPerfom() == CGame::PERFOM_NORMAL)
	{
		// 通常時の更新処理
		Update_Normal();
	}
	// !(特殊イベントが商人なら |
	// 特殊イベントが大将なら)
	// ->制限区域処理
	if (!(CGame::GetPerfom() == CGame::PERFOM_GENERAL ||
		CGame::GetPerfom() == CGame::PERFOM_MERCHANT))
	{
		// 制限区域処理
		Limit();
	}
	// 押し出し判定
	m_pExtrusion->CircleCollision();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 通常時の更新処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Update_Normal(void)
{
	// 移動
	Move();
	// 攻撃の無敵判定
	AttackInvi();
	// 攻撃判定
	AttackCollision();
	// 高さ
	GetFloorHeight();
	// モーション
	Motion();
	// エフェクト情報更新
	Motion_Effect();
	// モデル更新
	ModelUpdate();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 秘奥義発動中更新処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Update_TricActive(void)
{
	// 重力処理
	FagGravity();
	// 当たり判定の更新
	m_pCharacterCollision->GetShape()->PassPos(m_pos);
	// キャラクターがプレイヤーなら
	if (m_character == CHARACTER_PLAYER)
	{
		// 攻撃の無敵判定
		AttackInvi();
		// 攻撃判定
		AttackCollision();
	}
	// 高さ
	GetFloorHeight();
	// モーション
	Motion();
	// エフェクト情報更新
	Motion_Effect();
	// モデル更新
	ModelUpdate();
	// キャラクターがプレイヤーなら
	if (m_character == CHARACTER_PLAYER)
	{
		// 秘奥義ストップカウントアップ
		m_nCntTrickStop++;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 演出中の更新処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Update_Apperation(void)
{
	// 重力処理
	FagGravity();
	// 当たり判定の更新
	m_pCharacterCollision->GetShape()->PassPos(m_pos);
	// 高さ
	GetFloorHeight();
	// モーション
	Motion();
	// モデル更新
	ModelUpdate();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 状態ストップ処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Update_StopState(void)
{
	// 重力処理
	FagGravity();
	// 高さ
	GetFloorHeight();
	// モーション
	Motion();
	// モデル更新
	ModelUpdate();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 次のモーション設定処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::NextKeyMotion(void)
{
	for (int nCntModel = 0; nCntModel < m_NumParts[m_character]; nCntModel++)
	{
		// ヌルチェック
		if (&m_pModel[nCntModel] != NULL)
		{
			// モーション設定
			m_pModel[nCntModel].SetMotion(
				m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].Key[nCntModel],
				m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].nFrame * m_nSlow);
		}
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 移動処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Move(void)
{
	// 現在回転差分
	m_rotbetween = m_rotLast - m_rot;

	// 限度調整
	m_rotbetween.x = CCalculation::Rot_One_Limit(m_rotbetween.x);
	m_rotbetween.y = CCalculation::Rot_One_Limit(m_rotbetween.y);
	m_rotbetween.z = CCalculation::Rot_One_Limit(m_rotbetween.z);

	// 回転移動
	m_rot += m_rotbetween * 0.1f;

	// 限度調整
	m_rot.x = CCalculation::Rot_One_Limit(m_rot.x);
	m_rot.y = CCalculation::Rot_One_Limit(m_rot.y);
	m_rot.z = CCalculation::Rot_One_Limit(m_rot.z);

	// 重力処理
	FagGravity();

	// モーションタイプが起き上がりモーション以外なら
	// ->移動する
	if (m_nMotiontype != MOTIONTYPE_STANDUP)
	{
		m_pos.x += m_move.x;
		m_pos.z += m_move.z;
	}
	// 当たり判定の更新
	m_pCharacterCollision->GetShape()->PassPos(m_pos);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// モーション処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Motion(void)
{
	// フレームカウントアップ
	m_nFrame++;
	// モーション全体のフレーム数アップ
	m_nMotionFrame++;
	// モーションタイプが変化したなら
	// ->初期化
	if (m_nMotiontype != m_nMotiontypeOld)
	{
		m_nFrame = 0;		// フレームキー情報のカウント
		m_nMotionFrame = 0;	// モーション一つのフレームカウント
		m_keyinfoCnt = 0;	// キー情報のカウント
		// 次のモーション設定
		NextKeyMotion();
	}
	// モーションの保存
	m_nMotiontypeOld = m_nMotiontype;
	// フレーム数が同じになったら
	if (m_nFrame >= m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].nFrame * m_nSlow)
	{
		// 初期化
		m_nFrame = 0;	// フレーム
		// カウントアップ
		m_keyinfoCnt++;
		// サウンド回数初期化
		m_nCntSound = 0;
		// モーションカメラの切り替えの初期化
		m_bMotionCamera = false;
		// キー情報が超えたら
		if (m_keyinfoCnt >= m_modelAll[m_character]->pMotion[m_nMotiontype]->nNumKey)
		{
			// ループしないとき
			if (m_modelAll[m_character]->pMotion[m_nMotiontype]->nLoop == 0)
			{
				m_nMotiontype = 0;	// モーションタイプ
			}
			// 初期化
			m_keyinfoCnt = 0;				// キー情報
			m_nMotionFrame = 0;				// モーション一つのフレームカウント
			m_bAttack = false;				// 攻撃状態
			m_bTrick = false;				// 秘奥義中
		}
		// 次のモーション設定
		NextKeyMotion();
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 攻撃の判定処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::AttackCollision(void)
{
	// 攻撃状態でなければ
	// ->関数を抜ける
	if (!m_bAttack)
	{
		return;
	}
	// 変数宣言
	MOTION * motion = m_modelAll[m_character]->pMotion[m_nMotiontype];	// モーションの情報代入
	// 範囲内以外なら関数を抜ける
	for (int nCntAtCol = 0; nCntAtCol < (signed)motion->v_Collision.size(); nCntAtCol++)
	{
		if (m_nMotionFrame < motion->v_Collision[nCntAtCol].nBeginFrame ||
			m_nMotionFrame >= motion->v_Collision[nCntAtCol].nEndFrame)
		{
			return;
		}
		// 変数宣言
		CCharacter * pCharacter = NULL;										// キャラクター情報
		int nCollisionID = motion->v_Collision[nCntAtCol].nCollisionID;	// 当たり判定のID
		// 攻撃の当たり判定の行列更新
		m_vec_AttackCollision[nCollisionID]->GetShape()->PassMatrix(
			*GetMatrix(m_modelAll[m_character]->v_AttackCollision[nCollisionID].nParts));
		// 現在攻撃しているキャラクターと他のキャラクターの当たり判定
		for (int nCntLayer = 0; nCntLayer < CScene::GetMaxLayer(CScene::LAYER_CHARACTER); nCntLayer++)
		{
			// 情報取得
			pCharacter = (CCharacter*)CScene::GetScene(CScene::LAYER_CHARACTER, nCntLayer);	// キャラクター
			// 存在しているかどうか |
			// 情報が同じかどうか
			// ->ループスキップ
			if (pCharacter == NULL ||
				pCharacter == this)
			{
				continue;
			}
			// 敵か味方か |
			// 無敵状態ではないか
			// ->ループスキップ
			else if (this->m_nIDWho == pCharacter->m_nIDWho ||
				pCharacter->m_bInvincible)
			{
				continue;
			}
			// 当たり判定が存在しなかったら |
			// キャラクターの当たり判定が存在しなかったら
			// ->ループスキップ
			else if ((signed)m_vec_AttackCollision.size() == 0 ||
				pCharacter->m_pCharacterCollision == NULL)
			{
				continue;
			}
			// 攻撃の当たり判定とキャラクターの当たり判定の判定
			else if (m_vec_AttackCollision[motion->v_Collision[nCntAtCol].nCollisionID]->
				CollisionDetection(pCharacter->m_pCharacterCollision))
			{
				// 攻撃音がoffの場合
				// ->攻撃音を再生する
				if (!m_bAttackSound)
				{
					CManager::GetSound()->PlaySound(CSound::LABEL_SE_SWORDSLASH1);
					m_bAttackSound = true;
				}
				// ダメージ処理
				pCharacter->AplayDamage(
					m_pos,
					(BLUST)motion->KeyInfo[m_keyinfoCnt].nBlust,
					motion->v_Collision[nCntAtCol].nDamage
				);
				// MPアップ
				if (!this->m_bTrick)
				{
					MpUp();
				}
			}
			// 秘奥義中なら |・
			// 相手がダメージ判定なら |
			// 死の状態なら
			else if (this->m_bTrick &&
				(pCharacter->m_State == STATE_DAMAGE ||
					pCharacter->m_State == STATE_DIE))
			{
				// 攻撃の当たり判定とキャラクターの当たり判定の判定
				if (m_vec_AttackCollision[motion->v_Collision[nCntAtCol].nCollisionID]->
					CollisionDetection(pCharacter->m_pCharacterCollision))
				{
					// ダメージ処理
					pCharacter->AplayDamage(
						m_pos,
						(BLUST)motion->KeyInfo[m_keyinfoCnt].nBlust,
						motion->v_Collision[nCntAtCol].nDamage
					);
				}
				// 攻撃が当たる範囲
				else if (CCalculation::Collision_Sphere(
					this->GetPos() + D3DXVECTOR3(0.0, 50.0f, 0.0f),
					150.0f,
					pCharacter->GetPos() + D3DXVECTOR3(0.0, 50.0f, 0.0f),
					50.0f))
				{
					// ダメージ処理
					pCharacter->AplayDamage(
						m_pos,
						(BLUST)motion->KeyInfo[m_keyinfoCnt].nBlust,
						motion->v_Collision[nCntAtCol].nDamage
					);
				}
			}
		}
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 攻撃の無敵判定処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::AttackInvi(void)
{
	// 変数宣言
	MOTION * motion = m_modelAll[m_character]->pMotion[m_nMotiontype];	// モーションの情報代入
	// キーカウントが上限を超えてしまったら
	// 関数を抜ける
	if (motion->nNumKey <= m_keyinfoCnt)
	{
		return;
	}
	// 当たり判定の回数がない場合
	// 関数を抜ける
	else if (motion->KeyInfo[m_keyinfoCnt].nNumCollision == 0)
	{
		return;
	}
	// キー数超えたら
	// 関数を抜ける
	else if (m_keyinfoCnt >= motion->nNumKey)
	{
		return;
	}
	// 当たり判定が無効化されるタイミング
	else if (m_nFrame % motion->KeyInfo[m_keyinfoCnt].nMaxCollisiontime == 0)
	{
		// 変数宣言
		CCharacter * pCharacter = NULL;	// 床
		// 攻撃音をfalseに
		m_bAttackSound = false;
		// 情報取得
		for (int nCntLayer = 0; nCntLayer < CScene::GetMaxLayer(CScene::LAYER_CHARACTER); nCntLayer++)
		{
			// キャラクター情報取得
			pCharacter = (CCharacter*)CScene::GetScene(CScene::LAYER_CHARACTER, nCntLayer);
			// 存在しているかどうか
			// ->ループスキップ
			if (pCharacter == NULL)
			{
				continue;
			}
			// 判定を適用するかどうか
			// ->ループスキップ
			else if (this->m_character == pCharacter->GetCharacter())		// 同族か
			{
				continue;
			}
			// 無敵状態解除
			pCharacter->SetInvi(false);
		}
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// モデルの更新処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::ModelUpdate(void)
{
	// モデル全体の更新
	for (int nCntModel = 0; nCntModel < m_NumParts[m_character]; nCntModel++)
	{
		// モデル情報のヌルチェック
		// ->更新
		if (&m_pModel[nCntModel] != NULL)
		{
			// 更新
			m_pModel[nCntModel].Update();
		}
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// MPアップ状態
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::MpUp(void)
{
	// MPを上げる
	m_nMP++;
	// 上限を超えたら最大MP分代入
	if (m_nMP > m_sStatus[m_character].nMaxMP)
	{
		m_nMP = m_sStatus[m_character].nMaxMP;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// カメラ追尾
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::TrackCamera(void)
{
	// 演出の状態
	switch (CGame::GetPerfom())
	{
		// 演出が通常なら
	case CGame::PERFOM_NORMAL:
		// キャラクターがプレイヤー以外なら
		if (m_character != CHARACTER_PLAYER)
		{
			break;
		}
		// モードがゲームとチュートリアルの場合
		if (CManager::GetMode() == CManager::MODE_GAME ||
			CManager::GetMode() == CManager::MODE_TUTORIAL)
		{
			// カメラの注視点設定
			CManager::GetRenderer()->GetCamera()->SetPosDestRPlayer(
				CCharacter::GetPos(),
				CCharacter::GetRot()
			);
		}
		// モードがタイトルの場合
		else if (CManager::GetMode() == CManager::MODE_TITLE)
		{
			// カメラの注視点設定
			CManager::GetRenderer()->GetCamera()->SetPosDestR(
				CCharacter::GetPos(),
				CCharacter::GetRot()
			);
		}
		// それ以外
		else
		{
			// カメラの注視点設定
			CManager::GetRenderer()->GetCamera()->SetPosR(
				CCharacter::GetPos(),
				CCharacter::GetRot()
			);
		}
		break;
		// 演出が商人なら
	case CGame::PERFOM_MERCHANT:
		// キャラクターが商人以外なら
		// ->ケースを抜ける
		if (m_character != CHARACTER_MERCHANT)
		{
			break;
		}
		// カメラの注視点設定
		CManager::GetRenderer()->GetCamera()->SetPosR(
			CCharacter::GetPos(),
			CCharacter::GetRot()
		);
		break;
		// 演出が大将なら
	case CGame::PERFOM_GENERAL:
		// キャラクターが大将以外なら
		// ->ケースを抜ける
		if (m_character != CHARACTER_GENERAL)
		{
			break;
		}
		// カメラの注視点設定
		CManager::GetRenderer()->GetCamera()->SetPosR(
			CCharacter::GetPos(),
			CCharacter::GetRot()
		);
		break;
		// それ以外
	default:
		break;
	}

}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 制限区域
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Limit(void)
{
	// xの制限
	if (m_pos.x < -CHARACTER_LIMITPOS)
	{
		m_pos.x = -CHARACTER_LIMITPOS;
	}
	if (m_pos.x > CHARACTER_LIMITPOS)
	{
		m_pos.x = CHARACTER_LIMITPOS;
	}
	// zの制限
	if (m_pos.z < -CHARACTER_LIMITPOS)
	{
		m_pos.z = -CHARACTER_LIMITPOS;
	}
	if (m_pos.z > CHARACTER_LIMITPOS)
	{
		m_pos.z = CHARACTER_LIMITPOS;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// モーションエフェクト更新
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Motion_Effect(void)
{
	// 変数宣言
	D3DXVECTOR3 pos;																				// 位置
	KEY_INFO * keyInfo = &m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt];	// キー情報
	// 出現するエフェクトの個数分ループ
	for (int nCntMotionEffect = 0; nCntMotionEffect < (signed)keyInfo->v_MotionEffect.size(); nCntMotionEffect++)
	{
		// 開始時間外なら
		// ->ループスキップ
		if (m_nFrame < keyInfo->v_MotionEffect[nCntMotionEffect].nStart)
		{
			continue;
		}
		// 終了時間外なら
		// ->ループスキップ
		else if (m_nFrame > keyInfo->v_MotionEffect[nCntMotionEffect].nEnd)
		{
			continue;
		}
		// 位置設定
		D3DXVec3TransformCoord(
			&pos,
			&keyInfo->v_MotionEffect[nCntMotionEffect].offset,
			CCharacter::GetMatrix(keyInfo->v_MotionEffect[nCntMotionEffect].nKeyID)
		);
		// パーティクルの生成
		C3DParticle::Create(
			(C3DParticle::PARTICLE_ID)keyInfo->v_MotionEffect[nCntMotionEffect].nParticleType,
			pos);
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// モーション軌跡更新
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Motion_Obit()
{
	// すべての軌跡更新
	for (int nCntMotionObit = 0; nCntMotionObit < (signed)m_modelAll[m_character]->pMotion[m_nMotiontype]->v_MeshObit_detail.size(); nCntMotionObit++)
	{
		// 変数宣言
		MESHOBIT_DETAILS * MeshObit_Detals =
			&m_modelAll[m_character]->pMotion[m_nMotiontype]->v_MeshObit_detail[nCntMotionObit];	// 軌跡の詳細設定情報代入
		// 開始時間外なら
		// ->ループスキップ
		if (m_nMotionFrame < MeshObit_Detals->nBeginFram)
		{
			continue;
		}
		// 終了時間外なら
		// ->ループスキップ
		else if (m_nMotionFrame > MeshObit_Detals->nEndFram)
		{
			continue;
		}
		// 開始時なら
		// ->位置を初期に
		else if (m_nMotionFrame == MeshObit_Detals->nBeginFram)
		{
			m_vec_pMeshObit[MeshObit_Detals->nObitID]->InitPos(*CCharacter::GetMatrix(m_modelAll[m_character]->v_MeshObitLoad[
				MeshObit_Detals->nObitID].nPart));
		}
		// 軌跡の色の設定
		m_vec_pMeshObit[MeshObit_Detals->nObitID]->SetCol(
			MeshObit_Detals->BeginCol,
			MeshObit_Detals->EndCol
		);
		// 軌跡の描画設定
		m_vec_pMeshObit[MeshObit_Detals->nObitID]->DrawSet(*CCharacter::GetMatrix(m_modelAll[m_character]->v_MeshObitLoad[
			MeshObit_Detals->nObitID].nPart));
		// 軌跡の描画
		m_vec_pMeshObit[MeshObit_Detals->nObitID]->Draw();
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// モーションカメラの更新
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::MotionCamera(void)
{
	// 変数宣言
	// 情報取得
	static CCamera * pCamera = CManager::GetRenderer()->GetCamera();	// カメラ
	// カメラ演出の設定
	pCamera->SetPerfom(CCamera::PERFOM_MOTION);
	// モーションカメラの切り替えがOFFの場合
	if (!m_bMotionCamera)
	{
		// 要素があったら処理を行う
		if (m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].pMotionCamera != NULL)
		{
			// カメラモーション設定
			pCamera->SetCamera_Motion(
				m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].pMotionCamera->offsetR + m_pos,
				m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].pMotionCamera->rot + m_rot,
				m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].pMotionCamera->fLength,
				m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].pMotionCamera->fHeight,
				m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].pMotionCamera->fIntertia
			);
			// モーションカメラの切り替え
			m_bMotionCamera = true;
		}
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ダメージ状態
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::DamageState(void)
{
	// ぶっ飛びがスラッシュなら
	// ぶっ飛びが打ち上げなら
	// ->モーションタイプをダウンモーション設定
	if (m_blust == BLUST_SLASH ||
		m_blust == BLUST_UP)
	{
		// モーションタイプをダウンモーションに設定
		m_nMotiontype = MOTIONTYPE_DOWNACTION;
		// ダウン時のパーティクル生成
		C3DParticle::Create(C3DParticle::PARTICLE_ID_SHOCK_SMOKE, m_pos);
	}
	// それ以外なら
	// ->モーションタイプをダメージモーション設定
	else
	{
		m_nMotiontype = MOTIONTYPE_DAMAGEACTION;
	}
	// ゲーム状態がクリアなら |
	// ゲーム状態が死なら
	if (!(CGame::GetState() == CGame::STATE_CLEAR ||
		CGame::GetState() == CGame::STATE_GAMEDIED))
	{
		// 位置更新
		m_pos += m_BlustMove;
	}
	// ステートカウントアップ
	m_nCntState++;
	// カウントステートが1秒なら
	if (m_nCntState == DERAY_TIME(1))
	{
		// 初期化
		m_BlustMove.x = 0.0f;			// 吹き飛び移動量X
		m_BlustMove.z = 0.0f;			// 吹き飛び移動量y
		// あたり判定の始動
		m_pExtrusion->SetUse(true);
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 死の状態
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::DieState(void)
{
	// 攻撃時は入らない
	if (!(CGame::GetState() == CGame::STATE_CLEAR ||
		CGame::GetState() == CGame::STATE_GAMEDIED))
	{
		// 位置更新
		m_pos += m_BlustMove;
	}
	// 吹き飛びがSLASHなら ||
	// 吹き飛びがUPなら
	if (m_blust == BLUST_SLASH ||
		m_blust == BLUST_UP)
	{
		// ダウン時のパーティクル生成
		C3DParticle::Create(C3DParticle::PARTICLE_ID_SHOCK_SMOKE, m_pos);
		// 倒れるモーション
		m_nMotiontype = MOTIONTYPE_DOWNACTION;
	}

	// ステートカウントアップ
	m_nCntState++;
	// カウントステートが40なら
	if (m_nCntState == 40)
	{
		// モーションタイプ設定
		m_nMotiontype = MOTIONTYPE_DOWNACTION;
		// 初期化
		m_BlustMove = D3DVECTOR3_ZERO;	// 吹き飛び移動量
		// あたり判定の始動
		m_pExtrusion->SetUse(true);
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Draw(void)
{
	// 通常演出なら
	if (CGame::GetPerfom() == CGame::PERFOM_NORMAL)
	{
		// キャラクターの描画処理
		Draw_Character();
	}
	// 演出が商人演出なら
	else if (CGame::GetPerfom() == CGame::PERFOM_MERCHANT)
	{
		// キャラクターが商人なら
		// ->関数を抜ける
		if (m_character != CHARACTER_MERCHANT)
		{
			return;
		}
		// キャラクターの描画処理
		Draw_Character();
	}
	// 演出が大将演出なら
	else if (CGame::GetPerfom() == CGame::PERFOM_GENERAL)
	{
		// キャラクターが対象以外なら
		// ->関数を抜ける
		if (m_character != CHARACTER_GENERAL)
		{
			return;
		}
		// キャラクターの描画処理
		Draw_Character();
	}
	// モーション軌跡更新
	Motion_Obit();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// キャラクターの描画処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Draw_Character(void)
{
	// 変数宣言
	D3DXMATRIX
		mtxRot,		// 回転行列
		mtxTrans;	// 位置行列

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 回転を反映 //
	// スケールを反映
	D3DXMatrixRotationYawPitchRoll(&mtxRot,
		m_rot.y,
		m_rot.x,
		m_rot.z);
	// 行列の積(1:ワールド行列 = 2:ワールド行列 * 3:回転行列)
	D3DXMatrixMultiply(&m_mtxWorld,
		&m_mtxWorld, &mtxRot);

	// 位置を反映 //
	// 平行移動行列作成(オフセット)
	D3DXMatrixTranslation(&mtxTrans,	// 総合の入れ物
		m_pos.x,
		m_pos.y,
		m_pos.z);
	// 行列の積(1:ワールド行列 = 2:ワールド行列 * 3:移動行列)
	D3DXMatrixMultiply(
		&m_mtxWorld,	// 1
		&m_mtxWorld,	// 2
		&mtxTrans);		// 3

	// モデルループ
	for (int nCntModel = 0; nCntModel < m_NumParts[m_character]; nCntModel++)
	{
		// モデルのNULLチェック
		if (&m_pModel[nCntModel] != NULL)
		{
			// モデルの描画処理
			m_pModel[nCntModel].Draw();
		}
	}
}

#ifdef _DEBUG
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デバッグ表示
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Debug(void)
{
}
#endif // _DEBUG

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 重力処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::FagGravity(void)
{
	m_move.y -= CHARACTER_GRAVITY;	// 重力
	m_pos.y += m_move.y;			// 位置反映
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 奥義中に切り替え
//	bsTrick	: 秘奥義作動中か作動していないか
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::SetSTrick(
	bool const &bsTrick	// 秘奥義作動中か作動していないか
)
{
	// 秘奥義が発動した瞬間
	if (bsTrick == true &&
		m_bsTrickNow == false)
	{
		// 変数宣言
		CCharacter * pCharacter = NULL;	// キャラクター情報
		D3DXVECTOR3 pos;				// 位置
										// カウントを初期化
		m_nCntTrickStop = 0;
		// レイヤーループ
		for (int nCntLayer = 0; nCntLayer < CScene::GetMaxLayer(CScene::LAYER_CHARACTER); nCntLayer++)
		{
			// 情報取得
			pCharacter = (CCharacter*)CScene::GetScene(CScene::LAYER_CHARACTER, nCntLayer);	// キャラクター
																							// ヌルチェック
			if (pCharacter != NULL)
			{
				// 死亡している奴以外
				if (pCharacter->GetState() != CCharacter::STATE_DIE)
				{
					// モーション設定
					pCharacter->SetMotion(MOTIONTYPE_NEUTRAL);
				}
			}
		}
	}
	// 現在の秘奥義状態
	m_bsTrickNow = bsTrick;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// モーション設定
//	nMotiontype	: モーションタイプ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::SetMotion(
	int const &nMotiontype	// モーションタイプ
)
{
	// ループ状態の時
	if (m_modelAll[m_character]->pMotion[m_nMotiontype]->nLoop == 1)
	{
		// モーションタイプの設定
		m_nMotiontype = nMotiontype;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// モーション設定
//	nMotiontype	: モーションタイプ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::BeginMotion(
	int const &nMotiontype	// モーションタイプ
)
{
	// モーションタイプ設定
	m_nMotiontype = m_nMotiontypeOld = nMotiontype;
	// モデルループ
	for (int nCntModel = 0; nCntModel < m_NumParts[m_character]; nCntModel++)
	{
		// モーション設定
		m_pModel[nCntModel].BeginMotion(
			m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].Key[nCntModel],
			m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].nFrame * m_nSlow);
	}
	// キー数が1以下なら
	if (m_modelAll[m_character]->pMotion[m_nMotiontype]->nNumKey <= 1)
	{
		return;
	}
	// キー情報設定
	m_keyinfoCnt++;
	// モデルループ
	for (int nCntModel = 0; nCntModel < m_NumParts[m_character]; nCntModel++)
	{
		// モーション設定
		m_pModel[nCntModel].SetMotion(
			m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].Key[nCntModel],
			m_modelAll[m_character]->pMotion[m_nMotiontype]->KeyInfo[m_keyinfoCnt].nFrame * m_nSlow);
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 強制モーション設定
//	nMotiontype	: モーションタイプ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::ComplusionSetMotion(
	int const &nMotiontype	// モーションタイプ
)
{
	// モーションタイプの設定
	m_nMotiontype = nMotiontype;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ダメージを受けた処理
//	pos		: 位置
//	bBlust	: 当たった後の判定
//	nDamage	: ダメージ数
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::AplayDamage(
	D3DXVECTOR3 const &pos,
	BLUST		const &bBlust,
	int			const &nDamage
)
{
	// 変数宣言
	D3DXVECTOR3 diffpos;			// 自分と相手との距離
	float	fAngle;					// 角度
	D3DXVECTOR3 ParticlePos;		// パーティクルが出る位置
	// 情報設定
	diffpos = m_pos - pos;			// 自分と相手との距離
	m_move = D3DVECTOR3_ZERO;		// 移動量を初期化
	m_nLife -= nDamage;				// HP減少
	m_blust = bBlust;				// 飛び方
	m_nCntState = 0;				// ステートカウント初期化
	m_State = STATE_DAMAGE;			// ダメージ状態
	m_bInvincible = true;			// 無敵状態
	m_bDamage = true;				// ダメージ判定
	m_nCntSound++;					// サウンド回数加算
	// パーティクルの位置(体の位置)
	ParticlePos = D3DXVECTOR3(
		CCharacter::GetMatrix(1)->_41,
		CCharacter::GetMatrix(1)->_42,
		CCharacter::GetMatrix(1)->_43
	);
	/* 吹っ飛び方 */
	// 弱い攻撃
	if (m_blust == BLUST_WEAK)
	{
		// 状態が通常に
		m_State = STATE_NORMAL;
		// あたり判定の始動
		m_pExtrusion->SetUse(true);
	}
	// ストップ
	else if (m_blust == BLUST_STOP)
	{
		// 吹き飛び_上に上がる
		m_BlustMove.y = 3.0f;			// 反動
	}
	// 横に吹っ飛ぶ
	else if (m_blust == BLUST_SLASH)
	{
		fAngle = (atan2f(diffpos.x, diffpos.z));								// 角度
		m_BlustMove.x = sinf(fAngle) * 13.0f;									// 吹き飛びx
		m_BlustMove.z = cosf(fAngle) * 13.0f;									// 吹き飛びz
		m_BlustMove.y = 18.0f;													// 吹き飛びy
		// あたり判定の終了
		m_pExtrusion->SetUse(false);
		// パーティクル生成
		C3DParticle::Create(C3DParticle::PARTICLE_ID_SHOCKWAVE, ParticlePos);
	}
	// 上に吹っ飛ぶ
	else if (m_blust == BLUST_UP)
	{
		fAngle = (atan2f(diffpos.x, diffpos.z));								// 角度
		m_BlustMove.x = sinf(fAngle) * 5.0f;									// 吹き飛びx
		m_BlustMove.z = cosf(fAngle) * 5.0f;									// 吹き飛びz
		m_BlustMove.y = 20.0f;													// 吹き飛びy
		// あたり判定の終了
		m_pExtrusion->SetUse(false);
	}
	// 攻撃されているほうへ近づく
	else if (m_blust == BLUST_NEAR)
	{
		fAngle = (atan2f(-diffpos.x, -diffpos.z));								// 角度
		m_BlustMove.x = sinf(fAngle) * 0.5f;									// 吹き飛びx
		m_BlustMove.z = cosf(fAngle) * 0.5f;									// 吹き飛びz
		m_BlustMove.y = 7.0f;													// 吹き飛びy
	}
	// 死亡条件
	if (m_nLife < 0)
	{
		m_nLife = 0;
		m_State = STATE_DIE;
	}
	// パーティクル生成
	C3DParticle::Create(
		C3DParticle::PARTICLE_ID_CROSSLINE,
		ParticlePos
	);
	// 叫び声
	if (rand() % 25 == 1)
	{
		// サウンド再生
		CManager::GetSound()->PlaySound(CSound::LABEL_VOICE_DAMAGE);
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 回復処理
//	nType	: タイプ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::Recovery(
	int const &nType	// タイプ
)
{
	// アイテムタイプ
	switch (nType)
	{
		// HP回復
	case CItem::TYPE_LIFE:
		// HPを回復
		m_nLife += ITEM_HP;
		// 上限を超えたら最大HP分代入
		if (m_nLife > m_sStatus[m_character].nMaxLife)
		{
			m_nLife = m_sStatus[m_character].nMaxLife;
		}
		break;
		// 秘奥義回復
	case CItem::TYPE_TRICK:
		// MPを回復
		m_nMP += ITEM_MP;
		// 上限を超えたら最大MP分代入
		if (m_nMP > m_sStatus[m_character].nMaxMP)
		{
			m_nMP = m_sStatus[m_character].nMaxMP;
		}
		break;
	default:
		break;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// フィールドの高さを算出
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::GetFloorHeight(void)
{
	// 地面の当たり判定
	if (m_pos.y <= 0)
	{
		// 初期化
		m_BlustMove = D3DVECTOR3_ZERO;	// 吹き飛び移動量
		m_move.y = 0;					// 移動量y
		m_pos.y = 0;					// 位置y
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// モーションのフレーム情報取得処理
//	character	: キャラクター
//	nMotionID	: モーション番号
//	nNowKeyCnt	: 現在のキーカウント
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int const &CCharacter::GetMaxFrame(
	CHARACTER const &character,	// キャラクター
	int const &nMotionID,		// モーション番号
	int const &nNowKeyCnt		// 現在のキーカウント
)
{
	// モーション全体のフレーム数
	if (nNowKeyCnt == -1)
	{
		return m_modelAll[character]->pMotion[nMotionID]->nAllFrame;
	}
	// 一つのキー間のフレーム数
	return m_modelAll[character]->pMotion[nMotionID]->KeyInfo[nNowKeyCnt].nFrame;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// カメラ追尾処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int const &CCharacter::GetCameraCharacter(void)
{
	return m_nCameraCharacter;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 親と子の回転量取得
//	nModelID	: モデル番号
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
D3DXVECTOR3 *CCharacter::GetPartsRot(
	int const nModelID	// モデル番号
)
{
	// モデルの行列情報取得
	if (nModelID >= 0 ||
		nModelID < m_NumParts[m_character])
	{
		return &m_pModel[nModelID].GetRot();
	}
#ifdef _DEBUG
	CCalculation::Messanger("キャラクターのモデルの回転情報がありません");
#endif // _DEBUG
	// 指定されたIDがない場合
	return &m_rot;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 親と子の位置取得
//	nModelID	: モデル番号
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
D3DXVECTOR3 *CCharacter::GetPartsPos(
	int const nModelID		// モデル番号
)
{
	// モデルの行列情報取得
	if (nModelID >= 0 ||
		nModelID < m_NumParts[m_character])
	{
		return &m_pModel[nModelID].GetPos();
	}
#ifdef _DEBUG
	CCalculation::Messanger("キャラクターのモデルの回転情報がありません");
#endif // _DEBUG
	// 指定されたIDがない場合
	return &m_rot;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 親と子の行列取得
//	nModelID	: モデル番号(-1:キャラクター行列情報,0〜:モデル行列情報)
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
D3DXMATRIX * CCharacter::GetMatrix(int const nModelID)
{
	// モデルの行列情報取得
	if(nModelID >= 0 ||
		nModelID < m_NumParts[m_character])
	{
		return &m_pModel[nModelID].GetMatrix();
	}
	// キャラクターの行列情報取得
	else if (nModelID == -1)
	{
		return &m_mtxWorld;
	}
#ifdef _DEBUG
	CCalculation::Messanger("キャラクターのモデルの行列情報がありません");
#endif // _DEBUG
	// 指定されたIDがない場合
	return &m_mtxWorld;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// キャラクター全リソースの読み込み
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CCharacter::Load(void)
{
	// 変数宣言
	HRESULT hr;
	// キャラクターの情報読み込み
	hr = Load_Character();
	// ステータスの情報読み込み
	hr = LoadStatus();
	return hr;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// キャラクターの情報読み込み
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CCharacter::Load_Character(void)
{
	// ファイルの中身格納用
	std::vector<std::vector<std::string>> vsvec_Contens;
	// ファイルの中身を取得する
	vsvec_Contens = CCalculation::FileContens(CHARACTER_INFO_FILE, ',');
	// 行ごとに回す
	for (int nCntLine = 0; nCntLine < (signed)vsvec_Contens.size(); nCntLine++)
	{
		// 項目ごとに回す
		for (int nCntItem = 0; nCntItem < (signed)vsvec_Contens[nCntLine].size(); nCntItem++)
		{
			switch (nCntItem)
			{
				// パス情報
			case 0:
				// キャラクターの最大数を超えたら
				// ケースを抜ける
				if (nCntLine >= CHARACTER_MAX)
				{
#ifdef _DEBUG
					CCalculation::Messanger("CharacterのLoad_Character関数->キャラクターの最大数を超えました。");
#endif // _DEBUG
					break;
				}
				// キャラクターとモーション情報の生成
				m_modelAll[nCntLine] = new MODEL_ALL;
				// キャラクターのテキストデータの取得
				CModel_info::TextLoad(
					m_modelAll[nCntLine],							// キャラクター情報
					m_modelId[nCntLine],							// キャラクターファイル
					m_NumModel[nCntLine],							// 最大キャラクター数
					m_NumParts[nCntLine],							// 動かすキャラクター数
					vsvec_Contens[nCntLine][nCntItem].c_str()	// ファイル名
				);
				break;
			default:
				break;
			}
		}
	}
	// std::vectorの多重配列開放
	std::vector<std::vector<std::string>>().swap(vsvec_Contens);
	// 成功を返す
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ステータス情報読み込み処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CCharacter::LoadStatus(void)
{
	// 変数宣言
	vector<vector<string>> vsvec_Contens;	// ファイルの中身格納用
	// ファイルの中身を取得する
	vsvec_Contens = CCalculation::FileContens(CHARACTER_STATUS_FILE, ',');
	// 行ごとに回す
	for (int nCntLine = 0; nCntLine < (signed)vsvec_Contens.size(); nCntLine++)
	{
		// キャラクターが上限を超えていたら抜ける
		if (nCntLine >= CHARACTER_MAX)
		{
			break;
		}
		// 項目ごとに回す
		for (int nCntItem = 0; nCntItem < (signed)vsvec_Contens[nCntLine].size(); nCntItem++)
		{
			// アイテムタイプ
			switch (nCntItem)
			{
				// HP
			case 0:
				m_sStatus[nCntLine].nMaxLife = stoi(vsvec_Contens[nCntLine][nCntItem]);
				break;
				// MP
			case 1:
				m_sStatus[nCntLine].nMaxMP = stoi(vsvec_Contens[nCntLine][nCntItem]);
				break;
				// スコア
			case 2:
				m_sStatus[nCntLine].nMaxScore = stoi(vsvec_Contens[nCntLine][nCntItem]);
				break;
			default:
				break;
			}
		}
	}
	// vectorの多重配列開放
	vector<vector<string>>().swap(vsvec_Contens);
	// 成功を返す
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだ情報を破棄処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CCharacter::UnLoad(void)
{
	// キャラクター・モーションの破棄
	for (int nCntCharacter = 0; nCntCharacter < CHARACTER_MAX; nCntCharacter++)
	{
		CModel_info::TextUnload(m_modelAll[nCntCharacter]);
	}
}