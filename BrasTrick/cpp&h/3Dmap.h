// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 3Dマップ処理の説明[3Dmap.h]
// Author : Nishiyama Koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _3DMAP_H_
#define _3DMAP_H_	 // ファイル名を基準を決める

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "scene_X.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class C3DMap
{
public:
	/* 列挙型 */
	// マップステージ
	typedef enum
	{
		MAP_STAGE_1,
		MAP_STAGE_2,
		MAP_STAGE_3,
		MAP_MAX
	} MAP;

	/* 構造体 */
	// オブジェクト
	typedef struct OBJECT
	{
		// コンストラクタ
		OBJECT()
		{
			nModelType = 0;
			pos = D3DVECTOR3_ZERO;
			rot = D3DVECTOR3_ZERO;
		}
		int nModelType;		// モデルタイプ
		D3DXVECTOR3 pos;	// 位置
		D3DXVECTOR3 rot;	// 回転
	} OBJECT;
	// キャラクター
	typedef struct CHARACTER
	{
		// コンストラクタ
		CHARACTER()
		{
			nCharacter = 0;
			pos = D3DVECTOR3_ZERO;
			rot = D3DVECTOR3_ZERO;
		}
		int nCharacter;		// キャラクター番号
		D3DXVECTOR3 pos;	// 位置
		D3DXVECTOR3 rot;	// 回転
	} CHARACTER;
	// ポリゴン
	typedef struct POLYGON
	{
		// コンストラクタ
		POLYGON()
		{
			nTexType = 0;
			pos = D3DVECTOR3_ZERO;
			rot = D3DVECTOR3_ZERO;
			size = D3DVECTOR2_ZERO;
			bBillboard = false;
			bZEneble = false;
			bLighting = false;
			bAlpha = false;
		}
		int nTexType;		// テクスチャータイプ
		D3DXVECTOR3 pos;	// 位置
		D3DXVECTOR3 rot;	// 回転
		D3DXVECTOR2 size;	// サイズ
		bool bBillboard;	// ビルボード状態
		bool bZEneble;		// Zバッファ状態
		bool bLighting;		// ライティング状態
		bool bAlpha;		// α状態
	} POLYGON;
	// 床
	typedef struct FLOOR
	{
		// コンストラクタ
		FLOOR()
		{
			nTexType = 0;
			pos = D3DVECTOR3_ZERO;
			rot = D3DVECTOR3_ZERO;
			nBlockDepth = 0;
			nBlockWidth = 0;
			size = D3DVECTOR2_ZERO;
		}
		int nTexType;		// テクスチャータイプ
		D3DXVECTOR3 pos;	// 位置
		D3DXVECTOR3 rot;	// 回転
		int nBlockDepth;	// 縦のブロック数
		int nBlockWidth;	// 横のブロック数
		D3DXVECTOR2 size;	// サイズ
	} FLOOR;
	// 壁
	typedef struct WALL
	{
		// コンストラクタ
		WALL()
		{
			nTexType = 0;
			pos = D3DVECTOR3_ZERO;
			rot = D3DVECTOR3_ZERO;
			nBlockDepth = 0;
			nBlockWidth = 0;
			size = D3DVECTOR2_ZERO;
		}
		int nTexType;		// テクスチャータイプ
		D3DXVECTOR3 pos;	// 位置
		D3DXVECTOR3 rot;	// 回転
		int nBlockDepth;	// 縦のブロック数
		int nBlockWidth;	// 横のブロック数
		D3DXVECTOR2 size;	// サイズ
	} WALL;

	/* 関数 */
	// コンストラクタ
	C3DMap();
	// デストラクタ
	~C3DMap();
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void);
#endif // _DEBUG
	// 全リソース情報を読み込んだ情報の生成処理
	static HRESULT LoadCreate(MAP const &map);
	// 全リソース情報の読み込み処理
	static HRESULT Load(void);
	// マネージャーテキストファイルの読み込み処理
	static HRESULT ManagerText_Load(void);
	// スクリプトの読み込み処理
	static HRESULT ScriptLoad(void);
	// 全リソース情報の開放
	static void UnLoad(void);
protected:
private:
	/* 関数 */

	/* 変数 */
	static vector<vector<OBJECT>> m_vec_obj;		// オブジェ情報
	static vector<vector<CHARACTER>> m_vec_char;	// キャラクター情報
	static vector<vector<POLYGON>> m_vec_polygon;	// ポリゴン情報
	static vector<vector<FLOOR>> m_vec_floor;		// 床情報
	static vector<vector<WALL>> m_vec_wall;			// 壁情報
	static vector<string> m_vec_String;				// ステージごとのファイルパス情報
};

#endif