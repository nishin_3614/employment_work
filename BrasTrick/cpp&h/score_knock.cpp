// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 倒した数処理の説明[score_knock.cpp]
// Author : Nishiyama Koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* 描画 */
#include "score_knock.h"
#include "number.h"
#include "character.h"
#include "game.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// グローバル変数
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数宣言
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CScore_knock::CScore_knock() : CScene()
{
	for (int nCntNum = 0; nCntNum < MAX_SCORE_KNOCK; nCntNum++)
	{
		m_pNumber[nCntNum] = NULL;
	}
	m_nScore_knock = 0;
	m_pos = D3DVECTOR3_ZERO;
	m_size = D3DVECTOR2_ZERO;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CScore_knock::~CScore_knock()
{
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 初期化処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CScore_knock::Init(void)
{
	// 変数宣言
	int nScore_knock;
	for (int nCnt = 0; nCnt < MAX_SCORE_KNOCK; nCnt++)
	{
		// 桁の数字
		nScore_knock = m_nScore_knock % (int)powf(10.0f, (float)nCnt + 1.0f) / (int)powf(10.0f, (float)nCnt);
		// 数字の作成
		m_pNumber[nCnt] = CNumber::Create(
			nScore_knock,
			m_pos + D3DXVECTOR3(((MAX_SCORE_KNOCK * m_size.x) * 0.5f) - (nCnt * m_size.x) - (m_size.x * 0.5f),0.0f, 0.0f),
			CNumber::TEX_KNOCK,
			D3DXVECTOR2(m_size.x, m_size.y)
		);
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 終了処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CScore_knock::Uninit(void)
{
	for (int nCnt = 0; nCnt < MAX_SCORE_KNOCK; nCnt++)
	{
		if (m_pNumber[nCnt] != NULL)
		{
			m_pNumber[nCnt]->Uninit();
			delete m_pNumber[nCnt];
			m_pNumber[nCnt] = NULL;
		}
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 更新処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CScore_knock::Update(void)
{
	for (int nCnt = 0; nCnt < MAX_SCORE_KNOCK; nCnt++)
	{
		m_pNumber[nCnt]->Update();
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CScore_knock::Draw(void)
{
	// 通常演出出なかったら関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		return;
	}
	// 秘奥義発動中更新処理
	else if (CCharacter::GetSTrick() == true)
	{
		return;
	}
	for (int nCnt = 0; nCnt < MAX_SCORE_KNOCK; nCnt++)
	{
		m_pNumber[nCnt]->Draw();
	}
}

#ifdef _DEBUG
//-------------------------------------------------------------------------------------------------------------
// デバッグ表示
//-------------------------------------------------------------------------------------------------------------
void CScore_knock::Debug(void)
{
}
#endif // _DEBUG


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 倒した数設定処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CScore_knock::SetScore_knock(int nPoint)
{
	// 変数宣言
	int nScore_knock;

	// 現在のポイントを加算
	m_nScore_knock += nPoint;
	for (int nCnt = 0; nCnt < MAX_SCORE_KNOCK; nCnt++)
	{
		// 桁の数字
		nScore_knock = m_nScore_knock % (int)powf(10.0f, (float)nCnt + 1.0f) / (int)powf(10.0f, (float)nCnt);
		m_pNumber[nCnt]->SetNum(nScore_knock);
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 倒した数取得処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int CScore_knock::GetScore_knock(void)
{
	return m_nScore_knock;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 位置情報取得処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
D3DXVECTOR3 CScore_knock::GetPos(void)
{
	return m_pos;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 作成処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CScore_knock * CScore_knock::Create(
	D3DXVECTOR3 const &pos,
	D3DXVECTOR2 const &size
)
{
	// 変数宣言
	CScore_knock * pScore_knock;
	// メモリの生成(初め->基本クラス,後->派生クラス)
	pScore_knock = new CScore_knock();
	// シーン管理設定
	pScore_knock->ManageSetting(CScene::LAYER_UI);
	// 位置情報代入
	pScore_knock->m_pos = pos;
	// サイズ情報代入
	pScore_knock->m_size = size;
	// 初期化処理
	pScore_knock->Init();
	// 生成したオブジェクトを返す
	return pScore_knock;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// スコアの読み込み処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CScore_knock::Load(void)
{
	return S_OK;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだ情報を破棄
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CScore_knock::UnLoad(void)
{

}