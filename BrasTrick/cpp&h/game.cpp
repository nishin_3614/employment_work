// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// ゲーム処理 [game.cpp]
// Author : Nishiyama Koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "game.h"
/* 描画 */
#include "score.h"
#include "score_knock.h"
#include "number.h"
#include "fade.h"
#include "floor.h"
#include "player.h"
#include "soldier.h"
#include "archer.h"
#include "merchant.h"
#include "general.h"
#include "meshobit.h"
#include "meshdome.h"
#include "meshsphere.h"
#include "meshwall.h"
#include "3Deffect.h"
#include "forceline.h"
#include "time.h"
#include "item.h"
#include "topscore.h"
#include "collision.h"
#include "camera.h"
#include "topscore.h"
#include "event_fade.h"
#include "stategauge.h"
#include "statistics.h"
#include "3Dparticle.h"
#include "keyboard.h"
#include "ui_group.h"
#include "Extrusion.h"
#include "3Dmap.h"

/* ポーズ */
#include "pause.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数宣言
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CGame::STATE CGame::m_state = CGame::STATE_NORMAL;		// 状態
CGame::PERFOM	CGame::m_Perfom = CGame::PERFOM_NORMAL;	// 演出
// ボーナスポイント用
bool CGame::m_bBonus_1 = false;				// 100体以上倒したかどうか
bool CGame::m_bBonus_2 = false;				// 全種類の敵を倒したかどうか
bool CGame::m_bBonus_3 = false;				// 隊長を倒したかどうか

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CGame::CGame()
{
	m_pause = NULL;
	m_Time = NULL;
	m_Score = NULL;
	m_Score_knock = NULL;
	m_state = CGame::STATE_NORMAL;
	m_nCntTime = 0;
	m_bBouns_1_Ui = false;
	m_bBouns_2_Ui = false;
	m_bBouns_3_Ui = false;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CGame::~CGame()
{
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 初期化
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CGame::Init(void)
{
	// モードの初期化
	CBaseMode::Init();
	// カメラのタイプ設定
	CManager::GetRenderer()->GetCamera()->SetType(CCamera::TYPE_FOLLOW);
	// 変数宣言
	vector<CUi::UI_LOAD> vec_UiLoad = CUi::GetUi(CUi::UITYPE_GAMEUI_NUMBER);

	/* 初期化 */
	// 静的変数の初期化
	StaticInit();
	m_bBonus_1 = false;				// 100体以上倒したかどうか
	m_bBonus_2 = false;				// 全種類の敵を倒したかどうか
	m_bBonus_3 = false;				// 隊長を倒したかどうか
	// タイム
	m_Time = NULL;
	// スコア
	m_Score = NULL;
	// 倒した数
	m_Score_knock = NULL;
	// 演出
	m_Perfom = CGame::PERFOM_NORMAL;
	// 状態
	m_state = STATE_START;
	// カウントタイム
	m_nCntTime = 0;
	/* 作成 */
	// 3Dエフェクトの生成
	C3DEffect::Create();
	// 球の設定
	CMeshsphere::Create(D3DXVECTOR3(0.0f, 0.0f, 3000.0f),
		10000.0f);
	// 集中線の生成
	CForceline::Create();
	// 集中線の使用状態
	CForceline::Use(true);
	// 3Dマップ生成
	C3DMap::LoadCreate(C3DMap::MAP_STAGE_2);
	// プレイヤー
	CPlayer::Create(D3DXVECTOR3(0.0f,0.0f,0.0f));

	/*
	// アイテム
	CItem::Create(
		CItem::TYPE_LIFE,
		D3DXVECTOR3(100.0f, 100.0f, 100.0f),
		D3DXVECTOR3(100.0f, 100.0f, 0.0f)
	);
	*/
	// ゲームUIの生成(タイムと撃破数とプレイヤーアイコン)
	m_vec_UI = CUi::LoadCreate(CUi::UITYPE_GAMEUI);
	// タイム
	m_Time = CTime::Create(
		vec_UiLoad[UINUMBER_TIME].pos,
		vec_UiLoad[UINUMBER_TIME].size
	);
	// 倒した数
	m_Score_knock = CScore_knock::Create(
		vec_UiLoad[UINUMBER_KNOCK].pos,
		vec_UiLoad[UINUMBER_KNOCK].size
	);
	// スコア
	m_Score = CScore::Create(
		vec_UiLoad[UINUMBER_SCORE].pos,
		vec_UiLoad[UINUMBER_SCORE].size
	);
	// ゲーム目標表示UIの生成
	CUi_group::Create(CUi::UITYPE_GAMEEVENT_GOAL);
	// ポーズの生成
	m_pause = new CPause();
	// ポーズの初期化
	m_pause->Init();
	// タイムストップ設定
	m_Time->SetTimeStop(true);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 終了
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CGame::Uninit(void)
{
	// モードの終了処理
	CBaseMode::Uninit();
	// ポーズ
	if (m_pause != NULL)
	{
		m_pause->Uninit();
		delete m_pause;
		m_pause = NULL;
	}
	// タイム
	m_Time = NULL;
	// スコア
	m_Score = NULL;
	// 倒した数
	m_Score_knock = NULL;
	// 演出
	m_Perfom = CGame::PERFOM_NORMAL;
	// ゲーム状態の初期化
	m_state = STATE_NORMAL;
	// シーンの静的変数の初期化
	StaticInit();
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 更新処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CGame::Update(void)
{
	// モードの更新
	CBaseMode::Update();
	// 通常演出出なかったら関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		for (int nCntUi = 0; nCntUi < (signed)m_vec_UI.size(); nCntUi++)
		{
			m_vec_UI[nCntUi]->SetDrawState(false);
		}
	}
	// 秘奥義発動中更新処理
	else if (CCharacter::GetSTrick() == true)
	{
		for (int nCntUi = 0; nCntUi < (signed)m_vec_UI.size(); nCntUi++)
		{
			m_vec_UI[nCntUi]->SetDrawState(false);
		}
	}
	else
	{
		for (int nCntUi = 0; nCntUi < (signed)m_vec_UI.size(); nCntUi++)
		{
			m_vec_UI[nCntUi]->SetDrawState(true);
		}

	}
	// ゲームオーバー時
	if (m_state == STATE_GAMEOVER)
	{
		// ゲームオーバー時直後なら
		// ->ゲームクリアのUIを生成
		if (m_nCntTime == 0)
		{
			// UI生成
			CUi::LoadCreate(CUi::UITYPE_GAMECLEAR);
			// サウンド再生
			CManager::GetSound()->PlaySound(CSound::LABEL_SE_GAMECLEAR);
		}
		// タイムカウントアップ
		m_nCntTime++;
		// 3秒経過後統計へモード遷移
		if (m_nCntTime >= DERAY_TIME(3))
		{
			if (CManager::GetFade()->GetFade() == CFade::FADE_NONE)
			{
				// 統計へ
				CManager::GetFade()->SetFade(CManager::MODE_STATISTICS);
				// 決定音
				CManager::GetSound()->PlaySound(CSound::LABEL_SE_ENTER);
			}
		}
	}
	// ゲームスタート時
	else if (m_state == STATE_START) {}
	// ゲーム死亡時
	else if (m_state == STATE_GAMEDIED)
	{

		// ゲームオーバー時直後なら
		// ->ゲームクリアのUIを生成
		if (m_nCntTime == 0)
		{
			// UI生成
			CUi::LoadCreate(CUi::UITYPE_GAMEDIED);
			// 全サウンドを消す
			CManager::GetSound()->StopSound();
			// サウンド再生
			CManager::GetSound()->PlaySound(CSound::LABEL_VOICE_DIED);
		}
		// タイムカウントアップ
		m_nCntTime++;
		// 3秒経過後統計へモード遷移
		if (m_nCntTime >= DERAY_TIME(5))
		{
			if (CManager::GetFade()->GetFade() == CFade::FADE_NONE)
			{
				// 統計へ
				CManager::GetFade()->SetFade(CManager::MODE_TITLE);
				// 決定音
				CManager::GetSound()->PlaySound(CSound::LABEL_SE_ENTER);
			}
		}
		// 決定ボタンを押したら
		if (CManager::GetKeyConfig()->GetKeyConfigTrigger(CKeyConfig::CONFIG_DECISION))
		{
			if (CManager::GetFade()->GetFade() == CFade::FADE_NONE)
			{
				// タイトルへ
				CManager::GetFade()->SetFade(CManager::MODE_TITLE);
				// 決定音
				CManager::GetSound()->PlaySound(CSound::LABEL_SE_ENTER);
			}
		}
	}
	// ポーズ状態ならば
	else if (m_state == STATE_PAUSE)
	{
		if (m_pause != NULL)
		{
			m_pause->Update();
		}
	}
	// ポーズ状態ではないとき
	else
	{
		// ボーナス処理
		Bonus();
		// ゲーム内イベント
		GameEvent();
		// ポーズへ
		if (CManager::GetKeyConfig()->GetKeyConfigTrigger(CKeyConfig::CONFIG_POUSE))
		{
			PauseState();
		}

		// ゲーム終了
		if (m_Time->GetTimeOverFlag())
		{
			// ゲームオーバー状態
			m_state = STATE_GAMEOVER;
			// 統計に情報を渡す(スコア情報,倒した数)
			CStatistics::SetGameScore(
				CSoldier::GetKoCount(),
				CArcher::GetKoCount(),
				CMerchant::GetKoCount(),
				CGeneral::GetKoCount()
			);
			// 静的変数の初期化
			StaticInit();
			// ゲームクリアUI
			CUi::LoadCreate(CUi::UITYPE_GAMECLEAR);
		}
#ifdef _DEBUG
		// 隠しコードで統計へ
		if (CManager::GetKeyboard()->GetKeyboardPress(DIK_F3) &&
			CManager::GetKeyboard()->GetKeyboardTrigger(DIK_1))
		{
			if (CManager::GetFade()->GetFade() == CFade::FADE_NONE)
			{
				// 統計に情報を渡す(スコア情報,倒した数)
				CStatistics::SetGameScore(
					100,
					50,
					1,
					1
				);
				// 統計へ
				CManager::GetFade()->SetFade(CManager::MODE_STATISTICS);
				// 決定音
				CManager::GetSound()->PlaySound(CSound::LABEL_SE_ENTER);
			}
		}
#endif // _DEBUG
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画描画
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CGame::Draw(void)
{
	// モードの描画
	CBaseMode::Draw();
	if (m_state == STATE_PAUSE)
	{
		if (m_pause != NULL)
		{
			m_pause->Draw();
		}
	}
	// 通常演出出なかったら関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		// UIの描画状態をfalseへ
		for (int nCntUi = 0; nCntUi < (signed)m_vec_UI.size(); nCntUi++)
		{
			m_vec_UI[nCntUi]->SetDrawState(false);
		}
	}
	else
	{
		// UIの描画状態をfalseへ
		for (int nCntUi = 0; nCntUi < (signed)m_vec_UI.size(); nCntUi++)
		{
			m_vec_UI[nCntUi]->SetDrawState(true);
		}
	}
}

#ifdef _DEBUG
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デバッグ処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CGame::Debug(void)
{
	// ベースモードのデバッグ処理
	CBaseMode::Debug();
	// コントロールキーを押し続ける
	if (CManager::GetKeyboard()->GetKeyboardPress(DIK_LCONTROL))
	{
		// 番号2を押す
		if (CManager::GetKeyboard()->GetKeyboardTrigger(DIK_2))
		{
			// ボーナス1UIがfalseなら
			if (!m_bBouns_1_Ui &&
				!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR2) &&
				!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR3))
			{
				// ボーナス1UIをtrueに
				m_bBouns_1_Ui = true;
				CUi_group::Create(CUi::UITYPE_GAME_ORDERCLEAR1);
			}
			// ボーナス1UIがfalseなら
			if (!m_bBouns_2_Ui &&
				!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR1) &&
				!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR3))
			{
				// ボーナス1UIをtrueに
				m_bBouns_2_Ui = true;
				CUi_group::Create(CUi::UITYPE_GAME_ORDERCLEAR2);
			}
		}
	}
}
#endif // _DEBUG

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 生成処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CGame * CGame::Create(void)
{
	// 変数宣言
	CGame * pGame = new CGame;
	// 初期化処理
	pGame->Init();
	// 生成したオブジェクトを返す
	return pGame;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 静的変数の初期化処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CGame::StaticInit(void)
{
	CEnemy::StaticInit();
	CSoldier::StaticInit();
	CArcher::StaticInit();
	CGeneral::StaticInit();
	CMerchant::StaticInit();
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ポーズ状態処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CGame::PauseState(void)
{
	// ゲーム状態をポーズに
	if (m_state != STATE_PAUSE)
	{
		m_state = STATE_PAUSE;
		// 更新を止める
		CScene::UpdateStop(true);
		// メニュー音
		CManager::GetSound()->PlaySound(CSound::LABEL_SE_MENU);
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ゲーム内イベント処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CGame::GameEvent(void)
{
	// キーボード //
	// 変数宣言
	static int nCntTime = 0;
	// 兵士の出現
	if (CSoldier::GetAllNum() < 10)
	{
		CSoldier::LoadCreate();
	}

	// 弓兵の出現
	if (CArcher::GetAllNum() < 5)
	{
		CArcher::LoadCreate();
	}

	// 通常演出
	if (m_Perfom != PERFOM_NORMAL)
	{
		// カウントタイム
		nCntTime++;
		// 3秒後
		if (nCntTime >= DERAY_TIME(4))
		{
			CEvent_fade::Create(CEvent_fade::EVENT_TYPE_GAMEPLAYER);
		}
	}
	// 商人の出現イベント
	if (m_Time->GetRight(DERAY_TIME(55)))
	{
		CEvent_fade::Create(CEvent_fade::EVENT_TYPE_MERCHANT);
		nCntTime = 0;
	}
	// 大将の出現イベント
	if (m_Time->GetRight(DERAY_TIME(35)))
	{
		CEvent_fade::Create(CEvent_fade::EVENT_TYPE_GENERAL);
		nCntTime = 0;
	}
	// ラスト10秒UI
	if (m_Time->GetRight(DERAY_TIME(10)))
	{
		CUi_group::Create(CUi::UITYPE_GAME_LAST);
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ボーナス処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CGame::Bonus(void)
{
	// ボーナス1がfalseなら
	if (!m_bBonus_1)
	{
		// 100体以上の敵を倒したら
		// ->ボーナス1をtrueに
		if (CEnemy::GetKoDown() >= 100)
		{
			m_bBonus_1 = true;
		}
	}
	// それ以外
	else
	{
		// ボーナス1UIがfalseなら ||
		// UI_指令クリア2が不使用なら ||
		// UI_指令クリア3が不使用なら
		if (!m_bBouns_1_Ui &&
			!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR2) &&
			!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR3))
		{
			// ボーナス1UIをtrueに
			m_bBouns_1_Ui = true;
			// UIを生成
			CUi_group::Create(CUi::UITYPE_GAME_ORDERCLEAR1);
			// サウンド再生
			CManager::GetSound()->PlaySound(CSound::LABEL_SE_ORDERCLEAR);
		}
	}
	// ボーナス2がfalseなら
	if (!m_bBonus_2)
	{
		// 全種類の敵を倒したら
		// ->ボーナス2をtrueに
		if (CSoldier::GetKoCount() > 0 &&
			CArcher::GetKoCount() > 0 &&
			CMerchant::GetKoCount() > 0 &&
			CGeneral::GetKoCount() > 0)
		{
			m_bBonus_2 = true;
		}
	}
	// それ以外
	else
	{
		// ボーナス1UIがfalseなら ||
		// UI_指令クリア1が不使用なら ||
		// UI_指令クリア3が不使用なら
		if (!m_bBouns_2_Ui &&
			!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR1) &&
			!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR3))
		{
			// ボーナス1UIをtrueに
			m_bBouns_2_Ui = true;
			// UIを生成
			CUi_group::Create(CUi::UITYPE_GAME_ORDERCLEAR2);
			// サウンド再生
			CManager::GetSound()->PlaySound(CSound::LABEL_SE_ORDERCLEAR);
		}
	}
	// ボーナス3がfalseなら
	if (!m_bBonus_3)
	{
		// 隊長を倒したら
		// ->ボーナス3をtrueに
		if (CGeneral::GetKoCount() > 0)
		{
			m_bBonus_3 = true;
		}
	}
	// それ以外
	else
	{
		// ボーナス1UIがfalseなら ||
		// UI_指令クリア1が不使用なら ||
		// UI_指令クリア2が不使用なら
		if (!m_bBouns_3_Ui &&
			!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR1) &&
			!CUi::GetUse(CUi::UITYPE_GAME_ORDERCLEAR2))
		{
			// ボーナス1UIをtrueに
			m_bBouns_3_Ui = true;
			// UIを生成
			CUi_group::Create(CUi::UITYPE_GAME_ORDERCLEAR3);
			// サウンド再生
			CManager::GetSound()->PlaySound(CSound::LABEL_SE_ORDERCLEAR);
		}
	}
}
