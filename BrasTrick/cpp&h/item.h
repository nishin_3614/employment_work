// ----------------------------------------
//
// アイテム処理の説明[item.h]
// Author : Nishiyama Koki
//
// ----------------------------------------
#ifndef _ITEM_H_
#define _ITEM_H_	 // ファイル名を基準を決める

// ----------------------------------------
//
// インクルードファイル
//
// ----------------------------------------
#include "scene_three.h"

// ----------------------------------------
//
// マクロ定義
//
// ----------------------------------------
#define ITEM_HP (20)
#define ITEM_MP (50)

// ------------------------------------------
//
// クラス
//
// ------------------------------------------
class CItem : public CScene_THREE
{
public:
	/* 列挙型 */
	// タイプ
	typedef enum
	{
		TYPE_LIFE = 0,		// HP回復
		TYPE_TRICK,			// 秘奥義回復
		TYPE_ATTACK,		// 攻撃力アップ
		TYPE_COLLISION,		// 攻撃範囲アップ
		TYPE_SPEEDUP,		// スピードアップ
		TYPE_MAX
	} TYPE;

	/* 関数 */
	CItem();
	~CItem();
	void Init(void);
	void Uninit(void);
	void Update(void);
	void Draw(void);
	static CItem * Create(
		TYPE type,
		D3DXVECTOR3 const &pos,			// 位置
		D3DXVECTOR3 const &size			// サイズ
	);					// 作成
	static HRESULT Load(void);			// 読み込み
	static void UnLoad(void);			// 破棄

	// 取得 // 
	int GetRecovery(void);				// 回復量
	TYPE GetType(void);					// タイプ
protected:
private:
	static int		m_TexId[TYPE_MAX];	// テクスチャーID
	TYPE			m_Type;				// タイプ
	int				m_nRecovery;		// 回復量
};
#endif