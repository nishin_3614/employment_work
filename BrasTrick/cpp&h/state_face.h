// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 状態_表情処理の説明[state_face.h]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _STATE_FACE_H_
#define _STATE_FACE_H_	 // ファイル名を基準を決める

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "ui.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 前方宣言
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CScene_TWO;

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CState_face : public CScene
{
public:
	/* 列挙型 */
	typedef enum
	{
		STATE_NORMAL = -1,
		STATE_TRICK,
		STATE_PINCH,
		STATE_MAX
	} STATE;

	/* 関数 */
	CState_face();
	~CState_face();
	void Init(void);
	void Uninit(void);
	void Update(void);
	void Draw(void);
	void SelfInit(void);
#ifdef _DEBUG
	void Debug(void);
#endif // _DEBUG
	// 状態_表情の描画可否の設定
	void SetState(STATE const &state, bool const &bState)
		{ m_abState_Face[state] = bState; };
	static HRESULT Load(void);		// 読み込み
	static void UnLoad(void);		// UnLoadする
	// 作成(個人管理)
	static unique_ptr<CState_face> Create_Self(void);
	// 作成(シーン管理)
	static CState_face * Create(void);
protected:

private:
	/* 関数 */
	/* 変数 */
	static int m_nCntFram;							// 選択
	unique_ptr<CScene_TWO> m_apScene[STATE_MAX];	// 状態_表情の2Dポリゴン
	bool m_abState_Face[STATE_MAX];					// 状態_表情の描画可否
};

#endif