// ------------------------------------------
//
// チュートリアル処理 [tutorial.cpp]
// Author : Nishiyama Koki
//
// ------------------------------------------
#include "tutorial.h"
#include "character.h"
#include "3Deffect.h"
#include "floor.h"
#include "meshobit.h"
#include "meshdome.h"
#include "meshsphere.h"
#include "meshwall.h"
#include "player.h"
#include "soldier.h"
#include "ui_group.h"
#include "3Dmap.h"
#include "camera.h"

/* 描画 */
#include "fade.h"

// ------------------------------------------
//
// マクロ定義
//
// ------------------------------------------

// ------------------------------------------
//
// 静的変数宣言
//
// ------------------------------------------
CTutorial::STATE CTutorial::m_state = CTutorial::STATE_NORMAL;			// 状態

// ------------------------------------------
// コンストラクタ
// ------------------------------------------
CTutorial::CTutorial()
{
	m_state = CTutorial::STATE_NORMAL;
	m_bWalk = false;						// プレイヤーが歩いているかどうか
	m_bMPMax = false;						// MPが最大になっているかどうか
	m_bTrick = false;						// 秘奥義を出したかどうか
	m_bAttack = false;						// 攻撃状態
	m_bEnemy = false;						// 敵が出現するかどうか状態
	m_pPlayer = NULL;						// プレイヤー情報
}

// ------------------------------------------
// デストラクタ
// ------------------------------------------
CTutorial::~CTutorial()
{
}

// ------------------------------------------
// 初期化処理
// ------------------------------------------
void CTutorial::Init(void)
{
	// モード初期化
	CBaseMode::Init();

	// カメラのタイプ設定
	CManager::GetRenderer()->GetCamera()->SetType(CCamera::TYPE_FOLLOW);
	/* 作成 */
	m_state = CTutorial::STATE_NORMAL;
	m_bWalk = false;						// プレイヤーが歩いているかどうか
	m_bMPMax = false;						// MPが最大になっているかどうか
	m_bTrick = false;						// 秘奥義を出したかどうか
	m_bAttack = false;						// 攻撃状態
	m_bEnemy = false;						// 敵が出現するかどうか状態
	m_pPlayer = NULL;						// プレイヤー情報
	// 3Dエフェクトの生成
	C3DEffect::Create();
	// 球の設定
	CMeshsphere::Create(D3DXVECTOR3(0.0f, 0.0f, 3000.0f),
		10000.0f);
	// プレイヤー
	m_pPlayer = CPlayer::Create();
	// UIの生成
	CUi_group::Create(CUi::UITYPE_TUTORIAL_WALK);
	// UIの生成
	CUi_group::Create(CUi::UITYPE_TUTORIAL_SKIP);
	// 3Dマップ生成
	C3DMap::LoadCreate(C3DMap::MAP_STAGE_2);
}

// ------------------------------------------
// 終了処理
// ------------------------------------------
void CTutorial::Uninit(void)
{
	// モード終了
	CBaseMode::Uninit();
}

// ------------------------------------------
// 更新処理
// ------------------------------------------
void CTutorial::Update(void)
{
	// モード更新
	CBaseMode::Update();
	if (CManager::GetKeyboard()->GetKeyboardTrigger(DIK_M))
	{
		CSoldier::Create(
			D3DXVECTOR3(100.0f,0.0f,100.0f)
		);
	}
	// プレイヤー情報がNULLではないなら
	if (m_pPlayer != NULL)
	{
		// プレイヤーが動いているなら
		// ->歩いている状態に
		if (m_pPlayer->GetMove().x > 0 ||
			m_pPlayer->GetMove().y > 0)
		{
			m_bWalk = true;
		}
		// プレイヤーが動いているなら
		// ->歩いている状態に
		if (m_pPlayer->GetAttack())
		{
			m_bAttack = true;
		}
		// MPが最大になったら
		// ->MP最大値になっている状態に
		if (m_pPlayer->GetMP() == m_pPlayer->GetStatus().nMaxMP)
		{
			m_bMPMax = true;
		}
		// 秘奥義を打ったなら
		// ->秘奥義を打った状態に
		if (m_pPlayer->GetSTrick() == true)
		{
			m_bTrick = true;
		}
	}
	// 敵出現状態なら
	// ->敵が出現する
	if (m_bEnemy)
	{
		// 兵士の出現
		if (CSoldier::GetAllNum() < 1)
		{
			CSoldier::LoadCreate();
		}
	}
}

// ------------------------------------------
// 描画処理
// ------------------------------------------
void CTutorial::Draw(void)
{
	// モード描画
	CBaseMode::Draw();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 生成処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CTutorial * CTutorial::Create(void)
{
	// 変数宣言
	CTutorial * pTutorial = new CTutorial;
	// 初期化処理
	pTutorial->Init();
	// 生成したオブジェクトを返す
	return pTutorial;
}
