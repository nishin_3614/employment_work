// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// プレイヤー処理 [player.cpp]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "player.h"
#include "floor.h"
#include "input.h"
#include "camera.h"

#include "3Deffect.h"
#include "3Dparticle.h"
#include "meshsphere.h"
#include "collision.h"
#include "ui.h"

#include "2Dgauge.h"
#include "stategauge.h"
#include "item.h"
#include "game.h"
#include "state_face.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#define PLAYER_KEYMOVE				(7)									// 移動キー時の移動量
#define PLAYER_RESISTANCE			(0.5f)								// 抵抗力
#define PLAYER_UI_HP_POS			(D3DXVECTOR3(78.5f, 690.0f, 0.0f))	// UI_HPの位置
#define PLAYER_UI_MP_POS			(D3DXVECTOR3(90.0f, 670.0f, 0.0f))	// UI_MPの位置
#define PLAYER_UI_STATEGUAGE_POS	(D3DXVECTOR3(50.0f, 660.0f, 0.0f))	// UI_状態ゲージの位置

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数宣言
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CPlayer::CPlayer() : CCharacter::CCharacter()
{
	m_nConbo = 0;			// コンボ
	m_nCntState = 0;		// ステートカウント
	m_p2DHPGauge = NULL;	// HPゲージ
	m_bTrick = false;		// 秘奥義中かどうか
	m_p2DTrickGauge = NULL;	// 秘奥義ゲージ
	m_pStateGauge = NULL;	// ゲージ背景
	m_pState_Face = NULL;	// 状態_表情
	m_pSphere = NULL;		// 弾情報
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CPlayer::~CPlayer()
{
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 初期化処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::Init(void)
{
	// 秘奥義設定
	CCharacter::SetSTrick(false);
	// キャラクタータイプ設定
	CCharacter::SetCharacter(CHARACTER_PLAYER);
	// キャラクター初期化
	CCharacter::Init();

	// メッシュスフィアの生成
	m_pSphere = CMeshsphere::Create(
		D3DVECTOR3_ZERO,
		3000.0f,
		10,
		10,
		D3DXCOLOR(0.1f, 0.1f, 0.15f, 1.0f),
		CMeshsphere::TEXTYPE_STAR
	);
	// 使用状態設定
	m_pSphere->SetUse(false);

	// モードがゲームなら ||
	// モードがチュートリアルなら
	if (CManager::GetMode() == CManager::MODE_GAME ||
		CManager::GetMode() == CManager::MODE_TUTORIAL)
	{
		// 通常モーション
		CCharacter::BeginMotion(MOTIONTYPE_NEUTRAL);

		// HPゲージの生成
		m_p2DHPGauge = C2DGauge::Create(
			PLAYER_UI_HP_POS,
			D3DXVECTOR2(500.0f, 25.0f),
			D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f)
		);
		// HPゲージの変化定数を設定
		m_p2DHPGauge->SetConstance((float)CCharacter::GetStatus().nMaxLife);
		// HPゲージのメインカラー設定
		m_p2DHPGauge->SetMainCol(
			D3DXCOLOR(0.0f,1.0f,0.0f,1.0f),
			D3DXCOLOR(0.0f,0.7f,0.3f,1.0f));

		// MPゲージの生成
		m_p2DTrickGauge = C2DGauge::Create(
			PLAYER_UI_MP_POS,
			D3DXVECTOR2(180.0f, 15.0f),
			D3DXCOLOR(0.0f, 0.5f, 1.0f, 1.0f)
		);
		// 秘奥義ゲージの変化定数を設定
		m_p2DTrickGauge->SetConstance((float)CCharacter::GetStatus().nMaxMP);
		// MPゲージの変化定数を設定
		m_p2DTrickGauge->BeginGauge((float)m_nMP);
		// ステートゲージの生成
		m_pStateGauge = CStateGauge::Create(
			PLAYER_UI_STATEGUAGE_POS,
			D3DXVECTOR2(100.0f, 100.0f)
		);
		// 状態_表情の生成
		m_pState_Face = CState_face::Create();
	}
	// タイトル
	else if (CManager::GetMode() == CManager::MODE_TITLE)
	{
		// オープニング演習
		CCharacter::BeginMotion(MOTIONTYPE_OPENNING);
	}
	// ランキング
	else if (CManager::GetMode() == CManager::MODE_RANKING)
	{
		// オープニング演習
		CCharacter::BeginMotion(MOTIONTYPE_RANKINGPAUSE);
	}
	// カメラの注視点設定
	CManager::GetRenderer()->GetCamera()->SetPosR(
		CCharacter::GetPos(),
		CCharacter::GetRot()
	);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 終了処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::Uninit(void)
{
	// キャラクター初期化
	CCharacter::Uninit();
	// ステートゲージの開放
	if (m_pStateGauge != NULL)
	{
		m_pStateGauge = NULL;
	}
	// HPゲージの開放
	if (m_p2DHPGauge != NULL)
	{
		m_p2DHPGauge = NULL;
	}
	// 秘奥義ゲージの開放
	if (m_p2DTrickGauge != NULL)
	{
		m_p2DTrickGauge = NULL;
	}
	// 状態_表情の開放
	if (m_pState_Face != NULL)
	{
		m_pState_Face = NULL;
	}
	// 球の開放
	if (m_pSphere != NULL)
	{
		m_pSphere->Release();
		m_pSphere = NULL;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 更新処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::Update(void)
{
	// 通常演出出なかったら
	// ->関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		return;
	}
	// 状態がゲームオーバーの時 ||
	// 状態がゲームクリアの時 ||
	else if (CGame::GetState() == CGame::STATE_GAMEOVER ||
		CGame::GetState() == CGame::STATE_CLEAR
		)
	{
		// HPゲージの変化定数を設定
		m_p2DHPGauge->ChangeGauge((float)m_nLife);
		// キャラクター更新
		CCharacter::Update();
		// 関数を抜ける
		return;
	}
	// モードがゲームの時 ||
	// モードがチュートリアルの時
	if (CManager::GetMode() == CManager::MODE_GAME ||
		CManager::GetMode() == CManager::MODE_TUTORIAL)
	{
		// 状態が死亡ではないとき ||
		// 状態がダメージではないとき
		if (!(m_State == STATE_DIE ||
			m_State == STATE_DAMAGE))
		{
			// モーション設定
			CCharacter::SetMotion(MOTIONTYPE_NEUTRAL);
			// 攻撃
			Attack();
			// モーション状態
			MotionState();
			// 状態
			State();
			// 攻撃状態ではないとき
			if (CCharacter::GetAttack() == false)
			{
				// 移動処理
				Move();
			}
			// それ以外
			else
			{
			}
		}
		// それ以外
		else
		{
			// 状態処理
			State();
		}
		// MPゲージの変化定数を設定
		m_p2DTrickGauge->ChangeGauge((float)m_nMP);
		// HPゲージの変化定数を設定
		m_p2DHPGauge->ChangeGauge((float)m_nLife);
		// 状態_表情の開放
		if (m_pState_Face != NULL)
		{
			// HPが50%以下なら
			// ->描画状態をtrueに
			if (m_nLife < CCharacter::GetStatus().nMaxLife * 0.5f)
			{
				m_pState_Face->SetState(CState_face::STATE_PINCH,true);
			}
			// それ以外
			// ->描画状態をfalseに
			else
			{
				m_pState_Face->SetState(CState_face::STATE_PINCH, false);
			}
			// 秘奥義が発動中なら
			// ->描画状態をtrueに
			if (m_bTrick)
			{
				m_pState_Face->SetState(CState_face::STATE_TRICK, true);
			}
			// それ以外
			// ->描画状態をfalseに
			else
			{
				m_pState_Face->SetState(CState_face::STATE_TRICK, false);
			}
		}
		// チュートリアルなら
		// ->HP満タン
		if (CManager::GetMode() == CManager::MODE_TUTORIAL)
		{
			m_nLife = CCharacter::GetStatus().nMaxLife;
		}
		// ダメージ状態オフに
		m_bDamage = false;
	}
	// タイトルの演出モーション
	else if (CManager::GetMode() == CManager::MODE_TITLE)
	{
		CCharacter::SetMotion(MOTIONTYPE_OPENNING);
	}
	// キャラクター更新
	CCharacter::Update();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 移動処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::Move(void)
{
	// 変数宣言
	CFloor * pFloor = NULL;			// 床
	D3DXVECTOR3 move, rot;			// 移動量、回転
	bool bMove = false;				// 移動状態
	float fRot;						// 回転
	// 情報取得
	rot = CCharacter::GetRotDest();								// 目的回転量
	move = CCharacter::GetMove();								// 移動量
	fRot = CManager::GetRenderer()->GetCamera()->GetRot().y;	// カメラ回転

	// 移動 //
	/* ジョイパッド */
	// パッド用 //
	int nValueH, nValueV;	// ゲームパッドのスティック情報の取得用
	float fMove;			// 移動速度
	float fAngle;			// スティック角度の計算用変数
	fAngle = 0.0f;			// 角度

	if (CManager::GetJoy() != NULL)
	{
		// ゲームパッドのスティック情報を取得
		CManager::GetJoy()->GetStickLeft(0, nValueH, nValueV);

		/* プレイヤー移動 */
		// ゲームパッド移動
		if (nValueH != 0 || nValueV != 0)
		{
			// 角度の計算
			fAngle = atan2f((float)nValueH, (float)nValueV);

			if (fAngle > D3DX_PI)
			{
				fAngle -= D3DX_PI * 2;
			}
			else if (fAngle < -D3DX_PI)
			{
				fAngle += D3DX_PI * 2;
			}
			// 速度の計算
			if (abs(nValueH) > abs(nValueV))
			{
				fMove = (abs(nValueH) * PLAYER_KEYMOVE) / 1024.0f;
			}
			else
			{
				fMove = (abs(nValueV) * PLAYER_KEYMOVE) / 1024.0f;
			}
			rot.y = fAngle + fRot;

			// スティックの角度によってプレイヤー移動
			move.x -= sinf(fAngle + fRot) * (fMove);
			move.z -= cosf(fAngle + fRot) * (fMove);
			// 移動状態on
			bMove = true;
		}
	}
	/* キーボード */
	// 左
	if (CManager::GetKeyboard()->GetKeyboardPress(DIK_A))
	{
		// 移動状態on
		bMove = true;
		// 奥
		if (CManager::GetKeyboard()->GetKeyboardPress(DIK_W))
		{
			// 回転設定
			rot.y = -D3DX_PI * 0.25f + fRot;
			// 移動量設定
			move.x += sinf(D3DX_PI * 0.75f + fRot) * PLAYER_KEYMOVE;
			move.z += cosf(D3DX_PI * 0.75f + fRot) * PLAYER_KEYMOVE;
		}
		// 手前
		else if (CManager::GetKeyboard()->GetKeyboardPress(DIK_S))
		{
			// 回転設定
			rot.y = -D3DX_PI * 0.75f + fRot;
			// 移動量設定
			move.x += sinf(D3DX_PI * 0.25f + fRot) * PLAYER_KEYMOVE;
			move.z += cosf(D3DX_PI * 0.25f + fRot) * PLAYER_KEYMOVE;
		}
		// 左
		else
		{
			// 回転設定
			rot.y = -D3DX_PI * 0.5f + fRot;
			// 移動量設定
			move.x += sinf(D3DX_PI * 0.5f + fRot) * PLAYER_KEYMOVE;
			move.z += cosf(D3DX_PI * 0.5f + fRot) * PLAYER_KEYMOVE;
		}
	}
	// 右
	else if (CManager::GetKeyboard()->GetKeyboardPress(DIK_D))
	{
		// 移動状態on
		bMove = true;

		// 奥
		if (CManager::GetKeyboard()->GetKeyboardPress(DIK_W))
		{
			// 回転設定
			rot.y = D3DX_PI * 0.25f + fRot;
			// 移動量設定
			move.x += sinf(-D3DX_PI * 0.75f + fRot) * PLAYER_KEYMOVE;
			move.z += cosf(-D3DX_PI * 0.75f + fRot) * PLAYER_KEYMOVE;
		}
		// 手前
		else if (CManager::GetKeyboard()->GetKeyboardPress(DIK_S))
		{
			// 回転設定
			rot.y = D3DX_PI * 0.75f + fRot;
			// 移動量設定
			move.x += sinf(-D3DX_PI * 0.25f + fRot) * PLAYER_KEYMOVE;
			move.z += cosf(-D3DX_PI * 0.25f + fRot) * PLAYER_KEYMOVE;
		}
		// 右
		else
		{
			// 回転設定
			rot.y = D3DX_PI * 0.5f + fRot;
			// 移動量設定
			move.x += sinf(-D3DX_PI * 0.5f + fRot) * PLAYER_KEYMOVE;
			move.z += cosf(-D3DX_PI * 0.5f + fRot) * PLAYER_KEYMOVE;
		}
	}
	// 奥に行く
	else if (CManager::GetKeyboard()->GetKeyboardPress(DIK_W))
	{
		// 移動状態on
		bMove = true;
		// 回転設定
		rot.y = D3DX_PI * 0.0f + fRot;
		// 移動量設定
		move.x += sinf(-D3DX_PI * 1.0f + fRot) * PLAYER_KEYMOVE;
		move.z += cosf(-D3DX_PI * 1.0f + fRot) * PLAYER_KEYMOVE;
	}
	// 手前に行く
	else if (CManager::GetKeyboard()->GetKeyboardPress(DIK_S))
	{
		// 移動状態on
		bMove = true;
		// 回転設定
		rot.y = D3DX_PI * 1.0f + fRot;
		// 移動量設定
		move.x += sinf(D3DX_PI * 0.0f + fRot) * PLAYER_KEYMOVE;
		move.z += cosf(D3DX_PI * 0.0f + fRot) * PLAYER_KEYMOVE;
	}
	// 移動状態なら
	if (bMove == true)
	{
		// モーション設定
		// ->移動モーション
		CCharacter::SetMotion(MOTIONTYPE_MOVE);
	}
	// 移動慣性
	move.x *= PLAYER_RESISTANCE;
	move.z *= PLAYER_RESISTANCE;
	// 移動量設定
	CCharacter::SetMove(move);
	// 目標回転設定
	CCharacter::SetRotDest(rot);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// モーション時の状態処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::MotionState(void)
{
	// 変数宣言
	D3DXVECTOR3 move;
	// モーション時の各処理
	switch (CCharacter::GetMotion())
	{
		// 秘奥義1_常夜禍炎
	case MOTIONTYPE_TRICK1_FIREUPSIDE:
		// キーカウント8
		if (CCharacter::GetKeyInfoCnt() == 9)
		{
			move = CCharacter::GetMove();
			move.y += 1.05f;
			CCharacter::SetMove(move);
		}
		// カメラモーション
		MotionCamera();
		break;
		// 秘奥義2_轟破瞬撃刃
	case MOTIONTYPE_TRICK2_FLASHSTRIKE:
		// カメラモーション
		MotionCamera();
		break;
		// 秘奥義以外の時
	default:
		// 秘奥義状態設定
		m_bTrick = false;
		CCharacter::SetSTrick(m_bTrick);
		// カメラの演出を初期
		CManager::GetRenderer()->GetCamera()->SetPerfom(CCamera::PERFOM_NORMAL);
		// カメラのタイプの設定
		CManager::GetRenderer()->GetCamera()->SetType(CCamera::TYPE_FOLLOW);
		// 球の使用状態設定
		if (m_pSphere != NULL)
		{
			m_pSphere->SetUse(false);
		}
		break;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 攻撃処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::Attack(void)
{
	// 秘奥義中
	// ->関数を抜ける
	if (m_bTrick)
	{
		return;
	}
	// 通常攻撃
	At_Normal();
	// 秘奥義
	Trick();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 通常攻撃処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::At_Normal(void)
{
	// 攻撃中だったら
	// ->関数を抜ける
	if (CCharacter::GetAttack())
	{
		return;
	}
	// インターバルコンボカウントアップ
	m_nInvalConbo++;
	// コンボ初期化
	if (m_nInvalConbo >= 40)
	{
		m_nConbo = 0;
	}
	// キーボード用 //
	// コンボ攻撃
	if (CManager::GetKeyboard()->GetKeyboardTrigger(DIK_RETURN))
	{
		// コンボ攻撃処理
		ConboAttack();
		// 初期化処理
		m_nInvalConbo = 0;	// コンボインターバル
		m_nConbo++;			// コンボ
	}
	// ジョイパッド用 //
	// ジョイパッドがNULLなら
	// ->関数を抜ける
	if (CManager::GetJoy() == NULL)
	{
		return;
	}
	// コンボ攻撃
	if (CManager::GetJoy()->GetTrigger(0, CJoypad::KEY_B))
	{
		// コンボ攻撃処理
		ConboAttack();
		// 初期化処理
		m_nInvalConbo = 0;	// コンボインターバル
		// コンボアップ
		m_nConbo++;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンボ攻撃処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::ConboAttack(void)
{
	// コンボ攻撃
	switch (m_nConbo)
	{
		// 初手コンボ
	case 0:
		// コンボ1_追加入力なし(斜め切り)処理
		At_DiagonalCut();
		break;
		// 1コンボ
	case 1:
		// コンボ2_追加入力(逆切り)処理
		At_ReverseSide();
		break;
		// 2コンボ
	case 2:
		// コンボ3_追加入力(技:円転斬)処理
		At_CircleSlash();
		break;
		// コンボの終了
	default:
		// 初期化
		m_nConbo = 0;				// コンボ
		m_nInvalConbo = 0;			// コンボインターバル
		// コンボ4_追加入力(技:瞬撃斬)処理
		At_InstantSlasher();
		break;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンボ1_追加入力なし(斜め切り)処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::At_DiagonalCut(void)
{
	// 移動設定
	CCharacter::SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	// モーション設定
	CCharacter::SetMotion(MOTIONTYPE_DIAGONALCUT);
	// 攻撃状態設定
	CCharacter::SetAttack(true);
	// 攻撃の音
	CManager::GetSound()->PlaySound(CSound::LABEL_VOICE_ATTCK);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンボ1_追加入力↑(突き)処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::At_Thrust(void)
{
	// 移動設定
	CCharacter::SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンボ2_追加入力(逆切り)処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::At_ReverseSide(void)
{
	// 移動設定
	CCharacter::SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	// モーション設定
	CCharacter::SetMotion(MOTIONTYPE_REVERSESIDE);
	// 攻撃状態設定
	CCharacter::SetAttack(true);
	// 攻撃の音
	CManager::GetSound()->PlaySound(CSound::LABEL_VOICE_ATTCK);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンボ3_追加入力(技:円転斬)処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::At_CircleSlash(void)
{
	// 変数宣言
	D3DXVECTOR3 pos;										// 位置
	D3DXVECTOR3 offset = D3DXVECTOR3(0.0f, 100.0f, 0.0f);	// オフセット
	// 移動設定
	CCharacter::SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	// 位置
	D3DXVec3TransformCoord(
		&pos,
		&offset,
		CCharacter::GetMatrix(15)
	);
	// パーティクル生成
	C3DParticle::Create(
		C3DParticle::PARTICLE_ID_WEAVELINE,
		pos
	);
	// モーション設定
	CCharacter::SetMotion(MOTIONTYPE_CIRCLESLASH);
	// 攻撃状態設定
	CCharacter::SetAttack(true);
	// 技1の音
	CManager::GetSound()->PlaySound(CSound::LABEL_VOICE_SKILL_1);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンボ4_追加入力(技:瞬撃斬)処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::At_InstantSlasher(void)
{
	// 変数宣言
	D3DXVECTOR3 pos;										// 位置
	D3DXVECTOR3 offset = D3DXVECTOR3(0.0f, 100.0f, 0.0f);	// オフセット
	// 移動設定
	CCharacter::SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	// 位置
	D3DXVec3TransformCoord(
		&pos,
		&offset,
		CCharacter::GetMatrix(15)
	);

	// パーティクル生成
	C3DParticle::Create(
		C3DParticle::PARTICLE_ID_WEAVELINE,
		pos
	);
	// モーション設定
	CCharacter::SetMotion(MOTIONTYPE_INSTANTSLASHER);
	// 攻撃状態設定
	CCharacter::SetAttack(true);
	// 技2の音
	CManager::GetSound()->PlaySound(CSound::LABEL_VOICE_SKILL_2);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンボ4_追加入力(技:旋風斬)
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::At_WhirlWind(void)
{
	// 変数宣言
	D3DXVECTOR3 pos;										// 位置
	D3DXVECTOR3 offset = D3DXVECTOR3(0.0f, 100.0f, 0.0f);	// オフセット
	// 移動設定
	CCharacter::SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	// 位置
	D3DXVec3TransformCoord(
		&pos,
		&offset,
		CCharacter::GetMatrix(15)
	);

	// パーティクル生成
	C3DParticle::Create(
		C3DParticle::PARTICLE_ID_WEAVELINE,
		pos
	);
	// モーション設定
	CCharacter::SetMotion(MOTIONTYPE_WHIRLWIND);
	// 攻撃状態設定
	CCharacter::SetAttack(true);
	// 技2の音
	CManager::GetSound()->PlaySound(CSound::LABEL_VOICE_SKILL_2);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 秘奥義処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::Trick(void)
{
	// 秘奥義の条件に満たしていない場合
	// ->関数を抜ける
	if (m_nMP != CCharacter::GetStatus().nMaxMP)
	{
		return;
	}
	// 秘奥義1
	if (CManager::GetKeyConfig()->GetKeyConfigTrigger(CKeyConfig::CONFIG_TRICK1))
	{
		// 秘奥義1処理
		Trick1_FireUpside();
		// 秘奥義使用状態
		m_bTrick = true;
		// 全体の秘奥義使用中
		CCharacter::SetSTrick(m_bTrick);
		// MPを消費し初期化
		m_nMP = 0;
	}
	// 秘奥義2
	else if (CManager::GetKeyConfig()->GetKeyConfigTrigger(CKeyConfig::CONFIG_TRICK2))
	{
		// 秘奥義2処理
		Trick2_FlashStrike();
		// 秘奥義使用状態
		m_bTrick = true;
		// 全体の秘奥義使用中
		CCharacter::SetSTrick(m_bTrick);
		// MPを消費し初期化
		m_nMP = 0;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 秘奥義1処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::Trick1_FireUpside(void)
{
	// 変数宣言
	int nFrame;												// フレーム
	D3DXVECTOR3 pos;										// 位置
	D3DXVECTOR3 offset = D3DXVECTOR3(0.0f,100.0f,0.0f);		// オフセット
	// 指定のモーションの最大フレーム取得
	nFrame = CCharacter::GetMaxFrame(
		CCharacter::CHARACTER_PLAYER,
		MOTIONTYPE_TRICK1_FIREUPSIDE
	);
	// モーション設定
	CCharacter::ComplusionSetMotion(MOTIONTYPE_TRICK1_FIREUPSIDE);
	// 移動設定
	CCharacter::SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	// 攻撃状態設定
	CCharacter::SetAttack(true);
	// 位置
	D3DXVec3TransformCoord(
		&pos,
		&offset,
		CCharacter::GetMatrix(15)
	);
	// パーティクル生成
	C3DParticle::Create(
		C3DParticle::PARTICLE_ID_TRICK_BEFORE_SHOCK,
		pos
	);
	// パーティクル生成
	C3DParticle::Create(
		C3DParticle::PARTICLE_ID_WEAVELINE,
		pos
	);
	// 位置設定
	m_pSphere->Set(pos + D3DXVECTOR3(0.0f, 100.0f, 0.0f),
		nFrame,
		true
	);
	// 秘奥義音
	CManager::GetSound()->PlaySound(CSound::LABEL_VOICE_TRICK1);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 秘奥義2処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::Trick2_FlashStrike(void)
{
	// 変数宣言
	int nFrame;												// フレーム
	D3DXVECTOR3 pos;										// 位置
	D3DXVECTOR3 offset = D3DXVECTOR3(0.0f, 100.0f, 0.0f);	// オフセット
	nFrame = CCharacter::GetMaxFrame(
		CCharacter::CHARACTER_PLAYER,
		MOTIONTYPE_TRICK2_FLASHSTRIKE
	);

	// モーション設定
	CCharacter::ComplusionSetMotion(MOTIONTYPE_TRICK2_FLASHSTRIKE);
	// 移動設定
	CCharacter::SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	// 攻撃状態設定
	CCharacter::SetAttack(true);
	// 位置
	D3DXVec3TransformCoord(
		&pos,
		&offset,
		CCharacter::GetMatrix(15)
	);
	// パーティクル生成
	C3DParticle::Create(
		C3DParticle::PARTICLE_ID_TRICK_BEFORE_SHOCK,
		pos
	);
	// パーティクル生成
	C3DParticle::Create(
		C3DParticle::PARTICLE_ID_WEAVELINE,
		pos
	);
	// 設定
	m_pSphere->Set(pos + D3DXVECTOR3(0.0f, 100.0f, 0.0f),
		nFrame,
		true
	);
	// 秘奥義音
	CManager::GetSound()->PlaySound(CSound::LABEL_VOICE_TRICK2);
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// UIの描画状態設定
//	bState	: 描画状態
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::UiDrawState(
	bool const &bState	// 描画状態
)
{
	// HPゲージの描画
	if (m_p2DHPGauge != NULL)
	{
		m_p2DHPGauge->SetDrawState(bState);
	}
	// 秘奥義ゲージの描画
	if (m_p2DTrickGauge != NULL)
	{
		m_p2DTrickGauge->SetDrawState(bState);
	}
	// ステートゲージの描画
	if (m_pStateGauge != NULL)
	{
		m_pStateGauge->SetDrawState(bState);
	}
	// 状態_表情の描画
	if (m_pState_Face != NULL)
	{
		m_pState_Face->SetDrawState(bState);
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 状態処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::State(void)
{
	// 状態
	switch (m_State)
	{
		// ダメージ
	case STATE_DAMAGE:
		DamageState();
		break;
		// 死
	case STATE_DIE:
		DieState();
		break;
	default:
		break;
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ダメージ状態処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::DamageState(void)
{
	// キャラクターのダメージ状態処理
	CCharacter::DamageState();
	// 30フレーム後
	if (m_nCntState >= 30)
	{
		// 起き上がりモーションへ
		if (CCharacter::GetMotion() == MOTIONTYPE_DOWNACTION)
		{
			CCharacter::SetMotion(MOTIONTYPE_STANDUP);
		}
		// 待機モーションへ
		else
		{
			CCharacter::SetMotion(MOTIONTYPE_NEUTRAL);
		}

		m_nCntState = 0;
		if (m_State != STATE_DIE)
		{
			m_State = STATE_NORMAL;
		}
	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 死処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::DieState(void)
{
	// 倒れるモーション
	CCharacter::SetMotion(MOTIONTYPE_DOWNACTION);
	// ゲームステートを
	CGame::SetState(CGame::STATE_GAMEDIED);
	// キャラクターの死の処理
	CCharacter::DieState();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::ItemCollision(void)
{
	// 変数宣言
	CItem *pItem = NULL;	// アイテム情報

	for (int nCntLayer = 0; nCntLayer < CScene::GetMaxLayer(CScene::LAYER_3DOBJECT); nCntLayer++)
	{
		// 情報取得
		pItem = (CItem*)CScene::GetScene(CScene::LAYER_3DOBJECT, nCntLayer, CItem());	// アイテム
		// アイテムがヌルなら次のループにジャンプする
		if (pItem == NULL)
		{
			continue;
		}
		// 球の当たり判定
		if (CCalculation::Collision_Sphere(
			CCharacter::GetPos(),				// 位置
			100.0f,								// 範囲
			pItem->GetPos(),					// キャラクター位置
			100.0f								// 範囲
		))
		{
			// ダメージ処理
			CCharacter::Recovery(
				pItem->GetType()
			);
		}

	}
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::Draw(void)
{
	CCharacter::Draw();
	// タイトルの場合の場合
	if (CManager::GetMode() == CManager::MODE_TITLE)
	{
		// UIの描画状態をfalseへ
		UiDrawState(false);
		return;
	}
	// 通常演出出なかったら関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		// UIの描画状態をfalseへ
		UiDrawState(false);
		return;
	}
	// UIの描画状態をtrueへ
	UiDrawState(true);
}

#ifdef _DEBUG
//-------------------------------------------------------------------------------------------------------------
// デバッグ表示
//-------------------------------------------------------------------------------------------------------------
void CPlayer::Debug(void)
{
	D3DXVECTOR3 move = CCharacter::GetMove();
	CDebugproc::Print("キーカウント:%d\n",CCharacter::GetKeyInfoCnt());
	if (CManager::GetKeyboard()->GetKeyboardPress(DIK_N))
	{
		D3DXVECTOR3 pos;
		pos = m_p2DTrickGauge->GetPos();
		pos.x += 0.1f;
		m_p2DTrickGauge->SetPos(pos);
	}
	if (CManager::GetKeyboard()->GetKeyboardPress(DIK_M))
	{
		D3DXVECTOR3 pos;
		pos = m_p2DTrickGauge->GetPos();
		pos.x -= 0.1f;
		m_p2DTrickGauge->SetPos(pos);
	}
}
#endif // _DEBUG

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 生成処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CPlayer * CPlayer::Create(
	D3DXVECTOR3 const & pos,
	D3DXVECTOR3 const & rot
)
{
	// 変数宣言
	CPlayer * pPlayer;
	// メモリの生成(初め->基本クラス,後->派生クラス)
	pPlayer = new CPlayer();
	// シーン管理設定
	pPlayer->ManageSetting(CScene::LAYER_CHARACTER);
	// 設定
	pPlayer->SetPos(pos);
	pPlayer->SetRot(rot);
	pPlayer->SetRotDest(rot);
	// 初期化処理
	pPlayer->Init();
	// 生成したオブジェクトを返す
	return pPlayer;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// リソース情報読み込み処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CPlayer::Load(void)
{
	return S_OK;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだリソース情報を破棄処理
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CPlayer::UnLoad(void)
{
}