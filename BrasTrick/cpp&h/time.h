// ------------------------------------------------------------------------------------------
//
// タイム処理 [time.h]
// Author : Nishiyama Koki
//
// ------------------------------------------------------------------------------------------
#ifndef _TIME_H_
#define _TIME_H_
//------------------------------------------------------
//
//	インクルードファイル
//
//--------------------------------------------------------
#include "scene_two.h"
#include "renderer.h"
#include "number.h"

//------------------------------------------------------
//
//	マクロ定義
//
//--------------------------------------------------------
#define TIME_DIGIT (2)//桁数

//------------------------------------------------------
//
//	前方宣言
//
//--------------------------------------------------------

//--------------------------------------------------------
//
//	クラス型の定義
//
//--------------------------------------------------------
class CTime : public CScene
{
public:
	/* 関数 */
	CTime();	
	~CTime();	

	void Init(void);
	void Uninit(void);
	void Update(void);
	void Draw(void);
#ifdef _DEBUG
	void Debug(void);
#endif // _DEBUG
	// 作成
	static CTime * Create(
		D3DXVECTOR3 const &pos,
		D3DXVECTOR2 const &size
	);		// 生成
	// リソースの読み込み
	static HRESULT Load(void);
	// リソースの開放
	static void UnLoad(void);
	void SetPos(D3DXVECTOR3 pos);						// 座標の設定
	void DecreasesTime(void);							// 時間の減少
	int	GetTime(void) const { return m_nTime; };		// 時間の取得
	bool GetRight(int const &nTime)						// 時間の正誤
		{ return (m_nTime == nTime) ? true : false; };
	bool GetTimeOverFlag(void);							// タイムオーバーフラグ
	bool GetTimeStop(void) { return m_bTimeStop; };		// タイムストップ取得
	void SetTimeStop(bool const &bTimeStop)				// タイムストップ設定
	{ m_bTimeStop = bTimeStop; };
private:											   
													   
	//静的変数宣言									 
													   
	//変数宣言		
	CNumber *m_apNumber[TIME_DIGIT];					// 桁数分の変数
	D3DXVECTOR3 m_pos;									// 座標
	D3DXVECTOR2 m_size;									// サイズ
	int m_nTime;										// 時間
	bool m_bTimeOver;									// タイムオーバー
	bool m_bTimeStop;									// タイムストップ
};
#endif