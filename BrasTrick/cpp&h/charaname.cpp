// ----------------------------------------
//
// キャラクター名処理の説明[charaname.cpp]
// Author : Nishiyama Koki
//
// ----------------------------------------

// ----------------------------------------
//
// インクルードファイル
//
// ----------------------------------------
#include "charaname.h"
#include "character.h"
#include "game.h"

// ----------------------------------------
//
// マクロ定義
//
// ----------------------------------------
#define CHARANAMECOLLISION_SIZE (30.0f)

// ----------------------------------------
//
// 静的変数宣言
//
// ----------------------------------------
// テクスチャーID
int CCharaname::m_nTexId[TYPE_MAX] =
{
	36,
	35
};
// サイズ
D3DXVECTOR3 CCharaname::m_size[TYPE_MAX] = {};

// ----------------------------------------
// イニシャライザコンストラクタ
// ----------------------------------------
CCharaname::CCharaname() : CScene_THREE::CScene_THREE()
{
	// 変数の初期化
	m_Type = TYPE_METAL;
}

// ----------------------------------------
// デストラクタ処理
// ----------------------------------------
CCharaname::~CCharaname()
{
}

// ----------------------------------------
// 初期化処理
// ----------------------------------------
void CCharaname::Init(void)
{
	// オフセットタイプ設定
	CScene_THREE::SetOffsetType(OFFSET_TYPE_VERTICAL_UNDER);
	// シーン3Dの初期化
	CScene_THREE::Init();
	CScene_THREE::SetBillboard(true);			// ビルボード
	CScene_THREE::SetAlphaBlend(true);			// αブレンド
	CScene_THREE::SetLighting(true);			// ライティング
	CScene_THREE::SetTexType(m_nTexId[m_Type]);	// テクスチャータイプ
}

// ----------------------------------------
// 終了処理
// ----------------------------------------
void CCharaname::Uninit(void)
{
	CScene_THREE::Uninit();
}

// ----------------------------------------
// 更新処理
// ----------------------------------------
void CCharaname::Update(void)
{
	CScene_THREE::Update();
}

// ----------------------------------------
// 描画処理
// ----------------------------------------
void CCharaname::Draw(void)
{
	// 通常演出出なかったら関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		return;
	}
	// 秘奥義発動中更新処理
	else if (CCharacter::GetSTrick() == true)
	{
		return;
	}

	// 描画
	CScene_THREE::Draw();
}

// ----------------------------------------
// 作成処理(個人管理)
// ----------------------------------------
CCharaname * CCharaname::Create(
	TYPE type,
	D3DXVECTOR3 const &pos							// 位置
)
{
	// 変数宣言
	CCharaname * pCharaname;		// シーン3Dクラス
	// メモリの生成(初め->基本クラス,後->派生クラス)
	pCharaname = new CCharaname();
	// キャラクター名タイプ
	pCharaname->m_Type = type;
	// 位置設定
	pCharaname->SetPos(pos);
	// サイズ設定
	pCharaname->SetSize(m_size[pCharaname->m_Type]);
	// シーン管理
	pCharaname->ManageSetting(CScene::LAYER_3DOBJECT);
	// 初期化処理
	pCharaname->Init();
	// 生成したオブジェクトを返す
	return pCharaname;
}

// ----------------------------------------
// 読み込み処理
// ----------------------------------------
HRESULT CCharaname::Load(void)
{
	// 変数宣言
	D3DXVECTOR3 TexSize[TYPE_MAX] = {};			// テクスチャーサイズ

	// サイズ比率
	float sizeratio = 0.2f;

	// テクスチャー設定
	for (int nCnt = 0; nCnt < TYPE_MAX; nCnt++)
	{
		// サイズ取得
		CCalculation::GetTexSize(CTexture_manager::GetTexture(m_nTexId[nCnt]), &TexSize[nCnt]);
		// サイズ設定
		m_size[nCnt] = sizeratio * TexSize[nCnt];
	}
	return S_OK;
}

// ----------------------------------------
// unload処理
// ----------------------------------------
void CCharaname::UnLoad(void)
{
}

// ----------------------------------------
// 回復処理
// ----------------------------------------
int CCharaname::GetRecovery(void)
{
	return m_nRecovery;
}

// ----------------------------------------
// タイプ処理
// ----------------------------------------
CCharaname::TYPE CCharaname::GetType(void)
{
	return m_Type;
}
