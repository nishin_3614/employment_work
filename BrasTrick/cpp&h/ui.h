// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// ユーザーインターフェース処理 [ui.h]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _UI_H_
#define _UI_H_

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "scene.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 前方宣言
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CScene_TWO;	// 2Dシーンクラス
class C2DPresents;	// 2Dプレゼンツクラス

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CUi : public CScene
{
public:
	/* 列挙型 */
	// UIタイプ
	typedef enum
	{
		UITYPE_GAMEDIED = 0,				// ゲーム死亡
		UITYPE_GAMEEVENT_BOSS,				// ゲームイベント_ボス
		UITYPE_GAMEEVENT_GOAL,				// ゲームイベント_ゴール
		UITYPE_GAMEEVENT_MERCHANT,			// ゲームイベント_商人
		UITYPE_GAMECLEAR,					// ゲームクリア
		UITYPE_GAMEWARNING,					// ゲーム警告
		UITYPE_GAMEUI,						// ゲームUI
		UITYPE_GAMEUI_NUMBER,				// ゲームUI_番号
		UITYPE_PAUSEUI_BG,					// ポーズUI_背景
		UITYPE_PAUSEUI_SELECT,				// ポーズUI_選択肢
		UITYPE_PAUSEUI_CLEAR,				// ポーズUI_クリア
		UITYPE_RANKINGUI,					// ランキングUI
		UITYPE_SETTINGUI_BG,				// 設定UI_背景
		UITYPE_SETTINGUI_SELECT,			// 設定UI_選択肢
		UITYPE_TITLEUI_NAME,				// タイトルUI_名前
		UITYPE_TITLEUI_SELECT,				// タイトルUI_選択肢
		UITYPE_TITLEUI_SELECT_BG,			// タイトルUI_選択肢時の背景
		UITYPE_TITLEUI_TUTORIALSKIP,		// タイトルUI_チュートリアルスキップ
		UITYPE_TITLEUI_TUTORIALSKIP_SELECT,	// タイトルUI_チュートリアルスキップ_背景
		UITYPE_TOTALUI_BG,					// 統計UI_背景
		UITYPE_TOTALUI_NUMBER,				// 統計UI_番号
		UITYPE_TUTORIAL_ATTACK,				// チュートリアル_攻撃
		UITYPE_TUTORIAL_CLEAR,				// チュートリアル_クリア
		UITYPE_TUTORIAL_END,				// チュートリアル_終了
		UITYPE_TUTORIAL_KNOCK,				// チュートリアル_敵を倒す
		UITYPE_TUTORIAL_TRICK,				// チュートリアル_秘奥義
		UITYPE_TUTORIAL_WALK,				// チュートリアル_歩く
		UITYPE_TUTORIAL_SKIP,				// チュートリアル_スキップ
		UITYPE_GAMECHANCE,					// ゲームチャンス
		UITYPE_GAME_ORDERCLEAR1,			// 指令1クリア(100体以上撃破)
		UITYPE_GAME_ORDERCLEAR2,			// 指令2クリア(全種類の敵を撃破)
		UITYPE_GAME_ORDERCLEAR3,			// 指令3クリア(隊長を撃破)
		UITYPE_GAME_LAST,					// ラスト10秒
		UITYPE_MAX							// 最大数
	} UITYPE;
	// UIの情報格納用型名(unique)
	typedef vector<unique_ptr<CUi>> VEC_UNI_UI;
	// UIの情報格納用型名
	typedef vector<CUi*> VEC_P_UI;
	/* 構造体*/
	// 点滅ロード
	typedef struct COOPERATION_COL_LOAD
	{
		// コンストラクタ
		COOPERATION_COL_LOAD()
		{
			changecol = D3DXCOLOR_INI;	// 変わる色
			nTimeSwitch = 0;			// 切り替え時間
			nTimeChange = 0;			// 切り替わる時間
		}
		D3DXCOLOR changecol;	// 変わる色
		int nTimeSwitch;		// 切り替え時間
		int nTimeChange;		// 切り替わる時間
	} COOPERATION_COL_LOAD, *P_COOPERATION_COL_LOAD;
	// 拡大率ロード
	typedef struct COOPERATION_SCAL_LOAD
	{
		// コンストラクタ
		COOPERATION_SCAL_LOAD()
		{
			fChangeScal = 0.0f;	// 拡大率
			nTimeChange = 0;	// 切り替わる時間
		}
		float fChangeScal;		// 拡大率
		int nTimeChange;		// 切り替わる時間
	} COOPERATION_SCAL_LOAD, *P_COOPERATION_SCAL_LOAD;
	// 強調ロード
	typedef struct COOPERATION_LOAD
	{
		// コンストラクタ
		COOPERATION_LOAD()
		{
			pCol = NULL;	// 色
			pScal = NULL;	// 拡大率
		}
		P_COOPERATION_COL_LOAD pCol;		// 色
		P_COOPERATION_SCAL_LOAD pScal;		// 拡大率
	} COOPERATION_LOAD, *P_COOPERATION_LOAD;
	// フェードスタート時間
	typedef int NSTARTFADE, *P_NSTARTFADE;
	// フェード(色指定)ロード
	typedef struct FADE_COL_LOAD
	{
		// コンストラクタ
		FADE_COL_LOAD()
		{
			BeginCol = D3DXCOLOR_INI;	// 初期カラー
			EndCol = D3DXCOLOR_INI;		// 目的カラー
			nMaxfram = 0;				// 最大フレーム
		}
		D3DXCOLOR BeginCol;	// 初期カラー
		D3DXCOLOR EndCol;	// 目的カラー
		int nMaxfram;		// 最大フレーム
	} FADE_COL_LOAD, *P_FADE_COL_LOAD;
	// フェード位置ロード
	typedef struct FADE_POS_LOAD
	{
		// コンストラクタ
		FADE_POS_LOAD()
		{
			BeginPos = D3DVECTOR3_ZERO;	// 初期位置
			EndPos = D3DVECTOR3_ZERO;	// 目的位置
			nMaxfram = 0;				// 最大フレーム
		}
		D3DXVECTOR3 BeginPos;	// 初期位置
		D3DXVECTOR3 EndPos;		// 目的位置
		int nMaxfram;			// 最大フレーム
	} FADE_POS_LOAD, *P_FADE_POS_LOAD;
	// フェードインロード
	typedef struct FADEIN_LOAD
	{
		// コンストラクタ
		FADEIN_LOAD()
		{
			FadeStart = 0;		// フェードスタート時間
			pFadeCol = NULL;	// フェード(色指定)
			pFadePos = NULL;	// フェード(位置)
			bSkip = false;		// フェードのスキップ
		}
		NSTARTFADE FadeStart;		// フェードスタート時間
		P_FADE_COL_LOAD pFadeCol;	// フェード(色指定)
		P_FADE_POS_LOAD pFadePos;	// フェード(位置)
		bool bSkip;					// フェードのスキップ
	} FADEIN_LOAD, *P_FADEIN_LOAD;
	// フェードアウトロード
	typedef struct FADEOUT_LOAD
	{
		// コンストラクタ
		FADEOUT_LOAD()
		{
			FadeStart = 0;		// フェードスタート時間
			pFadeCol = NULL;	// フェード(色指定)
			pFadePos = NULL;	// フェード(位置)
			bSkip = false;		// フェードのスキップ
		}
		NSTARTFADE FadeStart;		// フェードスタート時間
		P_FADE_COL_LOAD pFadeCol;	// フェード(色指定)
		P_FADE_POS_LOAD pFadePos;	// フェード(位置)
		bool bSkip;					// フェードのスキップ
	} FADEOUT_LOAD, *P_FADEOUT_LOAD;
	// 2DPresentロード
	typedef struct PRESENTS_LOAD
	{
		// コンストラクタ
		PRESENTS_LOAD()
		{
			pFadeIn = NULL;			// フェードインのロード
			pFadeOut = NULL;		// フェードアウトのロード
			pCooperation = NULL;	// 強調のロード
		}
		P_FADEIN_LOAD pFadeIn;				// フェードインのロード
		P_FADEOUT_LOAD pFadeOut;			// フェードアウトのロード
		P_COOPERATION_LOAD pCooperation;	// 強調のロード
	} PRESENTS_LOAD, *P_PRESENTS_LOAD;
	// UIロード
	typedef struct UI_LOAD
	{
		// コンストラクタ
		UI_LOAD()
		{
			pos = D3DVECTOR3_ZERO;			// 位置
			col = D3DXCOLOR_INI;			// 色
			size = D3DVECTOR2_ZERO;			// サイズ
			tex_first = D3DVECTOR2_ZERO;	// 最初のテクスチャー座標
			tex_last = { 1.0f,1.0f };		// 最後のテクスチャー座標
			fRot = 0.0f;					// 角度
			nTexType = 0;					// テクスチャータイプ
			nOffsetType = 0;				// オフセット
			bUse = false;					// 使用状態
			pPresents = NULL;				// 2Dプレゼンツのロード
		}
		D3DXVECTOR3 pos;			// 位置
		D3DXCOLOR col;				// 色
		D3DXVECTOR2 size;			// サイズ
		D3DXVECTOR2 tex_first;		// 最初のテクスチャー座標
		D3DXVECTOR2 tex_last;		// 最後のテクスチャー座標
		float fRot;					// 角度
		int nTexType;				// テクスチャータイプ
		int nOffsetType;			// オフセット
		bool bUse;					// 使用状態
		P_PRESENTS_LOAD pPresents;	// 2Dプレゼンツのロード
	} UI_LOAD, *P_UI_LOAD;

	/* 関数 */
	// コンストラクタ
	CUi();
	// デストラクタ
	~CUi();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void);
#endif // _DEBUG
	// シーン2Dの取得
	CScene_TWO * GetScene_Two(void);
	// プレゼンツ2Dの取得
	C2DPresents * GetPresents(void);
	// 全リソース情報の読み込み
	static HRESULT Load(void);
	// UIマネージャーの読み込み
	static HRESULT UiManagerLoad(void);
	// UIスクリプトの読み込み
	static HRESULT UiScriptLoad(void);
	// 全リソース情報の開放
	static void UnLoad(void);
	// 生成処理(シーン管理)
	//	Uitype	: UIタイプ
	//	nUi		: UI番号
	static CUi * Create(
		UITYPE const &Uitype,	// UIタイプ
		int const &nUi			// UI番号
	);
	// 生成処理(個人管理)
	//	Uitype	: UIタイプ
	//	nUi		: UI番号
	static unique_ptr<CUi> Create_Self(
		UITYPE const &Uitype,	// UIタイプ
		int const &nUi			// UI番号
	);
	// 読み込んだUI情報の生成する(シーン管理)
	//	Uitype	: UIタイプ
	static CUi::VEC_P_UI LoadCreate(
		UITYPE const &Uitype	// UIタイプ
	);
	// 読み込んだUI情報の生成する(個人管理) ※返り値の変数はstd::moveで返す
	//	Uitype	: UIタイプ
	static VEC_UNI_UI LoadCreate_Self(
		UITYPE const &Uitype	// UIタイプ
	);
	// 使用状態取得
	//	Uitype	: UIタイプ
	static bool GetUse(
		UITYPE const &Uitype	// UIタイプ
	) { return m_sta_UiUse[Uitype]; };
	// 使用状態設定
	//	bUse	: 使用状態
	//	Uitype	: UIタイプ
	static void SetUse(
		bool const &bUse,		// 使用状態
		UITYPE const &Uitype	// UIタイプ
	) { m_sta_UiUse[Uitype] = bUse; };
	// 指定したUIの情報取得
	//	Uitype	: UIタイプ
	static vector<UI_LOAD> GetUi(
		UITYPE const &Uitype	// UIタイプ
	) { return m_vec_UiLoad[Uitype]; };
protected:
private:
	/* 列挙型 */

	/* 関数 */

	/* 変数 */
	static vector<UI_LOAD> m_vec_UiLoad[UITYPE_MAX];	// Ui読み込み用変数
	static bool	m_sta_UiUse[UITYPE_MAX];				// このUIの使用状態
	static vector<string> m_vec_String;					// ファイル情報読み書き用
	CScene_TWO * m_pScene_two;							// シーン2D
	C2DPresents * m_pPresents;							// 2Dプレゼンツ
	UITYPE m_Uitype;									// UIタイプ
	int m_nUi;											// UI
};
#endif
