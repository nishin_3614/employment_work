// ------------------------------------------------------------------------------------------
//
// タイム処理 [time.cpp]
// Author : Nishiyama Koki
//
// ------------------------------------------------------------------------------------------

//--------------------------------------------------------
//
// インクルードファイル
//
//--------------------------------------------------------
#include "main.h"
#include "time.h"
#include "scene.h"
#include "number.h"
#include "game.h"
#include "character.h"

//--------------------------------------------------------
//
// マクロ定義
//
//--------------------------------------------------------

//--------------------------------------------------------
//
// 静的変数初期化
//
//--------------------------------------------------------

//------------------------------------------------------
// コンストラクタ
//--------------------------------------------------------
CTime::CTime(void) : CScene()
{
	m_nTime = DERAY_TIME(99);
	m_bTimeOver = false;
	m_bTimeStop = false;
	m_pos = D3DVECTOR3_ZERO;	// 位置情報
	m_size = D3DVECTOR2_ZERO;	// サイズ情報

}
//------------------------------------------------------
// デストラクタ
//--------------------------------------------------------
CTime::~CTime()
{
}
//------------------------------------------------------
// 初期化処理
//------------------------------------------------------
void CTime::Init(void)
{
	// 変数宣言
	int nNumber;
	for (int nCount = 0; nCount < TIME_DIGIT; nCount++)
	{
		// タイムの1桁を取得
		nNumber = (int)(m_nTime / 60) % (int)powf(10.0f, (float)nCount + 1.0f) / (int)powf(10.0f, (float)nCount);
		// 生成
		m_apNumber[nCount] = CNumber::Create(
			nNumber,
			m_pos + D3DXVECTOR3(((TIME_DIGIT * m_size.x) * 0.5f) - (nCount * m_size.x) - (m_size.x * 0.5f), 0.0f, 0.0f),
			CNumber::TEX_SCORE,
			D3DXVECTOR2(m_size.x, m_size.y)
		);
	}
}
//------------------------------------------------------
// 終了処理
//------------------------------------------------------
void CTime::Uninit(void)
{
	for (int nCount = 0; nCount < TIME_DIGIT; nCount++)
	{
		if (m_apNumber[nCount] != NULL)
		{
			m_apNumber[nCount]->Uninit();
			delete m_apNumber[nCount];
			m_apNumber[nCount] = NULL;
		}
	}
}
//------------------------------------------------------
// 更新処理
//------------------------------------------------------
void CTime::Update(void)
{
	// 通常演出出なかったら関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		return;
	}
	// 秘奥義発動中 |
	// 死亡時 |
	// 関数を抜ける
	else if (CCharacter::GetSTrick() == true ||
		CGame::GetState() == CGame::STATE_GAMEDIED)
	{
		return;
	}
	// 変数宣言
	int nNumber;
#ifdef _DEBUG
	// デバッグモード
	// タイムを強制で10秒にする
	if (CManager::GetKeyboard()->GetKeyboardPress(DIK_F4) &&
		CManager::GetKeyboard()->GetKeyboardTrigger(DIK_1))
	{
		m_nTime = DERAY_TIME(10);
	}
#endif // _DEBUG
	// ストップ状態ではないとき
	// ->タイムダウン
	if (!m_bTimeStop)
	{
		// タイムダウン
		DecreasesTime();
	}
	for (int nCount = 0; nCount < TIME_DIGIT; nCount++)
	{
		// ヌルチェック
		if (m_apNumber[nCount] != NULL)
		{
			// ナンバーの更新
			m_apNumber[nCount]->Update();
			// タイムの1桁を取得
			nNumber = (int)(m_nTime / 60) % (int)powf(10.0f, (float)nCount + 1.0f) / (int)powf(10.0f, (float)nCount);
			// ナンバーの設定
			m_apNumber[nCount]->SetNum(nNumber);
		}
	}
}
//------------------------------------------------------
// 描画処理
//------------------------------------------------------
void CTime::Draw(void)
{
	// 通常演出出なかったら関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		return;
	}
	// 秘奥義発動中更新処理
	else if (CCharacter::GetSTrick() == true)
	{
		return;
	}
	for (int nCount = 0; nCount < TIME_DIGIT; nCount++)
	{
		if (m_apNumber[nCount] != NULL)
		{
			m_apNumber[nCount]->Draw();
		}
	}
}

#ifdef _DEBUG
// ------------------------------------------
// デバッグ表示
// ------------------------------------------
void CTime::Debug(void)
{
}
#endif // _DEBUG

//------------------------------------------------------
// ポジションの設定
//------------------------------------------------------
void CTime::SetPos(D3DXVECTOR3 pos)
{
	m_pos = pos;
}

//------------------------------------------------------
// 時間減少処理
//------------------------------------------------------
void CTime::DecreasesTime()
{
	m_nTime--;

	if (m_nTime <= 0)
	{
		m_nTime = 0;
		m_bTimeOver = true;
	}
}
//------------------------------------------------------
// タイムオーバーフラグの取得
//------------------------------------------------------
bool CTime::GetTimeOverFlag(void)
{
	return m_bTimeOver;
}
//------------------------------------------------------
// 生成処理
//------------------------------------------------------
CTime *CTime::Create(
	D3DXVECTOR3 const &pos,
	D3DXVECTOR2 const &size
)
{
	// 変数宣言
	CTime *pTime;
	pTime = new CTime;
	// シーン管理設定
	pTime->ManageSetting(CScene::LAYER_UI);
	pTime->m_pos = pos;
	pTime->m_size = size;
	pTime->Init();
	return pTime;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// タイムの読み込み処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CTime::Load(void)
{
	return S_OK;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだ情報を破棄
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CTime::UnLoad(void)
{
}