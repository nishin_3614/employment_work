// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 矢処理の説明[arrow.cpp]
// Author : Nishiyama Koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "arrow.h"
#include "player.h"
#include "3Dparticle.h"
#include "game.h"
#include "spherecollision.h"
#include "circleshadow.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#define ARROW_SPEED (10.0f)									// 弓のスピード
#define ARROW_MODELID (3)									// モデルID
#define ARROW_COLLISIONSIZE (3)								// 当たり判定の半径
#define ARROW_SHADOWSIZE (D3DXVECTOR3(20.0f,0.0f,20.0f))	// 影のサイズ

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数宣言
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CModel_info *			CArrow::m_pModel;		// モデル情報

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CArrow::CArrow() : CScene_X::CScene_X()
{
	m_nCntFrame = 0;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CArrow::~CArrow()
{
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 初期化処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CArrow::Init(void)
{
	// シーンxの初期化
	CScene_X::Init();
	// モデル番号設定
	CScene_X::SetModelId(ARROW_MODELID);
	// 球の当たり判定
	m_AttackCollision = CSphereCollision::Create_Self(
		D3DVECTOR3_ZERO,
		CScene_X::GetPos(),
		ARROW_COLLISIONSIZE
	);
	// 円形シャドウの生成
	m_pCircleShadow = CCircleshadow::Create(
		CScene_X::GetPos(),
		ARROW_SHADOWSIZE);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 終了処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CArrow::Uninit(void)
{
	CScene_X::Uninit();
	// 円形シャドウ開放
	if (m_pCircleShadow != NULL)
	{
		m_pCircleShadow->Release();
		m_pCircleShadow->Uninit();
		delete m_pCircleShadow;
		m_pCircleShadow = NULL;
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 更新処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CArrow::Update(void)
{
	// 通常演出出なかったら関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		return;
	}
	// 秘奥義発動中更新処理
	else if (CCharacter::GetSTrick() == true)
	{
		return;
	}
	// 軌跡particle
	C3DParticle::Create(
		C3DParticle::PARTICLE_ID_ARROW_SMOKE,
		CScene_X::GetPos()
	);
	// フレームカウントアップ
	m_nCntFrame++;
	// 5秒後消滅
	if (m_nCntFrame > DERAY_TIME(5))
	{
		Release();
		return;
	}
	// 移動処理
	Move();
	// 当たり判定
	Collision();

	// 変数宣言
	D3DXVECTOR3 ShadowPos = CScene_X::GetPos();
	// yの座標を床に合わせる
	ShadowPos.y = 0.1f;
	// 円形シャドウの位置設定
	m_pCircleShadow->SetPos(ShadowPos);
	m_pCircleShadow->Set_Vtx_Pos(CScene_THREE::OFFSET_TYPE_SIDE_CENTER);
	// 円形シャドウ更新
	if (m_pCircleShadow != NULL)
	{
		m_pCircleShadow->Update();
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CArrow::Draw(void)
{
	// 通常演出出なかったら関数を抜ける
	if (CGame::GetPerfom() != CGame::PERFOM_NORMAL)
	{
		return;
	}
	// 秘奥義発動中更新処理
	else if (CCharacter::GetSTrick() == true)
	{
		return;
	}
	// 矢の描画
	CScene_X::Draw();
	// 円形シャドウ描画
	if (m_pCircleShadow != NULL)
	{
		m_pCircleShadow->Draw();
	}
}

#ifdef _DEBUG
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デバッグ表示
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CArrow::Debug(void)
{
}
#endif // _DEBUG

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 作成処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CArrow * CArrow::Create(
	D3DXVECTOR3 const &pos,						// 位置
	D3DXVECTOR3 const &rot						// 回転
)
{
	// 変数宣言
	CArrow * pArrow;		// シーン3Dクラス
	// メモリの生成(初め->基本クラス,後->派生クラス)
	pArrow = new CArrow();
	// シーン管理設定
	pArrow->ManageSetting(CScene::LAYER_3DOBJECT);
	// 初期化処理
	pArrow->Init();
	// 位置設定
	pArrow->SetPos(pos);
	// 回転設定
	pArrow->SetRot(rot);
	// 生成したオブジェクトを返す
	return pArrow;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// リソース情報読み込む設定
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT CArrow::Load(void)
{
	return S_OK;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだリソース情報を開放する
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CArrow::UnLoad(void)
{
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 移動処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CArrow::Move(void)
{
	// 変数宣言
	D3DXVECTOR3 pos;
	// 情報取得
	pos = CScene_X::GetPos();	// 位置
	// 位置更新
	pos.x += sinf(CScene_X::GetRot().y + D3DX_PI) * ARROW_SPEED;
	pos.z += cosf(CScene_X::GetRot().y + D3DX_PI) * ARROW_SPEED;
	// 位置設定
	CScene_X::SetPos(pos);
	// シーンX更新
	CScene_X::Update();
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 当たり判定
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CArrow::Collision(void)
{
	// 当たり判定の更新
	m_AttackCollision->GetShape()->PassPos(CScene_X::GetPos());
	// 変数宣言
	CPlayer * pPlayer = NULL;	// プレイヤー
	for (int nCntLayer = 0; nCntLayer < CScene::GetMaxLayer(CScene::LAYER_CHARACTER); nCntLayer++)
	{
		// 情報取得
		pPlayer = (CPlayer*)CScene::GetScene(CScene::LAYER_CHARACTER, nCntLayer, CPlayer());	// プレイヤー
		// 存在しているかどうか
		if (pPlayer == NULL)
		{
			continue;
		}

		// 当たり判定
		if (m_AttackCollision->CollisionDetection(pPlayer->GetCollision()))
		{
			// ダメージ処理
			pPlayer->AplayDamage(
				CScene_X::GetPos(),
				CCharacter::BLUST_WEAK,
				1
			);
			// 開放処理
			Release();
		}
	}
}
