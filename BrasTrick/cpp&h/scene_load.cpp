// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// シーン処理の説明[scene_load.cpp]
// Author : Nishiyama Koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "scene_load.h"
/* 描画 */
#include "score.h"
#include "score_knock.h"
#include "statistics_point.h"
#include "score_board.h"
#include "number.h"
#include "rank_ui.h"
#include "topscore.h"
#include "pause_ui.h"
#include "event_fade.h"
#include "2Dgauge.h"
#include "3Deffect.h"
#include "3Dparticle.h"
#include "item.h"
#include "floor.h"
#include "meshwall.h"
#include "meshdome.h"
#include "meshsphere.h"
#include "collision.h"
#include "camera.h"
#include "forceline.h"
#include "character.h"
#include "charaname.h"
#include "arrow.h"
#include "circleshadow.h"
#include "stategauge.h"
#include "3Dgauge.h"
#include "time.h"
#include "fade.h"
#include "Extrusion.h"
#include "texture_manager.h"
#include "ui.h"
#include "scene_X.h"
#include "3Dmap.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CScene_load::CScene_load()
{
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CScene_load::~CScene_load()
{
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// シーン親子作成処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CScene_load::LoadAll(void)
{
	/* ----------テクスチャー読み込み---------- */
	// テクスチャー情報
	if (!CTexture_manager::Load() == S_OK)
	{
		CCalculation::Messanger("テクスチャー情報読み取り失敗");
	}
	// モデルの情報
	if (!CScene_X::LoadModel() == S_OK)
	{
		CCalculation::Messanger("モデル情報読み取り失敗");
	}
	/* UI */
	// ランク用UI
	if (!CRank_ui::Load()== S_OK)
	{
		CCalculation::Messanger("ランクUI読み取り失敗");
	}
	// UI
	if (!CUi::Load() == S_OK)
	{
		CCalculation::Messanger("UI読み取り失敗");
	}
	// 統計ポイント
	if (!CStatistics_point::Load() == S_OK)
	{
		CCalculation::Messanger("統計ポイント読み取り失敗");
	}
	// スコアボード
	if (!CScore_board::Load() == S_OK)
	{
		CCalculation::Messanger("スコアボード読み取り失敗");
	}
	// 上位スコア
	if (!CTopscore::Load()== S_OK)
	{
		CCalculation::Messanger("上位スコア読み取り失敗");
	}
	// スコア
	if (!CScore::Load() == S_OK)
	{
		CCalculation::Messanger("スコア読み取り失敗");
	}
	// タイム
	if (!CTime::Load() == S_OK)
	{
		CCalculation::Messanger("タイム読み取り失敗");
	}
	// 倒した数
	if (!CScore_knock::Load() == S_OK)
	{
		CCalculation::Messanger("倒した数読み取り失敗");
	}
	// ポーズUI
	if (!CPause_ui::Load() == S_OK)
	{
		CCalculation::Messanger("ポーズUI読み取り失敗");
	}
	// イベントフェード
	if (!CEvent_fade::Load() == S_OK)
	{
		CCalculation::Messanger("イベントフェード読み取り失敗");
	}
	// フェード
	if (!CFade::Load() == S_OK)
	{
		CCalculation::Messanger("フェード読み取り失敗");
	}

	/* 効果 */
	// 3Deffect
	if(!C3DEffect::Load()== S_OK)
	{
		CCalculation::Messanger("3Dエフェクト読み取り失敗");
	}
	// 3Dparticle
	if(!C3DParticle::Load()== S_OK)
	{
		CCalculation::Messanger("3Dパーティクル読み取り失敗");
	}
	// 3Dゲージ
	if (!C3DGauge::Load() == S_OK)
	{
		CCalculation::Messanger("3Dゲージ読み取り失敗");
	}
	/*
	/* ゲームに必要なもの */
	// カメラ
	if (!CCamera::Load() == S_OK)
	{
		CCalculation::Messanger("カメラ読み取り失敗");
	}
	// ステートゲージ
	if (!CStateGauge::Load() == S_OK)
	{
		CCalculation::Messanger("ステートゲージ読み取り失敗");
	}
	// 2Dゲージ
	if (!C2DGauge::Load() == S_OK)
	{
		CCalculation::Messanger("2Dゲージ読み取り失敗");
	}

	/* 2Dオブジェクト */
	// 集中線
	if (!CForceline::Load() == S_OK)
	{
		CCalculation::Messanger("集中線読み取り失敗");
	}
	// 番号
	if (!CNumber::Load() == S_OK)
	{
		CCalculation::Messanger("番号読み取り失敗");
	}

	/* 3Dオブジェクト*/

	/* メッシュ */
	// 床
	if (!CFloor::Load() == S_OK)
	{
		CCalculation::Messanger("床読み取り失敗");
	}
	// メッシュドーム
	if (!CMeshdome::Load() == S_OK)
	{
		CCalculation::Messanger("メッシュドーム読み取り失敗");
	}
	// メッシュスフィア
	if (!CMeshsphere::Load() == S_OK)
	{
		CCalculation::Messanger("メッシュスフィア読み取り失敗");
	}
	// メッシュウォール
	if (!CMeshwall::Load() == S_OK)
	{
		CCalculation::Messanger("メッシュウォール読み取り失敗");
	}

	/* 3Dモデル*/
	// キャラクターステータス
	if (!CCharacter::LoadStatus() == S_OK)
	{
		CCalculation::Messanger("キャラクターステータス情報読み取り失敗");
	}
	// キャラクター
	if(!CCharacter::Load()== S_OK)
	{
		CCalculation::Messanger("キャラクター読み取り失敗");
	}
	// 静的オブジェクト
	if (!CArrow::Load() == S_OK)
	{
		CCalculation::Messanger("静的オブジェクト読み取り失敗");
	}
	// アイテム
	if (!CItem::Load() == S_OK)
	{
		CCalculation::Messanger("矢読み取り失敗");
	}
	// キャラ名
	if (!CCharaname::Load() == S_OK)
	{
		CCalculation::Messanger("キャラクター名読み取り失敗");
	}
	// 円形シャドウ
	if (!CCircleshadow::Load() == S_OK)
	{
		CCalculation::Messanger("円形シャドウ読み取り失敗");
	}
	// 3Dマップ
	if (!C3DMap::Load() == S_OK)
	{
		CCalculation::Messanger("3Dマップ読み取り失敗");
	}

}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだものを破棄する処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CScene_load::UnLoadAll(void)
{
	/* 読み込んだものの破棄*/
	// テクスチャー
	CTexture_manager::UnLoad();
	// モデルの情報
	CScene_X::UnLoadModel();
	// 番号
	CNumber::UnLoad();
	// ランク用UI
	CRank_ui::UnLoad();
	// UI
	CUi::UnLoad();
	// 上位スコア
	CTopscore::Save();
	// スコア
	CScore::UnLoad();
	// タイム
	CTime::UnLoad();
	// 倒した数
	CScore_knock::UnLoad();
	// ポーズUI
	CPause_ui::UnLoad();
	// 床
	CFloor::UnLoad();
	// キャラクター
	CCharacter::UnLoad();
	// 3Deffect
	C3DEffect::Unload();
	// 3Dparticle
	C3DParticle::Unload();
	// メッシュドーム
	CMeshdome::UnLoad();
	// メッシュスフィア
	CMeshsphere::Unload();
	// アイテム
	CItem::UnLoad();
	// 2Dゲージ
	C2DGauge::UnLoad();
	// 3Dゲージ
	C3DGauge::UnLoad();
	// 円形シャドウ
	CCircleshadow::UnLoad();
	// 集中線
	CForceline::UnLoad();
	// 矢
	CArrow::UnLoad();
	// キャラクター名
	CCharaname::UnLoad();
	// メッシュウォール
	CMeshwall::UnLoad();
	// フェード
	CFade::UnLoad();
}