// ------------------------------------------
//
// メイン処理の説明[main.h]
// Author : Nishiyama Koki
//
// ------------------------------------------

#ifndef _MAIN_H_
#define _MAIN_H_

// ------------------------------------------
// マクロ定義
// ------------------------------------------
#define _CRT_SECURE_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC
#define DIRECTINPUT_VERSION (0x0800)

// ------------------------------------------
// インクルードファイル
// ------------------------------------------
#include <Windows.h>					// ウィンドウアプリケーション
#include "d3dx9.h"
/* 標準ライブラリ */
#include <stdlib.h>						// システムヘッダー
#include <stdio.h>						// 一般的なヘッダー
#include <time.h>						// タイムヘッダー
#include <math.h>						// 数学関数ヘッダー
#include <iostream>						// 入出力ストリーム
#include <fstream>						// ファイルストリーム
#include <sstream>						// 文字列ストリーム
#include <tchar.h>						// 凡庸テキスト
#include <vector>						// 動的配列ヘッダー
#include <memory>						// メモリヘッダー
/* ImGui */
#include "ImGui/imgui.h"
#include "ImGui/imgui_impl_dx9.h"
#include "ImGui/imgui_impl_win32.h"

#include "dinput.h"						// 入力機器用
#include "xaudio2.h"					// BGM

// 標準ライブラリの名前空間指定
using namespace std;	// 名前空間指定

// ------------------------------------------
//
// ライブラリのリンク
//
// ------------------------------------------
#pragma comment(lib,"d3d9.lib")		// 描画処理に必要
#pragma comment(lib,"d3dx9.lib")	// [d3d9.lib]の拡張ライブラリ
#pragma comment(lib,"dxguid.lib")	// DirectXコンポーネント(部品)
#pragma comment (lib,"winmm.lib")
#pragma comment (lib, "dinput8.lib")

// ------------------------------------------
//
// マクロ関数定義
//
// ------------------------------------------
#define FVF_VERTEX_2D (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX2)				// ２Ｄポリゴン頂点フォーマット( 頂点座標[2D] / 頂点カラー / テクスチャ座標 )
#define	FVF_VERTEX_3D	(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1)	// 3Ｄポリゴン頂点フォーマット( 頂点座標[3D] / 法線ベクトル /頂点カラー / テクスチャ座標 )
#define SCREEN_WIDTH (1280)															// スクリーンの横サイズ
#define SCREEN_HEIGHT (720)															// スクリーンの縦サイズ
#define INTERVAL_TIME (60 * 15)														// インターバル時間
#define ERROW_ACTION (0)															// デバッグ状態

// ------------------------------------------
//
// 構造体
//
// ------------------------------------------
// ２Ｄポリゴン頂点フォーマットに合わせた構造体を定義
typedef struct
{
	D3DXVECTOR3 pos;	// 頂点座標
	float       rhw;	// 1.0で固定
	D3DCOLOR    col;	// 頂点カラー
	D3DXVECTOR2 tex;	// テクスチャー
} VERTEX_2D;

// 3Ｄポリゴン頂点フォーマットに合わせた構造体を定義
typedef struct
{
	D3DXVECTOR3 pos;	// 頂点座標
	D3DXVECTOR3 nor;	// 法線ベクトル
	D3DCOLOR col;		// 頂点カラー
	D3DXVECTOR2 tex;	// テクスチャー
} VERTEX_3D;

// ------------------------------------------
//
// 前方宣言
//
// ------------------------------------------
class CRenderer;	// レンダラークラス

// ------------------------------------------
//
// プロトタイプ宣言
//
// ------------------------------------------
// 終了状態設定
void SetDestWind(bool const &bDest);
#if _DEBUG
// FPS取得
int GetFPS(void);
// コンソール表示
void DispConsol();
#endif // _DEBUG

#endif // !_MAIN_H_