// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 大将処理 [general.h]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _GENERAL_H_
#define _GENERAL_H_

#define _CRT_SECURE_NO_WARNINGS

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// インクルードファイル
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "enemy.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 前方宣言
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CCharaname;

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// クラス
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CGeneral : public CEnemy
{
public:
	/* 列挙型*/
	typedef enum
	{
		STATE_NORMAL = 0,
		STATE_DAMAGE,
		STATE_DIE
	} STATE;

	// ---------モーションタイプ---------- //
	typedef enum
	{
		/* コンボ1 */
		MOTIONTYPE_DIAGONALCUT = CCharacter::MOTIONTYPE_MAX,
		/* コンボ2 */
		MOTIONTYPE_REVERSESIDE,
		/* コンボ3 */
		MOTIONTYPE_CIRCLESLASH,
		/* コンボ4 */
		MOTIONTYPE_INSTANTSLASHER,
		MOTIONTYPE_WHIRLWIND,
		MOTIONTYPE_CONBOMAX,
		/* 必奥義 */
		MOTIONTYPE_TRICK1_FIREUPSIDE = MOTIONTYPE_CONBOMAX,
		MOTIONTYPE_TRICK2_FLASHSTRIKE,
		MOTIONTYPE_TRICK3_PAINLIGHTNING,
		MOTIONTYPE_MAX
	} MOTIONTYPE;

	/* 関数 */
	CGeneral();
	~CGeneral();
	void Init(void);
	void Uninit(void);
	void Update(void);
	void Draw(void);
#ifdef _DEBUG
	void Debug(void);
#endif // _DEBUG
	// 倒した数カウント
	void EnemyDie(void);
	// 作成
	static CGeneral * Create(		
		D3DXVECTOR3 const & pos = D3DVECTOR3_ZERO,	// 位置
		D3DXVECTOR3 const & rot = D3DVECTOR3_ZERO
	);
	static HRESULT LoadCreate(void);
	static HRESULT Load(void);		// 読み込み
	static void StaticInit(void);	// 静的変数の初期化
	static void UnLoad(void);		// UnLoadする
	static int GetKoCount(void) { return m_nKoCount; };	// 倒した数
protected:
private:
	/* 関数 */
	void move(void);	// 移動
	void rot(void);		// 回転

	// AI用
	void AiAction(void);
	void AttackAction(void);
	bool ConboAction(void);
	void NormalAction(void);
	/* 変数 */
	static int m_nAllNum;							// 現在存在している数
	static int m_nKoCount;							// 倒した数
	CCharaname * m_charaname;						// キャラクター名
};
#endif
