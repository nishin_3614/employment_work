// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// シーン処理の説明[scene.h]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#ifndef _SCENE_H_
#define _SCENE_H_	 // ファイル名を基準を決める

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "manager.h"
#include "texture_manager.h"
#include "load.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CScene
{
public:
	/* 列挙型 */
	// 物の配置順番
	typedef enum
	{
		LAYER_3DOBJECT = 0,
		LAYER_WORLD,
		LAYER_CHARACTER,
		LAYER_3DPARTICLE,
		LAYER_3DOBJECT2,
		LAYER_COLLISION,
		LAYER_UI,
		LAYER_PARTICLE,
		LAYER_FADE,
		LAYER_MAX
	} LAYER;
	/* 関数 */
	CScene();
	virtual ~CScene();
	virtual void Init(void) = 0;										// 初期化
	virtual void Uninit(void) = 0;										// 終了
	virtual void Update(void) = 0;										// 更新
	virtual void Draw(void) = 0;										// 描画
#ifdef _DEBUG
	virtual void Debug(void);										// デバッグ用
#endif // _DEBUG

	static void ReleaseAll(void);										// シーン親子リリース
	static void UpdateAll(void);										// シーン親子更新
	static void DrawAll(void);											// シーン親子描画
	// 取得
	// シーン情報取得(レイヤー・クラス指定)
	static CScene * GetScene(
		LAYER const &layer,												// レイヤー
		int const &nCntScene,											// カウントシーン
		CScene &										// クラス型情報
	);
	// シーン情報取得(クラス名)
	//	layer		: レイヤー
	//	nCntScene	: カウントシーン
	//	cName		: クラス名
	CScene * GetScene(
		LAYER const & layer,		// レイヤー
		int const & nCntScene,		// カウントシーン
		char const * cName			// クラス名
	);
	// シーン情報取得(レイヤー指定)
	static CScene * GetScene(
		LAYER const &layer,												// レイヤー
		int const &nCntScene											// カウントシーン
	);
	// レイヤー内のシーン最大数取得
	static int GetMaxLayer(LAYER const &layer) 
	{ return (signed)m_pScene[layer].size(); };	
	static bool GetUpdateStop(void) { return m_sta_bStop; };						// 静止状態
	// 設定
	// シーンの静止
	static void UpdateStop(
		bool const &bStop,												// 静止するかしないか
		int const &nMaxStop = 0											// 最大静止時間
	);	// 更新をとめる
	// シーン管理設定
	void ManageSetting(LAYER const &layer);
	// オブジェクトの更新ストップ状態設定
	void SetStop(bool const &bStopState)
	{
		m_bStop = bStopState; 
	};
	// オブジェクトの更新ストップ状態取得
	bool GetStop(void)
	{
		return m_bStop;
	};
	// オブジェクトの描画状態設定
	void SetDrawState(bool const &bDraw)
	{
		m_bDraw = bDraw;
	};
	// オブジェクトの描画状態取得
	bool GetDrawState(void)
	{
		return m_bDraw;
	};

	void Release(void);													// オブジェクトの破棄
protected:

private:
	/* 関数 */
	static void DeadFragAll(void);				// 死亡フラグが立ったものをリリース
	/* 変数 */
	static vector<CScene*> m_pScene[LAYER_MAX];	// シーン管理用変数
	static bool	m_sta_bStop;						// 更新を止める
	static int m_nMaxStop;						// 最大静止時間
	static int m_nCntStop;						// 静止時間
	bool m_bDeadFrag;							// フラグ
	bool m_bStop;								// 更新を止めるか止めないか
	bool m_bDraw;								// 描画状態
};

#endif