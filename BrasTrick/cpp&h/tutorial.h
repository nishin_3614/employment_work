// ------------------------------------------
//
// チュートリアルヘッダー処理 [tutorial.h]
// Author : Nishiyama Koki
//
// ------------------------------------------
#ifndef _TUTORIAL_H_
#define _TUTORIAL_H_

// ------------------------------------------
//
// インクルードファイル
//
// ------------------------------------------
#include "basemode.h"

// ------------------------------------------
//
// マクロ定義
//
// ------------------------------------------

// ------------------------------------------
//
// 前方宣言
//
// ------------------------------------------
class CPlayer;				// プレイヤークラス

// ------------------------------------------
//
// クラス
//
// ------------------------------------------
class CTutorial : public CBaseMode
{
public:
	/* 列挙型 */
	// 状態
	typedef enum
	{
		STATE_NORMAL = 0,	// 通常
		STATE_PAUSE,		// ポーズ
		STATE_MAX			// 最大数
	} STATE;

	/* 関数 */
	// コンストラクタ
	CTutorial();
	// デストラクタ
	~CTutorial();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
	// 生成処理
	static CTutorial * Create(void);
	// 状態設定
	static void SetState(STATE const &state) { m_state = state; };
	// 状態取得
	static STATE const &GetState(void) { return m_state; };
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void) {};
#endif // _DEBUG

	// 歩いているかどうか取得
	bool GetWalk(void) const { return m_bWalk; };
	// 攻撃したかどうか取得
	bool GetAttack(void) const { return m_bAttack; };
	// MPが最大値かどうか取得
	bool GetMpMax(void) const { return m_bMPMax; };
	// 秘奥義が打ったかどうか取得
	bool GetTrick(void) const { return m_bTrick; };
	// 敵の出現をするかどうか設定
	void SetEnemy(bool const &bEnemy) { m_bEnemy = bEnemy; };
protected:
private:
	static STATE m_state;				// 状態
	bool m_bWalk;						// プレイヤーが歩いているかどうか
	bool m_bAttack;						// 攻撃したかどうか
	bool m_bMPMax;						// MPが最大になっているかどうか
	bool m_bTrick;						// 秘奥義を出したかどうか
	bool m_bEnemy;						// 敵出現するかどうか
	CPlayer * m_pPlayer;				// プレイヤー情報
};
#endif
