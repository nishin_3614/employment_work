// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// UIグループ処理の説明[ui_group.h]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _UI_GROUP_H_
#define _UI_GROUP_H_	 // ファイル名を基準を決める

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "ui.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 前方宣言
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CUi_group : public CScene
{
public:
	/* 関数 */
	// コンストラクタ
	CUi_group();
	// デストラクタ
	~CUi_group();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void);
#endif // _DEBUG
	// 全リソース情報の読み込み
	static HRESULT Load(void);
	// 全リソース情報の開放
	static void UnLoad(void);
	// 生成処理(個人管理)
	//	uitype	: UIタイプ
	static unique_ptr<CUi_group> Create_Self(
		CUi::UITYPE const &uitype	// UIタイプ
	);
	// 生成処理(シーン管理)
	//	uitype	: UIタイプ
	static CUi_group * Create(
		CUi::UITYPE const &uitype	// UIタイプ
	);
	// UI全体の状態情報取得
	int GetUiGroup_FadeType(void);
	// フェードアウトスタート
	// 引数なし:UI全部
	// 引数あり:UIの番号だけ
	void Start_FadeOut(int const &ID = -1);
	// フェードインスタート
	// 引数なし:UI全部
	// 引数あり:UIの番号だけ
	void Start_FadeIn(int const & ID);

protected:

private:
	/* 関数 */
	// 初期化 //
	// タイトルUI名前の初期化
	void Init_UiType_Title_Ui_Name(void);
	// タイトル選択の更新
	void Init_UiType_Title_Select(void);
	// ゲームスタート時の初期化
	void Init_UiType_GameStart(void);
	// ゲームオーバー時の初期化
	void Init_UiType_GameOver(void);
	// ゲーム死亡時の初期化
	void Init_UiType_GameDied(void);
	// ゲーム警告の初期化
	void Init_UiType_GameWarning(void);
	// ゲーム好機の初期化
	void Init_UiType_GameChance(void);
	// チュートリアル合格の初期化
	void Init_UiType_Tutorial_Clear(void);
	// チュートリアル敵を倒すの更新
	void Init_UiType_Tutorial_Knock(void);
	// チュートリアル終了の初期化
	void Init_UiType_Tutorial_End(void);

	// 更新 //
	// ゲームオーバーの更新
	void Update_UiType_GameOver(void);
	// ゲームスタートの更新
	void Update_UiType_GameStart(void);
	// ゲーム死亡の更新
	void Update_UiType_GameDied(void);
	// ゲーム警告の更新
	void Update_UiType_GameWarning(void);
	// ゲーム好機の更新
	void Update_UiType_GameChance(void);
	// ゲーム指令1クリア時の更新
	void Update_UiType_GameOrder_Clear1(void);
	// ゲーム指令2クリア時の更新
	void Update_UiType_GameOrder_Clear2(void);
	// ゲーム指令3クリア時の更新
	void Update_UiType_GameOrder_Clear3(void);
	// ゲームラスト10秒時の更新
	void Update_UiType_GameLast(void);
	// タイトルUI名前の更新
	void Update_UiType_Title_Ui_Name(void);
	// タイトルチュートリアルスキップの更新
	void Update_UiType_Title_TutorialSkip(void);
	// チュートリアルスキップの更新
	void Update_UiType_Tutorial_Skip(void);
	// チュートリアル歩くの更新
	void Update_UiType_Tutorial_Walk(void);
	// チュートリアル攻撃の更新
	void Update_UiType_Tutorial_Attack(void);
	// チュートリアル敵を倒すの更新
	void Update_UiType_Tutorial_Knock(void);
	// チュートリアル秘奥義の更新
	void Update_UiType_Tutorial_Trick(void);
	// チュートリアル終了の更新
	void Update_UiType_Tutorial_End(void);
	// チュートリアル合格の更新
	void Update_UiType_Tutorial_Clear(void);

	/* 変数 */
	CUi::UITYPE m_Uitype;
	/* 各UIクラス */
	CUi::VEC_UNI_UI m_Ui;		// UI情報
	// 処理に必要な変数
	vector<int> m_vec_nNumber;	// int情報
	vector<bool> m_vec_Bool;	// bool情報
};

#endif