// ----------------------------------------
//
// 総計処理の説明[statistics.h]
// Author : Nishiyama Koki
//
// ----------------------------------------
#ifndef _STATISTICS_H_
#define _STATISTICS_H_	 // ファイル名を基準を決める

// ----------------------------------------
//
// インクルードファイル
//
// ----------------------------------------
#include "basemode.h"

// ----------------------------------------
//
// マクロ定義
//
// ----------------------------------------

// ----------------------------------------
//
// 前方宣言
//
// ----------------------------------------
class CStatistics_point;

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CStatistics : public CBaseMode
{
public:
	/* 列挙型 */
	/* 関数 */
	// コンストラクタ
	CStatistics();
	// デストラクタ
	~CStatistics();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void);
#endif // _DEBUG
	// 生成処理
	static CStatistics * Create(void);
	// ゲームスコアの設定
	//	nSollider	: 兵士KOカウント
	//	nArcher		: 弓兵KOカウント
	//	nMerchant	: 商人KOカウント
	//	nBoss		: ボスKOカウント
	static void SetGameScore(
		int const &nSollider,	// 兵士KOカウント
		int const &nArcher,		// 弓兵KOカウント
		int const &nMerchant,	// 商人KOカウント
		int const &nBoss		// ボスKOカウント
	);
	// ゲームスコア取得
	static int GetGameScore(void) { return m_nGameScore; };
	// 各敵のKOカウント取得
	static int GetEnemy(int const &nID);
protected:

private:
	/* 関数 */

	/* 変数 */
	static int m_nSollider;						// 兵士KOカウント
	static int m_nArcher;						// 弓兵KOカウント
	static int m_nMerchant;						// 商人KOカウント
	static int m_nBoss;							// 隊長カウント
	static int m_nGameScore;					// ゲームスコア
	CStatistics_point * m_pStatistics_point;	// 統計ポイント
};

#endif