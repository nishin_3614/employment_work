// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 3Dマップ処理の説明[3Dmap.cpp]
// Author : Nishiyama Koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "3Dmap.h"
#include "manager.h"
#include "floor.h"
#include "meshwall.h"
#include "scene_X.h"
#include "scene_three.h"
#include "player.h"
#include "soldier.h"
#include "archer.h"
#include "general.h"
#include "merchant.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#define MANAGER_FILE ("data/LOAD/MAPPING/map_manager.txt")	// マップテキストのファイルパス

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数宣言
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vector<vector<C3DMap::OBJECT>> C3DMap::m_vec_obj;		// オブジェ情報
vector<vector<C3DMap::CHARACTER>> C3DMap::m_vec_char;	// キャラクター情報
vector<vector<C3DMap::POLYGON>> C3DMap::m_vec_polygon;	// ポリゴン情報
vector<vector<C3DMap::FLOOR>> C3DMap::m_vec_floor;		// 床情報
vector<vector<C3DMap::WALL>> C3DMap::m_vec_wall;		// 壁情報
vector<string> C3DMap::m_vec_String;					// ステージごとのファイルパス情報

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
C3DMap::C3DMap()
{

}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
C3DMap::~C3DMap()
{

}

#ifdef _DEBUG
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デバッグ処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DMap::Debug(void)
{
}
#endif // _DEBUG

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込んだ情報を生成
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT C3DMap::LoadCreate(MAP const &map)
{
	// 変数宣言
	int nCntMap = 0;	// マップカウント

	// 読み込んだ情報の生成
	// オブジェクトループ
	for (nCntMap = 0; nCntMap < (signed)m_vec_obj[map].size(); nCntMap++)
	{
		// シーンXの生成
		CScene_X::Create(
			m_vec_obj[map][nCntMap].pos,
			m_vec_obj[map][nCntMap].rot,
			m_vec_obj[map][nCntMap].nModelType,
			true
		);
	}
	// キャラクターループ
	for (nCntMap = 0; nCntMap < (signed)m_vec_char[map].size(); nCntMap++)
	{
		// プレイヤー
		if (m_vec_char[map][nCntMap].nCharacter == CCharacter::CHARACTER_PLAYER)
		{
			CPlayer::Create(m_vec_char[map][nCntMap].pos,
				m_vec_char[map][nCntMap].rot
			);
		}
		// 兵士
		if (m_vec_char[map][nCntMap].nCharacter == CCharacter::CHARACTER_SOLDIER)
		{
			CSoldier::Create(m_vec_char[map][nCntMap].pos,
				m_vec_char[map][nCntMap].rot
			);
		}
		// 弓兵
		if (m_vec_char[map][nCntMap].nCharacter == CCharacter::CHARACTER_ARCHER)
		{
			CArcher::Create(m_vec_char[map][nCntMap].pos,
				m_vec_char[map][nCntMap].rot
			);
		}
		// 商人
		if (m_vec_char[map][nCntMap].nCharacter == CCharacter::CHARACTER_MERCHANT)
		{
			CMerchant::Create(
				m_vec_char[map][nCntMap].pos,
				m_vec_char[map][nCntMap].rot
			);
		}
		// 大将
		if (m_vec_char[map][nCntMap].nCharacter == CCharacter::CHARACTER_GENERAL)
		{
			CGeneral::Create(
				m_vec_char[map][nCntMap].pos,
				m_vec_char[map][nCntMap].rot
			);
		}
	}
	// ポリゴンループ
	for (nCntMap = 0; nCntMap < (signed)m_vec_polygon[map].size(); nCntMap++)
	{
		// シーンXの生成
		CScene_THREE::Create(
			CScene_THREE::OFFSET_TYPE_VERTICAL_CENTER,
			m_vec_polygon[map][nCntMap].pos,
			D3DXVECTOR3(m_vec_polygon[map][nCntMap].size.x, m_vec_polygon[map][nCntMap].size.y, 0.0f),
			m_vec_polygon[map][nCntMap].nTexType,
			m_vec_polygon[map][nCntMap].rot
		);
	}
	// 床ループ
	for (nCntMap = 0; nCntMap < (signed)m_vec_floor[map].size(); nCntMap++)
	{
		// 床の生成
		CFloor::Create(
			m_vec_floor[map][nCntMap].pos,
			D3DXVECTOR3(m_vec_floor[map][nCntMap].size.x,0.0f, m_vec_floor[map][nCntMap].size.y),
			m_vec_floor[map][nCntMap].rot,
			m_vec_floor[map][nCntMap].nBlockWidth,
			m_vec_floor[map][nCntMap].nBlockDepth,
			m_vec_floor[map][nCntMap].nTexType
			);
	}
	// 壁ループ
	for (nCntMap = 0; nCntMap < (signed)m_vec_wall[map].size(); nCntMap++)
	{
		// 壁の生成
		CMeshwall::Create(
			m_vec_wall[map][nCntMap].pos,
			D3DXVECTOR3(m_vec_wall[map][nCntMap].size.x, m_vec_wall[map][nCntMap].size.y,0.0f),
			m_vec_wall[map][nCntMap].rot,
			m_vec_wall[map][nCntMap].nBlockWidth,
			m_vec_wall[map][nCntMap].nBlockDepth,
			m_vec_wall[map][nCntMap].nTexType
			);
	}

	return S_OK;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// リソース情報読み込む設定
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT C3DMap::Load(void)
{
	// 変数宣言
	LPDIRECT3DDEVICE9	pDevice = CManager::GetRenderer()->GetDevice();
	// マネージャーテキストの読み込み
	ManagerText_Load();
	// スクリプトの読み込み
	ScriptLoad();
	return S_OK;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// マネージャーテキストファイルの読み込み処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT C3DMap::ManagerText_Load(void)
{
	// 変数宣言
	vector<vector<string>> vsvec_Contens;	// ファイルの中身格納用
	// ファイルの中身を取得する
	vsvec_Contens = CCalculation::FileContens(MANAGER_FILE, '\0');
	// 行ループ
	for (int nCntLine = 0; nCntLine < (signed)vsvec_Contens.size(); nCntLine++)
	{
		// 項目ループ
		for (int nCntItem = 0; nCntItem < (signed)vsvec_Contens.at(nCntLine).size(); nCntItem++)
		{
			// 項目カウント
			switch (nCntItem)
			{
				// ファイルパス情報
			case 0:
				// ファイルパス情報格納
				m_vec_String.emplace_back(vsvec_Contens.at(nCntLine).at(nCntItem));
				break;
				//それ以外
			default:
				break;
			}
		}
	}
	return S_OK;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// スクリプト読み込み
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT C3DMap::ScriptLoad(void)
{
	// 変数宣言
	FILE *pFile;			// ファイル情報
	char	cRaedText[128],	// 文字として読み取り用
		cHeadText[128],	// 比較するよう
		cDie[128];		// 不必要な文字
	int		nCntError = 0,	// エラーカウント
		nCntObj,		// オブジェクトカウント
		nCntChar,		// キャラクターカウント
		nCntPoly,		// ポリゴンカウント
		nCntFloor,		// 床カウント
		nCntWall;		// 壁カウント
// ファイルパスのサイズループ
	for (int nCntScript = 0; nCntScript < (signed)m_vec_String.size(); nCntScript++)
	{
		// 変数宣言
		vector<OBJECT> vec_obj;			// オブジェクト情報
		vector<CHARACTER> vec_char;		// キャラクター情報
		vector<POLYGON> vec_poly;		// ポリゴン情報
		vector<FLOOR> vec_floor;		// 床情報
		vector<WALL> vec_wall;			// 壁情報
		// 初期化
		nCntObj = 0;					// オブジェクトカウント
		nCntChar = 0;					// キャラクターカウント
		nCntPoly = 0;					// ポリゴンカウント
		nCntFloor = 0;					// 床カウント
		nCntWall = 0;					// 壁カウント

		// ファイルが開かれていなかったら
		// ->エラー発生
		if ((pFile = fopen(m_vec_String[nCntScript].c_str(), "r")) == NULL)
		{
#ifdef _DEBUG
			string sErrow = m_vec_String[nCntScript] + "が見つかりません";
			CCalculation::Messanger(sErrow.c_str());
#endif // _DEBUG
			// エラーを返す
			return E_FAIL;
		}
		// スクリプトが来るまでループ
		while (strcmp(cHeadText, "SCRIPT") != 0)
		{
			fgets(cRaedText, sizeof(cRaedText), pFile);	// 一文を読み込む
			sscanf(cRaedText, "%s", &cHeadText);		// 比較用テクストに文字を代入
			// エラーカウントをインクリメント
			nCntError++;
			// ファイルカウントが上限を超えたら
			// ->エラー発生
			if (nCntError > FILELINE_ERROW)
			{
				nCntError = 0;
				// ファイル情報を閉じる
				fclose(pFile);
#ifdef _DEBUG
				CCalculation::Messanger("スタティックオブジェクトのスクリプトがありません");
#endif // _DEBUG
				// エラーを返す
				return E_FAIL;
			}
		}

		// スクリプトなら
		if (strcmp(cHeadText, "SCRIPT") == 0)
		{
			// エンドスクリプトが来るまでループ
			while (strcmp(cHeadText, "END_SCRIPT") != 0)
			{
				fgets(cRaedText, sizeof(cRaedText), pFile);
				sscanf(cRaedText, "%s", &cHeadText);
				// 改行だったら
				if (strcmp(cHeadText, "\n") == 0)
				{
				}
				// オブジェクトが来たら
				else if (strcmp(cHeadText, "OBJECTSET") == 0)
				{
					// オブジェクト追加
					vec_obj.push_back(OBJECT());
					// エンドオブジェクトセットが来るまでループ
					while (strcmp(cHeadText, "END_OBJECTSET") != 0)
					{
						fgets(cRaedText, sizeof(cRaedText), pFile);
						sscanf(cRaedText, "%s", &cHeadText);

						// 親情報読み込み
						if (strcmp(cHeadText, "MODELTYPE") == 0)
						{
							sscanf(cRaedText, "%s %s %d", &cDie, &cDie, &vec_obj[nCntObj].nModelType);
						}
						// 位置情報読み込み
						else if (strcmp(cHeadText, "POS") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_obj[nCntObj].pos.x,
								&vec_obj[nCntObj].pos.y,
								&vec_obj[nCntObj].pos.z);
						}
						// 回転情報読み込み
						else if (strcmp(cHeadText, "ROT") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_obj[nCntObj].rot.x,
								&vec_obj[nCntObj].rot.y,
								&vec_obj[nCntObj].rot.z);
						}
						// エラーカウントをインクリメント
						nCntError++;
						//
						if (nCntError > FILELINE_ERROW)
						{// エラー
							nCntError = 0;
							// ファイル情報を閉じる
							fclose(pFile);
#ifdef _DEBUG
							CCalculation::Messanger("エンドオブジェクトセットがありません");
#endif
							// エラーを返す
							return E_FAIL;
						}
					}
					// カウントオブジェ
					nCntObj++;
				}
				// キャラクターが来たら
				else if (strcmp(cHeadText, "CHARACTERSET") == 0)
				{
					// キャラクター追加
					vec_char.push_back(CHARACTER());
					// エンドキャラクターセットが来るまでループ
					while (strcmp(cHeadText, "END_CHARACTERSET") != 0)
					{
						fgets(cRaedText, sizeof(cRaedText), pFile);
						sscanf(cRaedText, "%s", &cHeadText);

						// 親情報読み込み
						if (strcmp(cHeadText, "CHARACTERTYPE") == 0)
						{
							sscanf(cRaedText, "%s %s %d", &cDie, &cDie, &vec_char[nCntChar].nCharacter);
						}

						// 位置情報読み込み
						else if (strcmp(cHeadText, "POS") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_char[nCntChar].pos.x,
								&vec_char[nCntChar].pos.y,
								&vec_char[nCntChar].pos.z);
						}

						// 回転情報読み込み
						else if (strcmp(cHeadText, "ROT") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_char[nCntChar].rot.x,
								&vec_char[nCntChar].rot.y,
								&vec_char[nCntChar].rot.z);
						}
						// エラーカウントをインクリメント
						nCntError++;
						if (nCntError > FILELINE_ERROW)
						{// エラー
							nCntError = 0;
							// ファイル情報を閉じる
							fclose(pFile);
#ifdef _DEBUG
							CCalculation::Messanger("エンドキャラクターセットがありません");
#endif
							// エラーを返す
							return E_FAIL;
						}
					}
					// カウントキャラ
					nCntChar++;
				}
				// ポリゴンが来たら
				else if (strcmp(cHeadText, "POLYGONSET") == 0)
				{
					// ポリゴン追加
					vec_poly.push_back(POLYGON());
					// エンドポリゴンセットが来るまでループ
					while (strcmp(cHeadText, "END_POLYGONSET") != 0)
					{
						fgets(cRaedText, sizeof(cRaedText), pFile);
						sscanf(cRaedText, "%s", &cHeadText);

						// テクスチャータイプ情報読み込み
						if (strcmp(cHeadText, "TEXTYPE") == 0)
						{
							sscanf(cRaedText, "%s %s %d", &cDie, &cDie, &vec_poly[nCntPoly].nTexType);
						}

						// 位置情報読み込み
						else if (strcmp(cHeadText, "POS") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_poly[nCntPoly].pos.x,
								&vec_poly[nCntPoly].pos.y,
								&vec_poly[nCntPoly].pos.z);
						}

						// 回転情報読み込み
						else if (strcmp(cHeadText, "ROT") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_poly[nCntPoly].rot.x,
								&vec_poly[nCntPoly].rot.y,
								&vec_poly[nCntPoly].rot.z);
						}
						// サイズ情報読み込み
						else if (strcmp(cHeadText, "SIZE") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f", &cDie, &cDie,
								&vec_poly[nCntPoly].size.x,
								&vec_poly[nCntPoly].size.y
							);
						}
						// ビルボードをかけるかかけないか情報読み込み
						else if (strcmp(cHeadText, "BILLBOARD") == 0)
						{
							vec_poly[nCntPoly].bBillboard = true;
						}
						// Zバッファをかけるかかけないか情報読み込み
						else if (strcmp(cHeadText, "ZENEBLE") == 0)
						{
							vec_poly[nCntPoly].bZEneble = true;
						}
						// ライティングをかけるかかけないか情報読み込み
						else if (strcmp(cHeadText, "LIGHTING") == 0)
						{
							vec_poly[nCntPoly].bLighting = true;
						}
						// アルファブレンドをかけるかかけないか情報読み込み
						else if (strcmp(cHeadText, "ALPHA") == 0)
						{
							vec_poly[nCntPoly].bAlpha = true;
						}

						// エラーカウントをインクリメント
						nCntError++;
						if (nCntError > FILELINE_ERROW)
						{// エラー
							nCntError = 0;
							// ファイル情報を閉じる
							fclose(pFile);
#ifdef _DEBUG
							CCalculation::Messanger("エンドポリゴンセットがありません");
#endif
							// エラーを返す
							return E_FAIL;
						}
					}
					// カウントポリゴン
					nCntPoly++;
				}
				// 床が来たら
				else if (strcmp(cHeadText, "FLOORSET") == 0)
				{
					// 床追加
					vec_floor.push_back(FLOOR());
					// エンド床セットが来るまでループ
					while (strcmp(cHeadText, "END_FLOORSET") != 0)
					{
						fgets(cRaedText, sizeof(cRaedText), pFile);
						sscanf(cRaedText, "%s", &cHeadText);

						// 親情報読み込み
						if (strcmp(cHeadText, "TEXTYPE") == 0)
						{
							sscanf(cRaedText, "%s %s %d", &cDie, &cDie, &vec_floor[nCntFloor].nTexType);
						}

						// 位置情報読み込み
						else if (strcmp(cHeadText, "POS") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_floor[nCntFloor].pos.x,
								&vec_floor[nCntFloor].pos.y,
								&vec_floor[nCntFloor].pos.z);
						}

						// 回転情報読み込み
						else if (strcmp(cHeadText, "ROT") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_floor[nCntFloor].rot.x,
								&vec_floor[nCntFloor].rot.y,
								&vec_floor[nCntFloor].rot.z);
						}
						// 縦ブロック情報読み込み
						else if (strcmp(cHeadText, "BLOCK_DEPTH") == 0)
						{
							sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
								&vec_floor[nCntFloor].nBlockDepth
							);
						}
						// 横ブロック情報読み込み
						else if (strcmp(cHeadText, "BLOCK_WIDTH") == 0)
						{
							sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
								&vec_floor[nCntFloor].nBlockWidth
							);
						}
						// サイズ情報読み込み
						else if (strcmp(cHeadText, "SIZE") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f", &cDie, &cDie,
								&vec_floor[nCntFloor].size.x,
								&vec_floor[nCntFloor].size.y
							);
						}

						// エラーカウントをインクリメント
						nCntError++;
						if (nCntError > FILELINE_ERROW)
						{// エラー
							nCntError = 0;
							// ファイル情報を閉じる
							fclose(pFile);
#ifdef _DEBUG
							CCalculation::Messanger("エンド床セットがありません");
#endif
							// エラーを返す
							return E_FAIL;
						}
					}
					// カウント床
					nCntFloor++;
				}
				// 壁が来たら
				else if (strcmp(cHeadText, "WALLSET") == 0)
				{
					// 壁追加
					vec_wall.push_back(WALL());
					// エンド壁セットが来るまでループ
					while (strcmp(cHeadText, "END_WALLSET") != 0)
					{
						fgets(cRaedText, sizeof(cRaedText), pFile);
						sscanf(cRaedText, "%s", &cHeadText);

						// 親情報読み込み
						if (strcmp(cHeadText, "TEXTYPE") == 0)
						{
							sscanf(cRaedText, "%s %s %d", &cDie, &cDie, &vec_wall[nCntWall].nTexType);
						}

						// 位置情報読み込み
						else if (strcmp(cHeadText, "POS") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_wall[nCntWall].pos.x,
								&vec_wall[nCntWall].pos.y,
								&vec_wall[nCntWall].pos.z);
						}

						// 回転情報読み込み
						else if (strcmp(cHeadText, "ROT") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f %f", &cDie, &cDie,
								&vec_wall[nCntWall].rot.x,
								&vec_wall[nCntWall].rot.y,
								&vec_wall[nCntWall].rot.z);
						}
						// 縦ブロック情報読み込み
						else if (strcmp(cHeadText, "BLOCK_DEPTH") == 0)
						{
							sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
								&vec_wall[nCntWall].nBlockDepth
							);
						}
						// 横ブロック情報読み込み
						else if (strcmp(cHeadText, "BLOCK_WIDTH") == 0)
						{
							sscanf(cRaedText, "%s %s %d", &cDie, &cDie,
								&vec_wall[nCntWall].nBlockWidth
							);
						}
						// サイズ情報読み込み
						else if (strcmp(cHeadText, "SIZE") == 0)
						{
							sscanf(cRaedText, "%s %s %f %f",
								&cDie, &cDie,
								&vec_wall[nCntWall].size.x,
								&vec_wall[nCntWall].size.y
							);
						}

						// エラーカウントをインクリメント
						nCntError++;
						if (nCntError > FILELINE_ERROW)
						{// エラー
							nCntError = 0;
							// ファイル情報を閉じる
							fclose(pFile);
#ifdef _DEBUG
							CCalculation::Messanger("エンド壁セットがありません");
#endif
							// エラーを返す
							return E_FAIL;
						}
					}
					// カウント壁
					nCntWall++;
				}

				// エラーカウントをインクリメント
				nCntError++;
				if (nCntError > FILELINE_ERROW)
				{// エラー
					nCntError = 0;
					// ファイル情報を閉じる
					fclose(pFile);
#ifdef _DEBUG
					CCalculation::Messanger("エンドスクリプトがありません");
#endif
					// エラーを返す
					return E_FAIL;
				}
			}
			// オブジェクトの格納
			m_vec_obj.push_back(vec_obj);
			// キャラクターの格納
			m_vec_char.push_back(vec_char);
			// ポリゴンの格納
			m_vec_polygon.push_back(vec_poly);
			// 床の格納
			m_vec_floor.push_back(vec_floor);
			// 壁の格納
			m_vec_wall.push_back(vec_wall);
		}
		// ファイル情報を閉じる
		fclose(pFile);
	}
	// 成功を返す
	return S_OK;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 全リソース情報の開放
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DMap::UnLoad(void)
{

}