// ----------------------------------------
//
// 3D線処理の説明[3Dline.h]
// Author : Nishiyama Koki
//
// ----------------------------------------
#ifndef _3DLINE_H_
#define _3DLINE_H_	 // ファイル名を基準を決める

// ----------------------------------------
//
// インクルードファイル
//
// ----------------------------------------
#include "scene.h"

// ----------------------------------------
//
// マクロ定義
//
// ----------------------------------------

// ------------------------------------------
//
// 前方宣言
//
// ------------------------------------------

// ------------------------------------------
//
// クラス
//
// ------------------------------------------
class C3DLine : public CScene
{
public:
	/* 関数 */
	// コンストラクタ
	C3DLine();
	// デストラクタ
	~C3DLine();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void);
#endif // _DEBUG
	// 位置設定
	//	BeginPos	: 始点位置
	//	EndPos		: 終点位置
	void SetPos(
		D3DXVECTOR3 const &BeginPos,	// 始点位置
		D3DXVECTOR3 const &EndPos		// 終点位置
	);
	// 生成
	//	pos			: 位置
	//	rot			: 回転
	//	BeginPos	: 始点位置
	//	EndPos		: 終点位置
	static C3DLine * Create(
		D3DXVECTOR3 *pos,				// 位置
		D3DXVECTOR3 const &rot,			// 回転
		D3DXVECTOR3 const &BeginPos,	// 始点位置
		D3DXVECTOR3 const &EndPos		// 終点位置
	);
	// 全リソース情報の読み込み
	static HRESULT Load(void);
	// 全リソース情報の開放
	static void UnLoad(void);
protected:

private:
	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;	// 頂点バッファ情報
	D3DXMATRIX  m_mtxWorld;				// ワールドマトリックス
	D3DXVECTOR3 *m_ppos;				// 位置
	D3DXVECTOR3 m_rot;					// 回転
};

#endif