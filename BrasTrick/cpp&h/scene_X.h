// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// Xシーン処理の説明[scene_X.h]
// Author : Nishiyama Koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _SCENE_X_H_
#define _SCENE_X_H_	 // ファイル名を基準を決める

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "scene.h"
#include "model_info.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 前方宣言
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CCollision;		// あたり判定クラス
class CExtrusion;		// 押し出しクラス
class CScene_THREE;		// 3Dシーンクラス

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CScene_X : public CScene
{
public:
	/* 構造体 */
	// モデル情報
	typedef struct MODEL_LOAD
	{
		// コンストラクタ
		MODEL_LOAD()
		{
			pMesh = NULL;							// メッシュ情報へのポインタ
			pBuffMat = NULL;						// マテリアル情報へのポインタ
			nNumMat = 0;							// マテリアルの数
			vtxMinMaterials = D3DVECTOR3_ZERO;		// モデル情報の位置の最小値
			vtxMaxMaterials = D3DVECTOR3_ZERO;		// モデル情報の位置の最大値
			size = D3DVECTOR3_ZERO;					// サイズ
		}
		LPD3DXMESH			pMesh;					// メッシュ情報へのポインタ
		LPD3DXBUFFER		pBuffMat;				// マテリアル情報へのポインタ
		vector<LPDIRECT3DTEXTURE9> vec_pTexture;	// テクスチャー情報
		DWORD				nNumMat;				// マテリアルの数
		D3DXVECTOR3			vtxMinMaterials;		// モデル情報の位置の最小値
		D3DXVECTOR3			vtxMaxMaterials;		// モデル情報の位置の最大値
		D3DXVECTOR3			size;					// サイズ
	} MODEL_LOAD;

	/* 関数 */
	// コンストラクタ
	CScene_X();
	// デストラクタ
	~CScene_X();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void);
#endif // _DEBUG
	// 生成処理(シーン管理)
	//	pos			: 位置
	//	rot			: 回転
	//	nModelId	: モデル番号
	//	bShadowMap	: シャドウマッピング状態
	static CScene_X * Create(
		D3DXVECTOR3 const &pos,						// 位置
		D3DXVECTOR3 const &rot = D3DVECTOR3_ZERO,	// 回転
		int const &nModelId = 0,					// モデル番号
		bool const &bShadowMap = false				// シャドウマッピング状態
	);
	// 生成処理(個人管理)
	//	pos			: 位置
	//	rot			: 回転
	//	nModelId	: モデル番号
	//	bShadowMap	: シャドウマッピング状態
	static CScene_X * Create_Self(
		D3DXVECTOR3 const &pos,						// 位置
		D3DXVECTOR3 const &rot = D3DVECTOR3_ZERO,	// 回転
		int const &nModelId = 0,					// モデル番号
		bool const &bShadowMap = false				// シャドウマッピング状態
	);
	// 生成処理(unique個人管理) ※返り値をもらう変数はstd::moveを使う
	//	pos			: 位置
	//	rot			: 回転
	//	nModelId	: モデル番号
	//	bShadowMap	: シャドウマッピング状態
	static unique_ptr<CScene_X> Create_Uni(
		D3DXVECTOR3 const &pos,						// 位置
		D3DXVECTOR3 const &rot = D3DVECTOR3_ZERO,	// 回転
		int const &nModelId = 0,					// モデル番号
		bool const &bShadowMap = false				// シャドウマッピング状態
	);

	// モデル情報の読み込み
	static HRESULT LoadModel(void);
	// モデル情報の開放
	static HRESULT UnLoadModel(void);
	// 位置設定
	//	pos	: 位置
	void SetPos(D3DXVECTOR3 const &pos) { m_pos = pos; };
	// 回転設定
	//	rot	: 回転
	void SetRot(D3DXVECTOR3 const &rot) { m_rot = rot; };
	// モデル番号設定
	//	nModelId	: モデル番号
	void SetModelId(int const &nModelId) { m_nModelId = nModelId; };
	// シャドウマッピング設定
	//	bShadow	: シャドウマッピング状態
	void SetShadowMap(bool const &bShadow) { m_bShadowMap = bShadow; };
	// あたり判定設定
	void SetCollision(void);
	// 親子行列設定
	//	ParentMtx	: 親行列
	void SetParentMtx(D3DXMATRIX * ParentMtx) { m_pParentMtx = ParentMtx; };
	// 位置取得
	D3DXVECTOR3 &GetPos(void) { return m_pos; };
	// 回転取得
	D3DXVECTOR3 &GetRot(void) { return m_rot; };
	// 行列取得
	D3DXMATRIX &GetMatrix(void) { return m_mtxWorld; };
	// モデル情報の取得
	CScene_X::MODEL_LOAD * GetModel(void);
	// モデル情報の取得
	//	nModelId	: モデル番号
	static MODEL_LOAD * GetModelLoad(
		int const &nModelId		// モデル情報取得
	);
protected:
private:
	/* 関数 */
	// モデルの設定
	//	pModel_load	: モデル情報
	static void ModelSetting(
		MODEL_LOAD * pModel_load	// モデル情報
	);
	/* 変数 */
	static vector<unique_ptr<MODEL_LOAD>>	m_pModelLoad;	// モデル情報の読み込み用
	D3DXVECTOR3								m_pos;			// 位置情報
	D3DXVECTOR3								m_rot;			// 回転情報
	D3DXMATRIX								*m_pParentMtx;	// 親マトリックス
	D3DXMATRIX								m_mtxWorld;		// ワールドマトリックス
	int										m_nModelId;		// モデル番号
	bool									m_bShadowMap;	// シャドウマッピングにするかしないか
	float									m_fModelAlpha;	// モデルのアルファ値
	float									m_fShadowAlpha;	// 影のα値
	CScene_THREE *							m_pShadow;		// まる影
	CExtrusion								*m_pExtrusion;	// 押し出し
};

#endif