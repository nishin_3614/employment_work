// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// ゲームヘッダー処理 [game.h]
// Author : Nishiyama Koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _GAME_H_
#define _GAME_H_

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "basemode.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 前方宣言
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CPause;			// ポーズクラス
class CTime;			// タイムクラス
class CScore;			// スコアクラス
class CScore_knock;		// 倒した数クラス
class CEvent_ui;		// イベントクラス
class CUi;				// UIクラス

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CGame : public CBaseMode
{
public:
	/* 列挙型 */
	// 状態
	typedef enum
	{
		STATE_NORMAL = 0,	// 通常状態
		STATE_START,		// 開始状態
		STATE_CLEAR,		// クリア状態
		STATE_GAMEOVER,		// ゲームオーバー状態
		STATE_GAMEDIED,		// 死亡状態
		STATE_PAUSE,		// ポーズ状態
		STATE_MAX			// 最大数
	} STATE;
	// 演出
	typedef enum
	{
		PERFOM_NORMAL = 0,	// 通常演出
		PERFOM_MERCHANT,	// 商人演出
		PERFOM_GENERAL,		// 大将演出
		PERFOM_MAX			// 最大数
	} PERFOM;
	// UI番号
	typedef enum
	{
		UINUMBER_TIME = 0,	// UI_タイム
		UINUMBER_KNOCK,		// UI_倒した数
		UINUMBER_SCORE,		// UI_スコア
		UINUMBER_MAX		// 最大数
	} UINUMBER;

	/* 関数 */
	// コンストラクタ
	CGame();
	// デストラクタ
	~CGame();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
	// 生成処理
	static CGame * Create(void);
	// 情報取得
	// タイム取得
	CTime *GetTime(void)				{ return m_Time; };
	// スコア取得
	CScore *GetScore(void)				{ return m_Score; };
	// 倒した数取得
	CScore_knock *GetScore_knock(void)	{ return m_Score_knock; };
#ifdef _DEBUG
	void Debug(void);
#endif // _DEBUG
	// 設定
	// 状態設定
	static void SetState(STATE const state) { m_state = state; };
	// 演出設定
	static void SetPerfom(PERFOM const Perfom) { m_Perfom = Perfom; };
	// 取得
	// 状態取得
	static STATE GetState(void) { return m_state; };
	// 演出取得
	static PERFOM GetPerfom(void) { return m_Perfom; };
	// ボーナスポイント用
	// ボーナスポイント1取得
	static bool GetbBonus_1(void) { return m_bBonus_1; };
	// ボーナスポイント2取得
	static bool GetbBonus_2(void) { return m_bBonus_2; };
	// ボーナスポイント3取得
	static bool GetbBonus_3(void) { return m_bBonus_3; };
	// ボーナスポイント1設定
	static void SetbBonus_1(bool const &bBonus) { m_bBonus_1 = bBonus; };
	// ボーナスポイント2設定
	static void SetbBonus_2(bool const &bBonus) { m_bBonus_2 = bBonus; };
	// ボーナスポイント3設定
	static void SetbBonus_3(bool const &bBonus) { m_bBonus_3 = bBonus; };

protected:
private:
	/* 関数 */
	// 静的変数の初期化処理
	void StaticInit(void);
	// ポーズの状態
	void PauseState(void);
	// ゲーム内イベント
	void GameEvent(void);
	// ボーナス処理
	void Bonus(void);
	/* 変数 */
	static STATE m_state;				// 状態
	static PERFOM m_Perfom;				// 演出中かどうか

	// ボーナスポイント用
	static bool m_bBonus_1;				// 100体以上倒したかどうか
	static bool m_bBonus_2;				// 全種類の敵を倒したかどうか
	static bool m_bBonus_3;				// 隊長を倒したかどうか
	bool		m_bBouns_1_Ui;			// 100体以上倒したかどうかのUI使用状態
	bool		m_bBouns_2_Ui;			// 全種類の敵を倒したかどうかのUI使用状態
	bool		m_bBouns_3_Ui;			// 隊長を倒したかどうかのUI使用状態
	CPause * m_pause;					// ポーズ
	vector<CUi*> m_vec_UI;				// 背景のUI
	CTime * m_Time;						// タイム
	CScore * m_Score;					// スコア
	CScore_knock * m_Score_knock;		// 倒した数
	int m_nCntTime;						// カウントタイム
};
#endif
