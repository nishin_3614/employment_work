// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 2Dプレゼンツ処理の説明[2Dpresents.h]
// Author : Nishiyama Koki
//
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _2DPRESENTS_H_
#define _2DPRESENTS_H_	 // ファイル名を基準を決める

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "scene_two.h"

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// クラス
//
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class C2DPresents : public CScene_TWO
{
public:
	/* 列挙型 */
	// フェードタイプ
	typedef enum
	{
		FADETYPE_NONE = -1,		// フェードタイプ_NONE
		FADETYPE_IN,			// フェードタイプ_フェードイン
		FADETYPE_COOPERATION,	// フェードタイプ_強調
		FADETYPE_OUT,			// フェードタイプ_フェードアウト
		FADETYPE_END,			// フェードタイプ_フェードエンド
		FADETYPE_MAX
	} FADETYPE,*PFADETYPE;
	/* 関数 */
	// コンストラクタ
	C2DPresents();
	// デストラクタ
	~C2DPresents();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void);
#endif // _DEBUG
	/* 強調 */
	// 強調設定(点滅)
	//	changecol	: 変化色
	//	nTimeSwitch	: スイッチON/OFFにかかる時間
	//	nTimeChange	: 変化にかかる時間
	void SetCooperation(
		D3DXCOLOR const & changecol,	// 変わる色
		int const & nTimeSwitch,		// スイッチON/OFFにかかる時間
		int const & nTimeChange			// 変化にかかる時間
	);
	// 強調設定(拡大・縮小)
	//	fChangeScal	: 拡大率
	//	nTimeChange	: 変化にかかる時間
	void SetCooperation(
		float const & fChangeScal,		// 拡大率
		int const & nTimeChange			// 変化にかかる時間
	);
	/* フェードイン */
	// フェードイン設定(位置)
	//	BeginPos	: 初期位置
	//	EndPos		: 目的位置
	//	nMaxfram	: 最大フレーム
	void SetFadeIn(
		D3DXVECTOR3 const & BeginPos,	// 初期位置
		D3DXVECTOR3 const & EndPos,		// 目的位置
		int const & nMaxfram			// 最大フレーム
	);
	// フェードイン設定(色指定)
	//	BeginPos	: 初期カラー
	//	EndPos		: 目的カラー
	//	nMaxfram	: 最大フレーム
	void SetFadeIn(
		D3DXCOLOR const & BeginCol,		// 初期カラー
		D3DXCOLOR const & EndCol,		// 目標カラー
		int const & nMaxfram			// 最大フレーム
	);
	// フェードイン設定(アニメスタート)
	//	nMaxStartAnim	: 最大アニメスタートにかかる時間
	void SetFadeIn_StarAnim(
		int const & nMaxStartAnim		// 最大アニメスタート
	);
	// フェードイン強制終了設定
	//	bCompulsion	: 強制終了させるかさせないか
	void SetFadeIn_Compulsion(
		bool const & bCompulsion		// 強制終了させるかさせないか
	);
	// フェードインの使用設定
	bool GetFadeIn_Bool(void);
	// フェードインの強制終了
	void Compulsion_FadeIn(void);
	// フェードイン開始
	void Start_FadeIn(void);
	/* フェードアウト */
	// フェードアウト設定(位置)
	//	BeginPos	: 初期位置
	//	EndPos		: 目的位置
	//	nMaxfram	: 最大フレーム
	void SetFadeOut(
		D3DXVECTOR3 const & BeginPos,	// 初期位置
		D3DXVECTOR3 const & EndPos,		// 目標位置
		int const & nMaxfram			// 最大フレーム
	);
	// フェードアウト設定(色指定)
	//	BeginPos	: 初期カラー
	//	EndPos		: 目的カラー
	//	nMaxfram	: 最大フレーム
	void SetFadeOut(
		D3DXCOLOR const & BeginCol,		// 初期カラー
		D3DXCOLOR const & EndCol,		// 目標カラー
		int const & nMaxfram			// 最大フレーム
	);
	// フェードアウト設定(アニメスタート)
	//	nMaxStartAnim	: 最大アニメスタートにかかる時間
	void SetFadeOut_StarAnim(
		int const & nMaxStartAnim		// 最大アニメスタート
	);
	// フェードイン強制終了設定処理
	//	bCompulsion	: 強制終了させるかさせないか
	void SetFadeOut_Compulsion(
		bool const & bCompulsion		// 強制終了させるかさせないか
	);
	// フェードアウトの使用設定
	bool GetFadeOut_Bool(void);
	// フェードアウトの強制終了
	void Compulsion_FadeOut(void);
	// フェードアウトの開始設定
	void Start_FadeOut(void);
	// フェードタイプの状態取得
	FADETYPE GetFadetype(void);
	// フェードタイプの状態設定
	void SetFadetype(FADETYPE const &Fadetype) { m_Fadetype = Fadetype; };
	// フェードインの最大フレームを取得
	int const GetFadeInMaxFram(void);
	// フェードインのフレームを取得
	int const GetFadeInFram(void);
	// フェードインの最大開始アニメタイムを取得
	int const GetFadeInMaxAnimTime(void);
	// フェードインのアニメタイムを取得
	int const GetFadeInAnimTime(void);
	// フェードアウトの最大開始アニメタイムを取得
	int const GetFadeOutMaxAnimTime(void);
	// フェードアウトのアニメタイムを取得
	int const GetFadeOutAnimTime(void);

	// 取得 //
	// 生成(シーン管理)
	//	type		: タイプ
	//	pos			: 位置
	//	size		: サイズ
	//	frot		: 角度
	//	col			: 色
	static C2DPresents * Create(
		OFFSET_TYPE	const & type,									// タイプ
		D3DXVECTOR3 const & pos,									// 位置
		D3DXVECTOR2 const & size,									// サイズ
		float		const & frot = 0.0f,							// 角度
		D3DXCOLOR	const & col = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f)	// 色
	);
	// 生成(個人管理)
	//	type		: タイプ
	//	pos			: 位置
	//	size		: サイズ
	//	frot		: 角度
	//	col			: 色
	static C2DPresents * Create_Self(
		OFFSET_TYPE	const & type,									// タイプ
		D3DXVECTOR3 const & pos,									// 位置
		D3DXVECTOR2 const & size,									// サイズ
		float		const & frot = 0.0f,							// 角度
		D3DXCOLOR	const & col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)	// 色
	);
	// 生成(Unique管理)
	//	type		: タイプ
	//	pos			: 位置
	//	size		: サイズ
	//	frot		: 角度
	//	col			: 色
	static unique_ptr<C2DPresents> Create_Unique(
		OFFSET_TYPE	const & type,									// タイプ
		D3DXVECTOR3 const & pos,									// 位置
		D3DXVECTOR2 const & size,									// サイズ
		float		const & frot = 0.0f,							// 角度
		D3DXCOLOR	const & col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)	// 色
	);

protected:
	// 設定 //
private:
	/* 列挙型 */

	/* 構造体 */
	// 色の点滅
	typedef struct COL_FLASHING
	{
		COL_FLASHING()
		{
			Origincol = D3DXCOLOR_INI;
			changecol = D3DXCOLOR_INI;
			colDiff = D3DXCOLOR_CA(0.0f,0.0f);
			nTimeSwitch = 0;
			nTimeChange = 0;
			nCntTimeSwitch = 0;
			nCntTimeChange = 0;
			bSwitch = false;
			bChange = false;
		}
		D3DXCOLOR Origincol;			// 元色
		D3DXCOLOR changecol;			// 変わる色
		D3DXCOLOR colDiff;				// 色の差分
		int nTimeSwitch;				// スイッチON/OFFにかかる時間
		int nTimeChange;				// 変化にかかる時間
		int nCntTimeSwitch;				// スイッチON/OFFにかかる時間カウント
		int nCntTimeChange;				// 変化にかかる時間カウント
		bool bSwitch;					// スイッチON/OFF
		bool bChange;					// 変化状態
	} COL_FLASHING,*P_COL_FLASHING;
	// 拡大率情報
	typedef struct SCALING
	{
		SCALING()
		{
			OriginSize = D3DVECTOR2_ZERO;
			fScal = 1.0f;
			fScalChange = 0.0f;
			fScalDiff = 0.0f;
			nTimeChange = 0;
			nCntTimeChange = 0;
			bChange = false;
		}
		D3DXVECTOR2 OriginSize;			// 元のサイズ
		float fScal;					// 拡大率
		float fScalChange;				// 目標拡大率
		float fScalDiff;				// 拡大率上昇率
		int nTimeChange;				// 変化にかかる時間
		int nCntTimeChange;				// 変化にかかる時間カウント
		bool bChange;					// 変化状態
	} SCALING, *P_SCALING;
	// 移動情報
	typedef struct DESIGN_MOVE
	{
		DESIGN_MOVE()
		{
			BeginPos = D3DVECTOR3_ZERO;
			EndPos = D3DVECTOR3_ZERO;
			DiffPos = D3DVECTOR3_ZERO;
			nfram = 0;
			nMaxfram = 0;
		}
		D3DXVECTOR3 BeginPos;			// 初期位置
		D3DXVECTOR3 EndPos;				// 目標位置
		D3DXVECTOR3 DiffPos;			// 差分位置
		int nfram;						// フレーム
		int nMaxfram;					// 最大フレーム
	} DESIGN_MOVE, *P_DESIGN_MOVE;
	// 色指定情報
	typedef struct DESIGN_COL
	{
		DESIGN_COL()
		{
			BeginCol = D3DXCOLOR_INI;
			EndCol = D3DXCOLOR_INI;
			DiffCol = D3DXCOLOR_INI;
			nfram = 0;
			nMaxfram = 0;
		}
		D3DXCOLOR BeginCol;				// 初期カラー
		D3DXCOLOR EndCol;				// 目標カラー
		D3DXCOLOR DiffCol;				// 差分カラー
		int nfram;						// フレーム
		int nMaxfram;					// 最大フレーム
	} DESIGN_COL, *P_DESIGN_COL;
	// フェード情報
	typedef struct FADE
	{
		FADE()
		{
			pDesign_Move = NULL;
			pDesign_Col = NULL;
			bFade = false;
			bCompulsion = false;
			nCntFade = 0;
			nMaxFade = 0;
			nCntAnimStart = 0;
			nMaxAnimStart = 0;
		}
		P_DESIGN_MOVE pDesign_Move;		// 移動情報
		P_DESIGN_COL pDesign_Col;		// 色指定情報
		bool bFade;						// フェード中
		bool bCompulsion;				// フェード強制終了させるかさせないか
		int nCntFade;					// カウントフェード
		int nMaxFade;					// 最大フェード数
		int nCntAnimStart;				// スタートカウント
		int nMaxAnimStart;				// 最大スタート
	} FADE, *P_FADE;
	// 強調情報
	typedef struct COOPERATION
	{
		COOPERATION()
		{
			pCol_flash = NULL;
			pScal = NULL;
		}
		P_COL_FLASHING pCol_flash;		// 色情報
		P_SCALING pScal;				// スケール情報
	} COOPERATION, *P_COOPERATION;

	/* 関数 */
	// 点滅更新
	void Update_ColorFlashing(void);
	// 拡大率更新
	void Update_Scaling(void);
	// フェード(指定色)
	//	DiffCol		: 差分色
	//	EndCol		: 目標色
	//	nFram		: 現在のフレーム
	//	nMaxFram	: 最大フレーム
	void Update_Fade_DesignCol(
		D3DXCOLOR const & DiffCol,	// 差分色
		D3DXCOLOR const & EndCol,	// 目標色
		int & nFram,				// 現在のフレーム
		int const & nMaxFram		// 最大のフレーム
	);
	// フェード(移動)
	//	DiffPos		: 差分位置
	//	EndPos		: 目標位置
	//	nFram		: 現在のフレーム
	//	nMaxFram	: 最大フレーム
	void Update_Fade_DesignMove(
		D3DXVECTOR3 const & DiffPos,	// 差分位置
		D3DXVECTOR3 const & EndPos,		// 目標位置
		int & nFram,					// 現在のフレーム
		int const & nMaxFram			// 最大のフレーム
	);
	// フェードイン更新
	bool Update_FadeIn(void);
	// フェードアウト更新
	bool Update_FadeOut(void);
	// 強調更新
	bool Update_Cooperation(void);

	/* 変数 */
	P_COOPERATION m_pCooperation;	// 強調情報
	P_FADE m_pFadeIn;				// フェード情報
	P_FADE m_pFadeOut;				// フェード情報
	FADETYPE m_Fadetype;			// フェードタイプ
};

#endif