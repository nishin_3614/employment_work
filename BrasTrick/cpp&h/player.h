// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// プレイヤー処理 [player.h]
// Author : Nishiyama Koki
//
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _PLAYER_H_
#define _PLAYER_H_

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// インクルードファイル
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "character.h"

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 前方宣言
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CStateGauge;	// ステートゲージクラス
class C2DGauge;		// 2Dゲージクラス
class CMeshsphere;	// メッシュスフィアクラス
class CState_face;	// ステートフェイスクラス

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// クラス
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class CPlayer : public CCharacter
{
public:
	// モーションタイプ
	typedef enum
	{
		/* コンボ1 */
		MOTIONTYPE_DIAGONALCUT = CCharacter::MOTIONTYPE_MAX,	// 斜め切り
		MOTIONTYPE_THRUST,										// 突進攻撃
		/* コンボ2 */
		MOTIONTYPE_REVERSESIDE,									// 逆横切り
		/* コンボ3 */
		MOTIONTYPE_CIRCLESLASH,									// 円転斬
		/* コンボ4 */
		MOTIONTYPE_INSTANTSLASHER,								// 瞬撃斬
		MOTIONTYPE_WHIRLWIND,									// 疾風斬
		/* 必奥義 */
		MOTIONTYPE_TRICK1_FIREUPSIDE,							// 常夜禍炎
		MOTIONTYPE_TRICK2_FLASHSTRIKE,							// 轟破瞬撃刃
		/* 演出*/
		MOTIONTYPE_OPENNING,									// オープニング演出
		MOTIONTYPE_RANKINGPAUSE,								// ランキングポーズ
		MOTIONTYPE_MAX
	} MOTIONTYPE;
	/* 関数 */
	// コンストラクタ
	CPlayer();
	// デストラクタ
	~CPlayer();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void);
#endif // _DEBUG
	// 生成
	//	pos	: 位置
	//	rot	: 回転
	static CPlayer * Create(
		D3DXVECTOR3 const & pos = D3DVECTOR3_ZERO,	// 位置
		D3DXVECTOR3 const & rot = D3DVECTOR3_ZERO	// 回転
	);
	// 全リソース情報の読み込み
	static HRESULT Load(void);
	// 全リソース情報の開放
	static void UnLoad(void);
protected:
private:
	/* 構造体 */

	/* 関数 */
	// 移動処理
	void Move(void);
	// モーション時の状態処理
	void MotionState(void);
	// 攻撃処理
	void Attack(void);
	// 通常攻撃処理
	void At_Normal(void);
	// コンボ1_追加入力なし(斜め切り)
	void At_DiagonalCut(void);
	// コンボ1_追加入力↑(突き)
	void At_Thrust(void);
	// コンボ2_追加入力(逆切り)
	void At_ReverseSide(void);
	// コンボ3_追加入力(技:円転斬)
	void At_CircleSlash(void);
	// コンボ4_追加入力(技:瞬撃斬)
	void At_InstantSlasher(void);
	// コンボ4_追加入力(技:旋風斬)
	void At_WhirlWind(void);
	// コンボ攻撃
	void ConboAttack(void);
	// 秘奥義
	void Trick(void);
	// 秘奥義1
	void Trick1_FireUpside(void);
	// 秘奥義2
	void Trick2_FlashStrike(void);
	// UIの描画状態設定
	//	bState	: 描画状態
	void UiDrawState(
		bool const &bState	// 描画状態
	);
	// 状態処理
	void State(void);
	// ダメージ状態処理
	void DamageState(void);
	// 死亡状態処理
	void DieState(void);
	// アイテムの当たり判定
	void ItemCollision(void);

	/* 変数 */
	int			m_nConbo;				// コンボ
	int			m_nInvalConbo;			// コンボインターバル
	C2DGauge	*m_p2DHPGauge;			// HPゲージ
	C2DGauge	*m_p2DTrickGauge;		// 秘奥義ゲージ
	CStateGauge *m_pStateGauge;			// ゲージ背景
	CState_face *m_pState_Face;			// 状態_表情
	CMeshsphere *m_pSphere;				// 球
};
#endif
