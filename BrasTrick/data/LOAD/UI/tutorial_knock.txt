#==============================================================================
#
# [UIエディター] スクリプトファイル [data/LOAD/UI/tutorial_knock.txt]
# Author : KOKI NISHIYAMA
#
#==============================================================================
SCRIPT
	#==============================================================================
	# [0]
	#==============================================================================
	SET PRESENTS
		# ----------[基本設定]---------- #
		POS = 640.0 110.0 0.0
		COL = 1.0 1.0 1.0 1.0
		SIZE = 620.0 250.0
		ROT = 0.0
		FIRST_TEX = 0.0 0.0
		LAST_TEX = 1.0 1.0
		TEXTYPE = 28
		OFFSET = 0
		# ----------[フェードイン]---------- #
		FADE_IN
			# フェード開始時間
			STARTFADETIME = 0
			# 色指定
			FADE_COL
				BEGIN_COL = 1.0 1.0 1.0 0.0
				END_COL = 1.0 1.0 1.0 1.0
				MAXFRAME = 30
			END_FADE_COL
		END_FADE_IN
		# ----------[フェードアウト]---------- #
		FADE_OUT
			# フェード開始時間
			STARTFADETIME = 0
			# 色指定
			FADE_COL
				BEGIN_COL = 1.0 1.0 1.0 1.0
				END_COL = 1.0 1.0 1.0 0.0
				MAXFRAME = 30
			END_FADE_COL
		END_FADE_OUT
	END_SET
	#==============================================================================
	# [1]
	#==============================================================================
	SET PRESENTS
		# ----------[基本設定]---------- #
		POS = 179.0 595.0 0.0
		COL = 1.0 1.0 1.0 1.0
		SIZE = 50.0 120.0
		ROT = 0.0
		FIRST_TEX = 0.0 0.0
		LAST_TEX = 1.0 1.0
		TEXTYPE = 54
		OFFSET = 0
		# ----------[フェードイン]---------- #
		FADE_IN
			# フェード開始時間
			STARTFADETIME = 0
			# 色指定
			FADE_COL
				BEGIN_COL = 1.0 1.0 1.0 0.0
				END_COL = 1.0 1.0 1.0 1.0
				MAXFRAME = 30
			END_FADE_COL
		END_FADE_IN
		# ----------[フェードアウト]---------- #
		FADE_OUT
			# フェード開始時間
			STARTFADETIME = 0
			# 色指定
			FADE_COL
				BEGIN_COL = 1.0 1.0 1.0 1.0
				END_COL = 1.0 1.0 1.0 0.0
				MAXFRAME = 30
			END_FADE_COL
		END_FADE_OUT
	END_SET
	#==============================================================================
	# [2]
	#==============================================================================
	SET PRESENTS
		# ----------[基本設定]---------- #
		POS = 177.0 673.0 0.0
		COL = 1.0 1.0 1.0 1.0
		SIZE = 250.0 50.0
		ROT = 0.0
		FIRST_TEX = 0.0 0.0
		LAST_TEX = 1.0 1.0
		TEXTYPE = 55
		OFFSET = 0
		# ----------[フェードイン]---------- #
		FADE_IN
			# フェード開始時間
			STARTFADETIME = 0
			# 色指定
			FADE_COL
				BEGIN_COL = 1.0 1.0 1.0 0.0
				END_COL = 1.0 1.0 1.0 1.0
				MAXFRAME = 30
			END_FADE_COL
		END_FADE_IN
		# ----------[フェードアウト]---------- #
		FADE_OUT
			# フェード開始時間
			STARTFADETIME = 0
			# 色指定
			FADE_COL
				BEGIN_COL = 1.0 1.0 1.0 1.0
				END_COL = 1.0 1.0 1.0 0.0
				MAXFRAME = 30
			END_FADE_COL
		END_FADE_OUT
	END_SET
END_SCRIPT
