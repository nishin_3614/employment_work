// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 3Dパーティクルの処理[particle.h]
// Author : Nishiyama koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// インクルードファイル
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "3Dparticle.h"
#include "3Deffect.h"
#include "2Deffect.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ファイル名
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#define FILENAME ("data/LOAD/ParticleInfo.txt")

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 静的メンバ変数の初期化
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vector<C3DParticle::PARTICLE_OFFSET*>	C3DParticle::m_ParticleOffset = {};	// Ui読み込み用変数
bool									C3DParticle::m_b2D = false;			// 2D描画

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
C3DParticle::C3DParticle() : CScene()
{
	m_nFlameCount = 0;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デストラクタ
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
C3DParticle::~C3DParticle()
{
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 初期化
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DParticle::Init(void)
{
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 終了
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DParticle::Uninit(void)
{

}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 更新
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DParticle::Update(void)
{
	// フレームカウントアップ
	m_nFlameCount++;
	if (m_nFlameCount > m_ParticleOffset[m_nParticleId]->nFrame)
	{
		m_nFlameCount = 0;
		Release();
	}
	else
	{
		// 変数宣言
		D3DXVECTOR3	move = D3DVECTOR3_ZERO;			// 移動量
		D3DXVECTOR3	pos = D3DVECTOR3_ZERO;			// 位置
		D3DXVECTOR3 rot = D3DVECTOR3_ZERO;			// 回転
		D3DXVECTOR2	size = D3DVECTOR2_ZERO;			// サイズ
		D3DXVECTOR2 sizeValue = D3DVECTOR2_ZERO;	// サイズ変化値
		D3DXCOLOR	col = D3DXCOLOR_INI;			// 色
		D3DXVECTOR3	posRand = D3DVECTOR3_ZERO;		// 位置ランダム
		int			nLife = 0;						// ライフ
		int			nColRand = 0;					// 色のランダム
		int			nRand = 0;						// ランダム値
		float		fSpeed = 0;						// 速度
		float		fAngle[3];						// 角度
		float		fAlphaDerease = 0.0f;			// α変化値
		fAngle[0] = 0;
		fAngle[1] = 0;
		fAngle[2] = 0;

		// 設定数ループ
		for (int nCntEffect = 0; nCntEffect < m_ParticleOffset[m_nParticleId]->nNumber; nCntEffect++)
		{
			/* ライフ設定 */
			nLife = m_ParticleOffset[m_nParticleId]->nLife;
			// ランダムのライフを設定
			if (m_ParticleOffset[m_nParticleId]->nLifeRand.nMax > 0)
			{
				nLife +=
					(rand() % m_ParticleOffset[m_nParticleId]->nLifeRand.nMax +
						m_ParticleOffset[m_nParticleId]->nLifeRand.nMin);
			}

			/* カラー設定 */
			col = m_ParticleOffset[m_nParticleId]->Col;
			if (m_ParticleOffset[m_nParticleId]->bRedRand)
			{
				// ランダム値取得
				nRand = (int)(col.r * 10);
				if (nRand > 0)
				{
					nColRand = rand() % nRand;
					col.r = (float)nColRand / 10;
				}
			}
			if (m_ParticleOffset[m_nParticleId]->bGreenRand)
			{
				// ランダム値取得
				nRand = (int)(col.g * 10);
				if (nRand > 0)
				{
					nColRand = rand() % nRand;
					col.g = (float)nColRand / 10;
				}
			}
			if (m_ParticleOffset[m_nParticleId]->bBlueRand)
			{
				// ランダム値取得
				nRand = (int)(col.b * 10);
				if (nRand > 0)
				{
					nColRand = rand() % nRand;
					col.b = (float)nColRand / 10;
				}
			}
			if (m_ParticleOffset[m_nParticleId]->bAlphaRand)
			{
				// ランダム値取得
				nRand = (int)(col.a * 10);
				if (nRand > 0)
				{
					nColRand = rand() % nRand;
					col.a = (float)nColRand / 10;
				}
			}
			if (m_ParticleOffset[m_nParticleId]->bAlphaDecrease)
			{
				// α値の減少
				fAlphaDerease = col.a / nLife;
			}
			/* 半径設定 */
			// 読み込んだサイズを代入
			size = m_ParticleOffset[m_nParticleId]->Size;
			// ランダムで設定した分加算
			if (m_ParticleOffset[m_nParticleId]->SizeXRand.nMax > 0)
			{
				size.x += (float)(rand() % m_ParticleOffset[m_nParticleId]->SizeXRand.nMax + m_ParticleOffset[m_nParticleId]->SizeXRand.nMin);
			}
			if (m_ParticleOffset[m_nParticleId]->SizeYRand.nMax > 0)
			{
				size.y += (float)(rand() % m_ParticleOffset[m_nParticleId]->SizeYRand.nMax + m_ParticleOffset[m_nParticleId]->SizeXRand.nMin);
			}
			// サイズ減少状態がtrueなら
			if (m_ParticleOffset[m_nParticleId]->bSizeDecrease)
			{
				// サイズ値の減少
				sizeValue = -size / (float)nLife;
			}
			// それ以外
			else
			{
				// サイズの変化値代入
				sizeValue.x = sizeValue.y =
					m_ParticleOffset[m_nParticleId]->fSizeChange;
			}
			/* 速度の設定 */
			fSpeed = m_ParticleOffset[m_nParticleId]->fSpeed;
			// ランダムのスピード設定
			if (m_ParticleOffset[m_nParticleId]->nSpeedRand.nMax > 0)
			{
				fSpeed +=
					(float)(rand() % m_ParticleOffset[m_nParticleId]->nSpeedRand.nMax +
						m_ParticleOffset[m_nParticleId]->nSpeedRand.nMin);
			}

			/* 角度の設定 */
			fAngle[0] = m_ParticleOffset[m_nParticleId]->Rot.x;
			fAngle[1] = m_ParticleOffset[m_nParticleId]->Rot.y;
			fAngle[2] = m_ParticleOffset[m_nParticleId]->Rot.z;
			// ランダムの角度設定
			if (m_ParticleOffset[m_nParticleId]->nAngleRand.nMax > 0)
			{
				fAngle[0] +=
					(rand() % m_ParticleOffset[m_nParticleId]->nAngleRand.nMax +
						m_ParticleOffset[m_nParticleId]->nAngleRand.nMin)*0.01f;
				fAngle[1] +=
					(rand() % m_ParticleOffset[m_nParticleId]->nAngleRand.nMax +
						m_ParticleOffset[m_nParticleId]->nAngleRand.nMin)*0.01f;
				fAngle[2] +=
					(rand() % m_ParticleOffset[m_nParticleId]->nAngleRand.nMax +
						m_ParticleOffset[m_nParticleId]->nAngleRand.nMin)*0.01f;
			}

			/* 位置設定 */
			// ランダムの角度設定
			if (m_ParticleOffset[m_nParticleId]->PosXRand.nMax > 0)
			{
				posRand.x = (float)(rand() % m_ParticleOffset[m_nParticleId]->PosXRand.nMax + m_ParticleOffset[m_nParticleId]->PosXRand.nMin);
			}
			if (m_ParticleOffset[m_nParticleId]->PosYRand.nMax > 0)
			{
				posRand.y = (float)(rand() % m_ParticleOffset[m_nParticleId]->PosYRand.nMax + m_ParticleOffset[m_nParticleId]->PosYRand.nMin);
			}
			if (m_ParticleOffset[m_nParticleId]->PosZRand.nMax > 0)
			{
				posRand.z = (float)(rand() % m_ParticleOffset[m_nParticleId]->PosZRand.nMax + m_ParticleOffset[m_nParticleId]->PosZRand.nMin);
			}

			// 位置
			pos =
			{
				posRand.x + m_ParticleOffset[m_nParticleId]->Pos.x,
				posRand.y + m_ParticleOffset[m_nParticleId]->Pos.y,
				posRand.z + m_ParticleOffset[m_nParticleId]->Pos.z
			};

			/* 移動量設定 */
			move =
			{
				sinf(fAngle[1])*sinf(fAngle[2])*fSpeed,
				cosf(fAngle[0])*cosf(fAngle[2])*fSpeed,
				cosf(fAngle[0])*sinf(fAngle[1])*fSpeed
			};

			// パーティクルタイプ
			if (m_ParticleOffset[m_nParticleId]->type == TYPE_BONFIRE)
			{
				// 位置によって移動量

			}
			// 2D描画状態なら
			if (m_b2D)
			{
				// 2Dエフェクト生成
				C2DEffect::Set2DEffect(
					(C2DEffect::EFFECT_TYPE)m_ParticleOffset[m_nParticleId]->nEffectType,
					m_ParticleOffset[m_nParticleId]->nEffectTexType,
					pos + m_Origin,
					D3DXVECTOR3(
						fAngle[0],
						fAngle[1],
						fAngle[2]
					),
					move,
					col,
					size,
					nLife,
					m_ParticleOffset[m_nParticleId]->Blend,
					sizeValue,
					fAlphaDerease
				);
			}
			// それ以外
			else
			{
				// 3Dエフェクト生成
				C3DEffect::Set3DEffect(
					(C3DEffect::EFFECT_TYPE)m_ParticleOffset[m_nParticleId]->nEffectType,
					m_ParticleOffset[m_nParticleId]->nEffectTexType,
					pos + m_Origin,
					D3DXVECTOR3(
						fAngle[0],
						fAngle[1],
						fAngle[2]
					),
					move,
					col,
					size,
					nLife,
					m_ParticleOffset[m_nParticleId]->Blend,
					sizeValue,
					fAlphaDerease
				);
			}
		}
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 描画
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DParticle::Draw(void)
{
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 生成
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
C3DParticle * C3DParticle::Create(
	int const &nOffsetId,
	D3DXVECTOR3 const origin)
{
	// 変数宣言
	C3DParticle *p3DParticle = new C3DParticle;
	// シーン管理設定
	p3DParticle->ManageSetting(CScene::LAYER_3DPARTICLE);
	// 設定
	p3DParticle->SetParticle(nOffsetId);
	p3DParticle->SetOrigin(origin);
	return p3DParticle;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ツール処理
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DParticle::Tool(
	int const &nOffsetId
)
{
	// 変数宣言
	int nBlendType = 0;					// ブレンドタイプ
	static char const *cEffectType[] =	// エフェクトタイプ
	{
		"EFFECT_TYPE_EXPLOSION",	// 爆発
		"EFFECT_TYPE_SPARK",		// 花火
		"EFFECT_TYPE_SMOKE",		// 煙
		"EFFECT_TYPE_LINE",			// 線
		"EFFECT_TYPE_BALLOON",		// 風船
		"EFFECT_TYPE_SHOCK",		// 衝撃
		"EFFECT_TYPE_BONFIRE_SPARK"	// 焚火の火の粉
	};
	static char const *cBlendType[] =	// ブレンドタイプ
	{
		"BLEND_NORMAL",				// ブレンド_通常
		"BLEND_TRANSLUCENT",			// ブレンド_半透明
		"BLEND_ADD",				// ブレンド_加算
		"BLEND_ADD_TRANSLUCENT",	// ブレンド_加算半透明
		"BLEND_SUBTRACTION",		// ブレンド_減算
		"BLEND_MULTIPUL1",			// ブレンド_乗算1
		"BLEND_MULTIPUL2",			// ブレンド_乗算2
		"BLEND_SCREEN",				// ブレンド_スクリーン
		"BLEND_REVERSE",			// ブレンド_リバース
	};
	// ファイル名を入力
	ImGui::InputText("ParticleName", m_ParticleOffset[nOffsetId]->cName, 64);
	// 位置情報
	if (ImGui::TreeNode("Pos"))
	{
		ImGui::DragFloat3("pos", m_ParticleOffset[nOffsetId]->Pos, 1.0f);
		// 位置ランダム
		if (ImGui::TreeNode("POSRAND"))
		{
			ImGui::DragInt("posX_Max", &m_ParticleOffset[nOffsetId]->PosXRand.nMax, 1);
			ImGui::DragInt("posX_Min", &m_ParticleOffset[nOffsetId]->PosXRand.nMin, 1);
			ImGui::DragInt("posY_Max", &m_ParticleOffset[nOffsetId]->PosYRand.nMax, 1);
			ImGui::DragInt("posY_Min", &m_ParticleOffset[nOffsetId]->PosYRand.nMin, 1);
			ImGui::DragInt("posZ_Max", &m_ParticleOffset[nOffsetId]->PosZRand.nMax, 1);
			ImGui::DragInt("posZ_Min", &m_ParticleOffset[nOffsetId]->PosZRand.nMin, 1);
			ImGui::TreePop();
		}
		ImGui::TreePop();
	}
	// 色情報
	if (ImGui::TreeNode("Col"))
	{
		// 変数宣言
		float Col[4] = {
			m_ParticleOffset[nOffsetId]->Col.r,
			m_ParticleOffset[nOffsetId]->Col.g,
			m_ParticleOffset[nOffsetId]->Col.b,
			m_ParticleOffset[nOffsetId]->Col.a
		};
		// 色情報設定
		ImGui::ColorEdit4("色情報", Col);
		// 変化した値を代入する
		m_ParticleOffset[nOffsetId]->Col = Col;
		// 色ランダム //
		// 赤ランダム
		ImGui::Checkbox("Col_r", &m_ParticleOffset[nOffsetId]->bRedRand);
		ImGui::SameLine();
		// 青ランダム
		ImGui::Checkbox("Col_g", &m_ParticleOffset[nOffsetId]->bGreenRand);
		ImGui::SameLine();
		// 緑ランダム
		ImGui::Checkbox("Col_b", &m_ParticleOffset[nOffsetId]->bBlueRand);
		ImGui::SameLine();
		// αランダム
		ImGui::Checkbox("Col_a", &m_ParticleOffset[nOffsetId]->bAlphaRand);
		// α値減少
		ImGui::Checkbox("a_Decrease", &m_ParticleOffset[nOffsetId]->bAlphaDecrease);
		ImGui::TreePop();
	}
	// サイズ情報
	if (ImGui::TreeNode("SIZE"))
	{
		ImGui::DragFloat2("size", m_ParticleOffset[nOffsetId]->Size, 1.0f,0.0f,1000.0f);
		// サイズランダムランダム
		if (ImGui::TreeNode("SIZERAND"))
		{
			ImGui::DragInt("sizeX_Max", &m_ParticleOffset[nOffsetId]->SizeXRand.nMax, 1, 0);
			ImGui::DragInt("sizeX_Min", &m_ParticleOffset[nOffsetId]->SizeXRand.nMin, 1, 0);
			ImGui::DragInt("sizeY_Max", &m_ParticleOffset[nOffsetId]->SizeYRand.nMax, 1, 0);
			ImGui::DragInt("sizeY_Min", &m_ParticleOffset[nOffsetId]->SizeYRand.nMin, 1, 0);
			ImGui::TreePop();
		}
		// サイズの減少状態
		ImGui::Checkbox("size_Decrease", &m_ParticleOffset[nOffsetId]->bSizeDecrease);
		// サイズの減少状態がfalseなら
		if (!m_ParticleOffset[nOffsetId]->bSizeDecrease)
		{
			// サイズの変化値
			ImGui::DragFloat("size_Change", &m_ParticleOffset[nOffsetId]->fSizeChange, 1.0f, 0.0f);

		}
		ImGui::TreePop();
	}
	// 回転情報
	if (ImGui::TreeNode("ROT"))
	{
		ImGui::DragFloat3("rot", m_ParticleOffset[nOffsetId]->Rot, 0.01f, -D3DX_PI, D3DX_PI);
		// 回転ランダム
		if (ImGui::TreeNode("RotRand"))
		{
			ImGui::DragInt("Rot_MAX", &m_ParticleOffset[nOffsetId]->nAngleRand.nMax, 1, -314, 628);
			ImGui::DragInt("Rot_MIN", &m_ParticleOffset[nOffsetId]->nAngleRand.nMin, 1, -314, 628);
			ImGui::TreePop();
		}
		ImGui::TreePop();
	}
	// スピード情報
	if (ImGui::TreeNode("SPEED"))
	{
		ImGui::DragFloat("Speed", &m_ParticleOffset[nOffsetId]->fSpeed, 0.1f, 0.0f, 100.0f);
		// スピードランダム
		if (ImGui::TreeNode("SPEEDRAND"))
		{
			ImGui::DragInt("Speed_MAX", &m_ParticleOffset[nOffsetId]->nSpeedRand.nMax, 1);
			ImGui::DragInt("Speed_MIN", &m_ParticleOffset[nOffsetId]->nSpeedRand.nMin, 1);
			ImGui::TreePop();
		}
		ImGui::TreePop();
	}
	// ライフ情報
	if (ImGui::TreeNode("LIFE"))
	{
		ImGui::DragInt("Life", &m_ParticleOffset[nOffsetId]->nLife, 1, 0, 1000);
		// ライフランダム
		if (ImGui::TreeNode("LIFERAND"))
		{
			ImGui::DragInt("Life_MAX", &m_ParticleOffset[nOffsetId]->nLifeRand.nMax, 1, 0);
			ImGui::DragInt("Life_MIN", &m_ParticleOffset[nOffsetId]->nLifeRand.nMin, 1, 0);
			ImGui::TreePop();
		}
		ImGui::TreePop();
	}
	// フレーム情報
	if (ImGui::TreeNode("FRAME"))
	{
		ImGui::InputInt("Frame", &m_ParticleOffset[nOffsetId]->nFrame);
		ImGui::TreePop();
	}
	// 1フレームに出現するパーティクル数
	if (ImGui::TreeNode("NUMBER"))
	{
		ImGui::InputInt("Number", &m_ParticleOffset[nOffsetId]->nNumber);
		ImGui::TreePop();
	}

	// テクスチャータイプ情報
	if (ImGui::TreeNode("TEXTYPE"))
	{
		ImGui::DragInt("TexType", &m_ParticleOffset[nOffsetId]->nEffectTexType, 1, 0, CTexture_manager::GetTextureMax() - 1);
		ImGui::TreePop();
	}


	// エフェクトタイプ情報
	if (ImGui::TreeNode("EFFECTTYPE"))
	{
		ImGui::ListBox("EffectType", &m_ParticleOffset[nOffsetId]->nEffectType, cEffectType, CEffect::EFFECT_TYPE_MAX);
		ImGui::TreePop();
	}

	//ImGui::ListBox("BLEND",&m_ParticleOffset[nOffsetId]->Blend,)
	// ブレンドタイプ情報
	if (ImGui::TreeNode("BLEND"))
	{
		// 変数宣言
		int nBlend = m_ParticleOffset[nOffsetId]->Blend;	// ブレンドタイプ
		ImGui::ListBox("Blend", &nBlend, cBlendType, CRenderer::BLEND_MAX - 1);
		m_ParticleOffset[nOffsetId]->Blend = (CRenderer::BLEND)nBlend;
		ImGui::TreePop();
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 新しいパーティクル情報生成
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DParticle::CreateParticle(void)
{
	// パーティクルオフセットの情報確保
	m_ParticleOffset.push_back(new PARTICLE_OFFSET);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// パーティクル情報の削除
// nParticleId:パーティクル番号
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DParticle::DeleteParticleOffset(int const & nParticleId)
{
	// 指定したパーティクル情報の開放
	delete m_ParticleOffset[nParticleId];
	m_ParticleOffset[nParticleId] = NULL;
	// 指定した配列番号の要素を削除
	m_ParticleOffset.erase(m_ParticleOffset.begin() + nParticleId);
	// パーティクル情報の領域をコンテナサイズまで切り詰める
	m_ParticleOffset.shrink_to_fit();
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 読み込み
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT C3DParticle::Load(void)
{
	// 変数宣言
	FILE *pFile = NULL;				// ファイルのポインタ
	char cRead[128];				// 読み込み用
	char cComp[128];				// 比較用
	char cEmpty[128];				// 要らないもの用
	char cName[64];					// パーティクル名
	int nCntError = 0;				// エラー用
	int nCntOffset = 0;				// オフセットのカウント
	int nId = 0;					// パーティクル番号
	// 一時保存用
	// ファイルが開かれていなかったら
	if ((pFile = fopen(FILENAME, "r")) == NULL)
	{
		CCalculation::Messanger("パーティクルcppのLOAD関数->ファイル名が存在しません。");
		return E_FAIL;
	}
	// スクリプトが読み込まれるまでループ
	while (strcmp(cComp, "SCRIPT") != 0)
	{
		// 1行読み込む
		fgets(cRead, sizeof(cRead), pFile);
		// 読み込んど文字列代入
		sscanf(cRead, "%s", &cComp);

		// エラーカウントをインクリメント
		nCntError++;
		if (nCntError > 1048576)
		{// エラー
			nCntError = 0;
			CCalculation::Messanger("パーティクルcppのLOAD関数->テキスト内に「SCRIPT」が存在しません。");
			return E_FAIL;
		}
	}
	// エンドスクリプトが読み込まれるまでループ
	while (strcmp(cComp, "END_SCRIPT") != 0)
	{// END_SCRIPTまでループ
	 // 1行読み込む
		fgets(cRead, sizeof(cRead), pFile);
		// 読み込んど文字列代入
		sscanf(cRead, "%s", &cComp);

		// エラーカウントをインクリメント
		nCntError++;
		if (nCntError > 1048576)
		{// エラー
			nCntError = 0;
			fclose(pFile);
			CCalculation::Messanger("パーティクルcppのLOAD関数->テキスト内に「END_SCRIPT」が存在しません。");
			return E_FAIL;
		}
		// # が読み込まれたら
		else if (strcmp(cComp, "#") == 0)
		{
			// 位置xの最大値と最小値を代入
			sscanf(cRead, "%s [%d] %s", &cEmpty,
				&nId,
				&cName
			);
		}
		else if (strcmp(cComp, "OFFSET") == 0)
		{
			// パーティクルオフセットの情報確保
			m_ParticleOffset.push_back(new PARTICLE_OFFSET);
			// 文字列を代入する
			strcat(m_ParticleOffset[nCntOffset]->cName,cName);
			// パーティクル名の初期化
			cName[0] = '\0';
			while (strcmp(cComp, "END_OFFSET") != 0)
			{
				// 1行読み込む
				fgets(cRead, sizeof(cRead), pFile);
				// 読み込んど文字列代入
				sscanf(cRead, "%s", &cComp);
				// セット位置が来たら
				// ->入る
				if (strcmp(cComp, "SET_POSRAND") == 0)
				{
					// エンドセット位置がくるまで
					// ->ループ
					while (strcmp(cComp, "ENDSET_POSRAND") != 0)
					{
						// 文字列の初期化
						cComp[0] = '\0';
						// 1行読み込む
						fgets(cRead, sizeof(cRead), pFile);
						// 読み込んど文字列代入
						sscanf(cRead, "%s", &cComp);
						// 位置xが読み込まれたら
						if (strcmp(cComp, "POSX") == 0)
						{
							// 位置xの最大値と最小値を代入
							sscanf(cRead, "%s %s %d %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->PosXRand.nMax,
								&m_ParticleOffset[nCntOffset]->PosXRand.nMin
							);
						}
						// 位置yが読み込まれたら
						else if (strcmp(cComp, "POSY") == 0)
						{
							// 位置yの最大値と最小値を代入
							sscanf(cRead, "%s %s %d %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->PosYRand.nMax,
								&m_ParticleOffset[nCntOffset]->PosYRand.nMin
							);
						}
						// 位置zが読み込まれたら
						else if (strcmp(cComp, "POSZ") == 0)
						{
							// 位置zの最大値と最小値を代入
							sscanf(cRead, "%s %s %d %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->PosZRand.nMax,
								&m_ParticleOffset[nCntOffset]->PosZRand.nMin
							);
						}
					}
				}
				// セット色が来たら
				// ->入る
				else if (strcmp(cComp, "SET_COLRAND") == 0)
				{
					// エンドセットサイズがくるまで
					// ->ループ
					while (strcmp(cComp, "ENDSET_COLRAND") != 0)
					{
						// 文字列の初期化
						cComp[0] = '\0';
						// 1行読み込む
						fgets(cRead, sizeof(cRead), pFile);
						// 読み込んど文字列代入
						sscanf(cRead, "%s", &cComp);
						// 赤
						if (strcmp(cComp, "RED") == 0)
						{
							m_ParticleOffset[nCntOffset]->bRedRand = true;
						}
						// 緑
						else if (strcmp(cComp, "GREEN") == 0)
						{
							m_ParticleOffset[nCntOffset]->bGreenRand = true;
						}
						// 青
						else if (strcmp(cComp, "BLUE") == 0)
						{
							m_ParticleOffset[nCntOffset]->bBlueRand = true;
						}
						// 透明度
						else if (strcmp(cComp, "ALPHA") == 0)
						{
							m_ParticleOffset[nCntOffset]->bAlphaRand = true;
						}
					}
				}
				// セットサイズが来たら
				// ->入る
				else if (strcmp(cComp, "SET_SIZERAND") == 0)
				{
					// エンドセットサイズがくるまで
					// ->ループ
					while (strcmp(cComp, "ENDSET_SIZERAND") != 0)
					{
						// 文字列の初期化
						cComp[0] = '\0';
						// 1行読み込む
						fgets(cRead, sizeof(cRead), pFile);
						// 読み込んど文字列代入
						sscanf(cRead, "%s", &cComp);
						// サイズx
						if (strcmp(cComp, "SIZEX") == 0)
						{
							sscanf(cRead, "%s %s %d %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->SizeXRand.nMax,
								&m_ParticleOffset[nCntOffset]->SizeXRand.nMin
							);
						}
						// サイズy
						else if (strcmp(cComp, "SIZEY") == 0)
						{
							sscanf(cRead, "%s %s %d %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->SizeYRand.nMax,
								&m_ParticleOffset[nCntOffset]->SizeYRand.nMin
							);
						}
					}
				}
				// セット角度が来たら
				// ->入る
				else if (strcmp(cComp, "SET_ANGLERAND") == 0)
				{
					// エンドセット角度がくるまで
					// ->ループ
					while (strcmp(cComp, "ENDSET_ANGLERAND") != 0)
					{
						// 文字列の初期化
						cComp[0] = '\0';
						// 1行読み込む
						fgets(cRead, sizeof(cRead), pFile);
						// 読み込んど文字列代入
						sscanf(cRead, "%s", &cComp);
						// 最大角度
						if (strcmp(cComp, "MAX") == 0)
						{
							sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->nAngleRand.nMax
							);
						}
						// 最小角度
						else if (strcmp(cComp, "MIN") == 0)
						{
							sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->nAngleRand.nMin
							);
						}
					}
				}
				// セットスピードが来たら
				// ->入る
				else if (strcmp(cComp, "SET_SPEEDRAND") == 0)
				{
					// エンドセットスピードが読み込まれるまで
					// ->ループ
					while (strcmp(cComp, "ENDSET_SPEEDRAND") != 0)
					{
						// 文字列の初期化
						cComp[0] = '\0';
						// 1行読み込む
						fgets(cRead, sizeof(cRead), pFile);
						// 読み込んど文字列代入
						sscanf(cRead, "%s", &cComp);
						// 最大スピードが読み込まれるまで
						if (strcmp(cComp, "MAX") == 0)
						{
							// スピードランダムの最大値を代入
							sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->nSpeedRand.nMax
							);
						}
						// 最小スピードが読み込まれるまで
						else if (strcmp(cComp, "MIN") == 0)
						{
							// スピードランダムの最小値を代入
							sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->nSpeedRand.nMin
							);
						}
					}
				}
				// セットライフランダムが来たら
				// ->入る
				else if (strcmp(cComp, "SET_LIFERAND") == 0)
				{
					// エンドセットライフランダムがくるまで
					// ->ループ
					while (strcmp(cComp, "ENDSET_LIFERAND") != 0)
					{
						// 文字列の初期化
						cComp[0] = '\0';
						// 1行読み込む
						fgets(cRead, sizeof(cRead), pFile);
						// 読み込んど文字列代入
						sscanf(cRead, "%s", &cComp);
						// 最大ライフ
						if (strcmp(cComp, "MAX") == 0)
						{
							sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->nLifeRand.nMax
							);
						}
						// 最小ライフ
						else if (strcmp(cComp, "MIN") == 0)
						{
							sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
								&m_ParticleOffset[nCntOffset]->nLifeRand.nMin
							);
						}
					}
				}
				// 位置が読み込まれたら
				else if (strcmp(cComp, "POS") == 0)
				{
					// 位置情報の代入
					sscanf(cRead, "%s %s %f %f %f", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->Pos.x,
						&m_ParticleOffset[nCntOffset]->Pos.y,
						&m_ParticleOffset[nCntOffset]->Pos.z);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// 回転が読み込まれたら
				else if (strcmp(cComp, "ROT") == 0)
				{
					// 回転情報の代入
					sscanf(cRead, "%s %s %f %f %f", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->Rot.x,
						&m_ParticleOffset[nCntOffset]->Rot.y,
						&m_ParticleOffset[nCntOffset]->Rot.z);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// 色が読み込まれたら
				else if (strcmp(cComp, "COL") == 0)
				{
					// 色情報の代入
					sscanf(cRead, "%s %s %f %f %f %f", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->Col.r,
						&m_ParticleOffset[nCntOffset]->Col.g,
						&m_ParticleOffset[nCntOffset]->Col.b,
						&m_ParticleOffset[nCntOffset]->Col.a);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// 色の減少状態が読み込まれたら
				else if (strcmp(cComp, "COL_DESCREASE") == 0)
				{
					// サイズの減少状態をtrueに
					m_ParticleOffset[nCntOffset]->bAlphaDecrease = true;
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// サイズが読み込まれたら
				else if (strcmp(cComp, "SIZE") == 0)
				{
					// サイズの代入
					sscanf(cRead, "%s %s %f %f",
						&cEmpty,
						&cEmpty,
						&m_ParticleOffset[nCntOffset]->Size.x,
						&m_ParticleOffset[nCntOffset]->Size.y
					);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// サイズの減少が読み込まれたら
				else if (strcmp(cComp, "SIZE_DESCREASE") == 0)
				{
					// サイズの減少状態をtrueに
					m_ParticleOffset[nCntOffset]->bSizeDecrease = true;
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// サイズの変化値が読み込まれたら
				else if (strcmp(cComp, "SIZE_CHANGE") == 0)
				{
					// サイズの変化値の代入
					sscanf(cRead, "%s %s %f",
						&cEmpty,
						&cEmpty,
						&m_ParticleOffset[nCntOffset]->fSizeChange
					);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// スピードが読み込まれたら
				else if (strcmp(cComp, "SPEED") == 0)
				{
					// スピードの代入
					sscanf(cRead, "%s %s %f", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->fSpeed);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// ライフが読み込まれたら
				else if (strcmp(cComp, "LIFE") == 0)
				{
					// ライフの代入
					sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->nLife);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// フレームが読み込まれたら
				else if (strcmp(cComp, "FRAME") == 0)
				{
					// フレームの代入
					sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->nFrame);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// 番号が読み込まれたら
				else if (strcmp(cComp, "NUMBER") == 0)
				{
					// 番号の代入
					sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->nNumber);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// テクスチャータイプが読み込まれたら
				else if (strcmp(cComp, "TEXTYPE") == 0)
				{
					// テクスチャータイプの代入
					sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->nEffectTexType);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// エフェクトタイプが読み込まれたら
				else if (strcmp(cComp, "EFFECTTYPE") == 0)
				{
					// エフェクトタイプの代入
					sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->nEffectType);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// パーティクルタイプが読み込まれたら
				else if (strcmp(cComp, "PARTICLETYPE") == 0)
				{
					// パーティクルタイプの代入
					sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->type);
					// 文字列の初期化
					cComp[0] = '\0';
				}
				// ブレンドタイプが読み込まれたら
				else if (strcmp(cComp, "BLEND") == 0)
				{
					// パーティクルタイプの代入
					sscanf(cRead, "%s %s %d", &cEmpty, &cEmpty,
						&m_ParticleOffset[nCntOffset]->Blend);
					// 文字列の初期化
					cComp[0] = '\0';
				}
			}
			// オフセットカウントを進める
			nCntOffset++;
		}
	}
	fclose(pFile);
	return S_OK;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 開放
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DParticle::Unload(void)
{
	// パーティクルオフセットの情報開放
	for (int nCntParticle = 0; nCntParticle < (signed)m_ParticleOffset.size(); nCntParticle++)
	{
		delete m_ParticleOffset[nCntParticle];
		m_ParticleOffset[nCntParticle] = NULL;
	}
	// パーティクルオフセットの配列情報を開放
	m_ParticleOffset.clear();
	m_ParticleOffset.shrink_to_fit();
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// パーティクルのすべての情報をセーブ
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HRESULT C3DParticle::Save(void)
{
	// 書き込みファイル変数
	std::ofstream writing_file;

	// ファイル名を開く
	writing_file.open(FILENAME, std::ios::out);
	// 開いていなかったらループスキップ
	if (!writing_file)
	{
#ifdef _DEBUG
		CCalculation::Messanger("パーティクルのSave関数->開けませんでした。");
#endif // _DEBUG
		return E_FAIL;
	}
	// メインコメントの追加
	writing_file << CCalculation::FileMainComment(
		"パーティクルエディター",
		FILENAME,
		"KOKI NISHIYAMA"
	);
	// スクリプトの追加
	writing_file << "SCRIPT\n\n";
	// パーティクル情報の書き込み
	for (int nCntParticle = 0; nCntParticle < (signed)m_ParticleOffset.size(); nCntParticle++)
	{
		// サブコメントの追加
		writing_file << CCalculation::FileSubComment(
			nCntParticle,
			m_ParticleOffset[nCntParticle]->cName,
			0
		);
		// パーティクルの初期タイプの追加 //
		// オフセット
		writing_file << "OFFSET\n";
		// ----------基本情報の追加---------- //
		{
			// 位置ランダムの最大値・最小値が共に0以外の場合
			// ->テキストに書き込み
			if (m_ParticleOffset[nCntParticle]->PosXRand.nMax != 0 ||
				m_ParticleOffset[nCntParticle]->PosXRand.nMin != 0 ||
				m_ParticleOffset[nCntParticle]->PosYRand.nMax != 0 ||
				m_ParticleOffset[nCntParticle]->PosYRand.nMin != 0 ||
				m_ParticleOffset[nCntParticle]->PosZRand.nMax != 0 ||
				m_ParticleOffset[nCntParticle]->PosZRand.nMin != 0
				)
			{
				// 位置ランダムコメント
				writing_file << CCalculation::FileNormalComment(
					"位置ランダム",
					1
				);
				// セット位置ランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tSET_POSRAND\n"
				);
				// 位置ランダム追加 //
				// X
				writing_file << CCalculation::FileInfoWriting(
					"\t\tPOSX = %d %d\n",
					m_ParticleOffset[nCntParticle]->PosXRand.nMax,
					m_ParticleOffset[nCntParticle]->PosXRand.nMin
				);
				// Y
				writing_file << CCalculation::FileInfoWriting(
					"\t\tPOSY = %d %d\n",
					m_ParticleOffset[nCntParticle]->PosYRand.nMax,
					m_ParticleOffset[nCntParticle]->PosYRand.nMin
				);
				// Z
				writing_file << CCalculation::FileInfoWriting(
					"\t\tPOSZ = %d %d\n",
					m_ParticleOffset[nCntParticle]->PosZRand.nMax,
					m_ParticleOffset[nCntParticle]->PosZRand.nMin
				);
				// エンドセット位置ランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tENDSET_POSRAND\n"
				);
			}
			// 色ランダム //
			// 色ランダムがtrueなら
			// ->テキストに書き込み
			if (m_ParticleOffset[nCntParticle]->bRedRand ||
				m_ParticleOffset[nCntParticle]->bGreenRand ||
				m_ParticleOffset[nCntParticle]->bBlueRand ||
				m_ParticleOffset[nCntParticle]->bAlphaRand
				)
			{
				// 色ランダムコメント
				writing_file << CCalculation::FileNormalComment(
					"色ランダム",
					1
				);
				// 色ランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tSET_COLRAND\n"
				);
				// 色ランダム追加 //
				// 赤
				if (m_ParticleOffset[nCntParticle]->bRedRand)
				{
					writing_file << CCalculation::FileInfoWriting(
						"\t\tRED\n"
					);
				}
				// 緑
				if (m_ParticleOffset[nCntParticle]->bGreenRand)
				{
					writing_file << CCalculation::FileInfoWriting(
						"\t\tGREEN\n"
					);
				}
				// 青
				if (m_ParticleOffset[nCntParticle]->bBlueRand)
				{
					writing_file << CCalculation::FileInfoWriting(
						"\t\tBLUE\n"
					);
				}
				// 透明度
				if (m_ParticleOffset[nCntParticle]->bAlphaRand)
				{
					writing_file << CCalculation::FileInfoWriting(
						"\t\tALPHA\n"
					);
				}
				// エンド色ランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tENDSET_COLRAND\n"
				);
			}

			// サイズランダム //
			// サイズランダムの最大値・最小値が共に0以外の場合
			// ->テキストに書き込み
			if (m_ParticleOffset[nCntParticle]->SizeXRand.nMax != 0 ||
				m_ParticleOffset[nCntParticle]->SizeXRand.nMin != 0 ||
				m_ParticleOffset[nCntParticle]->SizeYRand.nMax != 0 ||
				m_ParticleOffset[nCntParticle]->SizeYRand.nMin != 0
				)
			{
				// サイズランダムコメント
				writing_file << CCalculation::FileNormalComment(
					"サイズランダム",
					1
				);
				// サイズランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tSET_SIZERAND\n"
				);
				// サイズランダム追加 //
				// X
				writing_file << CCalculation::FileInfoWriting(
					"\t\tSIZEX = %d %d\n",
					m_ParticleOffset[nCntParticle]->SizeXRand.nMax,
					m_ParticleOffset[nCntParticle]->SizeXRand.nMin
				);
				// Y
				writing_file << CCalculation::FileInfoWriting(
					"\t\tSIZEY = %d %d\n",
					m_ParticleOffset[nCntParticle]->SizeYRand.nMax,
					m_ParticleOffset[nCntParticle]->SizeYRand.nMin
				);
				// サイズランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tENDSET_SIZERAND\n"
				);
			}
			// 角度ランダム //
			// 角度ランダムの最大値・最小値が共に0以外の場合
			// ->テキストに書き込み
			if (m_ParticleOffset[nCntParticle]->nAngleRand.nMax != 0 ||
				m_ParticleOffset[nCntParticle]->nAngleRand.nMin != 0
				)
			{
				// 角度ランダムコメント
				writing_file << CCalculation::FileNormalComment(
					"角度ランダム",
					1
				);
				// 角度ランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tSET_ANGLERAND\n"
				);
				// 角度ランダム追加 //
				// MAX
				writing_file << CCalculation::FileInfoWriting(
					"\t\tMAX = %d\n",
					m_ParticleOffset[nCntParticle]->nAngleRand.nMax
				);
				// MIN
				writing_file << CCalculation::FileInfoWriting(
					"\t\tMIN = %d\n",
					m_ParticleOffset[nCntParticle]->nAngleRand.nMin
				);
				// 角度ランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tENDSET_ANGLERAND\n"
				);
			}
			// スピードランダム //
			// スピードランダムの最大値・最小値が共に0以外の場合
			// ->テキストに書き込み
			if (m_ParticleOffset[nCntParticle]->nSpeedRand.nMax != 0 ||
				m_ParticleOffset[nCntParticle]->nSpeedRand.nMin != 0
				)
			{
				// スピードランダムコメント
				writing_file << CCalculation::FileNormalComment(
					"スピードランダム",
					1
				);
				// スピードランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tSET_SPEEDRAND\n"
				);
				// スピードランダム追加 //
				// MAX
				writing_file << CCalculation::FileInfoWriting(
					"\t\tMAX = %d\n",
					m_ParticleOffset[nCntParticle]->nSpeedRand.nMax
				);
				// MIN
				writing_file << CCalculation::FileInfoWriting(
					"\t\tMIN = %d\n",
					m_ParticleOffset[nCntParticle]->nSpeedRand.nMin
				);
				// スピードランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tENDSET_SPEEDRAND\n"
				);
			}
			// ライフランダム //
			// ライフランダムの最大値・最小値が共に0以外の場合
			// ->テキストに書き込み
			if (m_ParticleOffset[nCntParticle]->nLifeRand.nMax != 0 ||
				m_ParticleOffset[nCntParticle]->nLifeRand.nMin != 0
				)
			{
				// ライフランダムコメント
				writing_file << CCalculation::FileNormalComment(
					"ライフランダム",
					1
				);
				// ライフランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tSET_LIFERAND\n"
				);
				// ライフランダム追加 //
				// MAX
				writing_file << CCalculation::FileInfoWriting(
					"\t\tMAX = %d\n",
					m_ParticleOffset[nCntParticle]->nLifeRand.nMax
				);
				// MIN
				writing_file << CCalculation::FileInfoWriting(
					"\t\tMIN = %d\n",
					m_ParticleOffset[nCntParticle]->nLifeRand.nMin
				);
				// ライフランダム追加
				writing_file << CCalculation::FileInfoWriting(
					"\tENDSET_LIFERAND\n"
				);
			}
			// 位置コメント
			writing_file << CCalculation::FileNormalComment(
				"位置情報",
				1
			);
			// 位置
			writing_file << CCalculation::FileInfoWriting(
				"\tPOS = %.1f %.1f %.1f\n",
				m_ParticleOffset[nCntParticle]->Pos.x,
				m_ParticleOffset[nCntParticle]->Pos.y,
				m_ParticleOffset[nCntParticle]->Pos.z
			);

			// 色コメント
			writing_file << CCalculation::FileNormalComment(
				"色情報",
				1
			);
			// 色
			writing_file << CCalculation::FileInfoWriting(
				"\tCOL = %.1f %.1f %.1f %.1f\n",
				m_ParticleOffset[nCntParticle]->Col.r,
				m_ParticleOffset[nCntParticle]->Col.g,
				m_ParticleOffset[nCntParticle]->Col.b,
				m_ParticleOffset[nCntParticle]->Col.a
			);
			// 色の減少状態がtrueだったら
			if (m_ParticleOffset[nCntParticle]->bAlphaDecrease)
			{
				// 色
				writing_file << CCalculation::FileInfoWriting(
					"\tCOL_DESCREASE"
				);
			}
			// サイズコメント
			writing_file << CCalculation::FileNormalComment(
				"サイズ情報",
				1
			);
			// サイズ
			writing_file << CCalculation::FileInfoWriting(
				"\tSIZE = %.1f %.1f\n",
				m_ParticleOffset[nCntParticle]->Size.x,
				m_ParticleOffset[nCntParticle]->Size.y
			);
			// サイズの減少状態がtrueだったら
			if (m_ParticleOffset[nCntParticle]->bSizeDecrease)
			{
				// 色
				writing_file << CCalculation::FileInfoWriting(
					"\tSIZE_DESCREASE"
				);
			}
			// それ以外
			else
			{
				// サイズの変化値
				writing_file << CCalculation::FileInfoWriting(
					"\tSIZE_CHANGE = %.1f\n",
					m_ParticleOffset[nCntParticle]->fSizeChange
				);
			}
			// 回転コメント
			writing_file << CCalculation::FileNormalComment(
				"回転情報",
				1
			);
			// 回転
			writing_file << CCalculation::FileInfoWriting(
				"\tROT = %.1f %.1f %.1f\n",
				m_ParticleOffset[nCntParticle]->Rot.x,
				m_ParticleOffset[nCntParticle]->Rot.y,
				m_ParticleOffset[nCntParticle]->Rot.z
			);
			// スピードコメント
			writing_file << CCalculation::FileNormalComment(
				"スピード情報",
				1
			);
			// スピード
			writing_file << CCalculation::FileInfoWriting(
				"\tSPEED = %.1f\n",
				m_ParticleOffset[nCntParticle]->fSpeed
			);
			// ライフコメント
			writing_file << CCalculation::FileNormalComment(
				"ライフ情報",
				1
			);
			// ライフ
			writing_file << CCalculation::FileInfoWriting(
				"\tLIFE = %d\n",
				m_ParticleOffset[nCntParticle]->nLife
			);
			// フレームコメント
			writing_file << CCalculation::FileNormalComment(
				"フレーム情報",
				1
			);
			// フレーム
			writing_file << CCalculation::FileInfoWriting(
				"\tFRAME = %d\n",
				m_ParticleOffset[nCntParticle]->nFrame
			);
			// 一度の出せる個数コメント
			writing_file << CCalculation::FileNormalComment(
				"一度に出せる個数情報",
				1
			);
			// 一度に出せる個数
			writing_file << CCalculation::FileInfoWriting(
				"\tNUMBER = %d\n",
				m_ParticleOffset[nCntParticle]->nNumber
			);
			// テクスチャータイプコメント
			writing_file << CCalculation::FileNormalComment(
				"テクスチャータイプ情報",
				1
			);
			// テクスチャータイプ
			writing_file << CCalculation::FileInfoWriting(
				"\tTEXTYPE = %d\n",
				m_ParticleOffset[nCntParticle]->nEffectTexType
			);
			// エフェクトタイプコメント
			writing_file << CCalculation::FileNormalComment(
				"エフェクトタイプ情報",
				1
			);
			// エフェクトタイプ
			writing_file << CCalculation::FileInfoWriting(
				"\tEFFECTTYPE = %d\n",
				m_ParticleOffset[nCntParticle]->nEffectType
			);
			// パーティクルタイプコメント
			writing_file << CCalculation::FileNormalComment(
				"パーティクルタイプ情報",
				1
			);
			// パーティクルタイプ
			writing_file << CCalculation::FileInfoWriting(
				"\tPARTICLETYPE = %d\n",
				m_ParticleOffset[nCntParticle]->type
			);
			// ブレンドタイプコメント
			writing_file << CCalculation::FileNormalComment(
				"ブレンドタイプ情報",
				1
			);
			// ブレンドタイプ
			writing_file << CCalculation::FileInfoWriting(
				"\tBLEND = %d\n",
				m_ParticleOffset[nCntParticle]->Blend
			);
		}
		// エンドオフセット
		writing_file << "END_OFFSET\n";
	}
	// スクリプトの追加
	writing_file << "END_SCRIPT\n";
	writing_file.close();
	return S_OK;
}

#ifdef _DEBUG
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// デバッグ表示
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void C3DParticle::Debug(void)
{
}
#endif // _DEBUG