// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// SphereCollision処理の説明[spherecollision.cpp]
// Author : Koki Nishiyama
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// インクルードファイル
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "SphereCollision.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// マクロ定義
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// グローバル変数
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 静的変数
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// コンストラクタ
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CSphereCollision::CSphereCollision() : CCollision::CCollision()
{
	// 初期化
}


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 当たり判定処理(矩形と矩形)
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
bool CSphereCollision::Judg(CRectShape * const RectShape)
{
	return RectAndSphere(RectShape, this->m_pSphereShape.get());
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 当たり判定処理(矩形と球)
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
bool CSphereCollision::Judg(CSphereShape * const SphereShape)
{
	return SphereAndSphere(this->m_pSphereShape.get(), SphereShape);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 当たり判定処理(球と円柱)
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
bool CSphereCollision::Judg(CColumnShape * const ColumnShape)
{
	return SphereAndColumn(this->m_pSphereShape.get(), ColumnShape);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 作成処理(シーン管理)
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CSphereCollision *CSphereCollision::Create(
	D3DXVECTOR3 const offset,
	D3DXVECTOR3 const &pos,
	float const &fRadius
)
{
	// 変数宣言
	CSphereCollision *pSphereCollision;
	// メモリ確保
	pSphereCollision = new CSphereCollision();
	// 球の設定
	pSphereCollision->m_pSphereShape = CSphereShape::Create(offset,pos,fRadius);
	pSphereCollision->m_pSphereShape->SetPos(pos);
	// シーン管理設定
	pSphereCollision->ManageSetting(CScene::LAYER_COLLISION);
	return pSphereCollision;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 作成処理(個人管理)
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
unique_ptr<CSphereCollision> CSphereCollision::Create_Self(
	D3DXVECTOR3 const offset,
	D3DXVECTOR3 const &pos,
	float const &fRadius
)
{
	// 変数宣言
	unique_ptr<CSphereCollision> pSphereCollision(new CSphereCollision);
	// 球の設定
	pSphereCollision->m_pSphereShape = CSphereShape::Create(offset,pos,fRadius);
	pSphereCollision->m_pSphereShape->SetPos(pos);
	return pSphereCollision;
}
