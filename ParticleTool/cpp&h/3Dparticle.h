// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// 3Dパーティクルの処理[particle.h]
// Author : Nishiyama koki
//
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef _3DPARTICLE_H_
#define _3DPARTICLE_H_

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// インクルードファイル
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "Scene.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// マクロ定義
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// クラス定義
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class C3DParticle :public CScene
{
public:
	/* 列挙型 */
	// エフェクトタイプ
	typedef enum
	{
		TYPE_NORMAL = 0,
		TYPE_CHARGE,
		TYPE_BONFIRE,
		TYPE_MAX,
	} TYPE;
	/* 構造体 */
	typedef struct PARTICLE_OFFSET
	{
		// コンストラクタ
		PARTICLE_OFFSET()
		{
			nFrame = 60;							// フレーム時間
			nNumber = 10;							// 出現個数
			nEffectTexType = 0;						// エフェクトのテクスチャの種類
			nEffectType = 0;							// エフェクトの種類
			type = TYPE_NORMAL;						// 種類(主に移動の)
			/* 位置情報 */
			Pos = D3DVECTOR3_ZERO;					// 位置
			PosXRand = INTEGER2(0,0);				// 位置X座標ランダム用
			PosYRand = INTEGER2(0, 0);				// 位置Y座標ランダム用
			PosZRand = INTEGER2(0, 0);				// 位置Y座標ランダム用
			/* カラー情報 */
			Col = D3DXCOLOR_INI;					// 色
			bRedRand = false;						// 赤ランダム
			bGreenRand = false;						// 緑ランダム
			bBlueRand = false;						// 青ランダム
			bAlphaRand = false;						// 透明度ランダム
			bAlphaDecrease = false;					// 透明度減少状態
			/* サイズ情報 */
			Size = D3DXVECTOR2(50.0f,50.0f);		// サイズ
			SizeXRand = INTEGER2(0, 0);				// サイズxランダム用
			SizeYRand = INTEGER2(0, 0);				// サイズyランダム用
			fSizeChange = 0.0f;						// サイズ変化値
			bSizeDecrease = false;					// サイズの減少状態
			/* 角度情報 */
			Rot = D3DVECTOR3_ZERO;					// 回転ー
			nAngleRand = INTEGER2(0, 0);			// 角度のランダム用
			/* 速度情報 */
			fSpeed = 1.0f;							// 速度
			nSpeedRand = INTEGER2(0, 0);			// 速度のランダム用
			/* ライフ情報 */
			nLife = 10;								// ライフ
			nLifeRand = INTEGER2(0, 0);				// ライフのランダム用
			/* ブレンドタイプ */
			Blend = CRenderer::BLEND_TRANSLUCENT;	// ブレンドタイプ
			/* 名前記入 */
			cName[0] = '\0';
		}
		// 変数宣言
		int					nFrame;			// フレーム時間
		int					nNumber;		// 出現個数
		int					nEffectTexType;	// エフェクトのテクスチャの種類
		int					nEffectType;	// エフェクトの種類
		C3DParticle::TYPE	type;			// 種類(主に移動の)
		/* 位置情報 */
		D3DXVECTOR3			Pos;			// 位置
		INTEGER2			PosXRand;		// 位置X座標ランダム用
		INTEGER2			PosYRand;		// 位置Y座標ランダム用
		INTEGER2			PosZRand;		// 位置Y座標ランダム用
		/* カラー情報 */
		D3DXCOLOR			Col;			// 色
		bool				bRedRand;		// 赤ランダム
		bool				bGreenRand;		// 緑ランダム
		bool				bBlueRand;		// 青ランダム
		bool				bAlphaRand;		// 透明度ランダム
		bool				bAlphaDecrease;	// 透明度減少状態
		/* サイズ情報 */
		D3DXVECTOR2			Size;			// サイズ
		INTEGER2			SizeXRand;		// サイズxランダム用
		INTEGER2			SizeYRand;		// サイズyランダム用
		float				fSizeChange;	// サイズ変化値
		bool				bSizeDecrease;	// サイズの減少状態
		/* 角度情報 */
		D3DXVECTOR3			Rot;			// 回転
		INTEGER2			nAngleRand;		// 角度のランダム用
		float				fAngleChange;	// 角度の変化値
		/* 速度情報 */
		float				fSpeed;			// 速度
		INTEGER2			nSpeedRand;		// 速度のランダム用
		/* ライフ情報 */
		int					nLife;			// ライフ
		INTEGER2			nLifeRand;		// ライフのランダム用
		/* ブレンドタイプ */
		CRenderer::BLEND	Blend;			// ブレンドタイプ
		/* 名前記入 */
		char				cName[64];		// パーティクル名
	} PARTICLE_OFFSET;

	/* 関数 */
	// コンストラクタ
	C3DParticle();
	// デストラクタ
	~C3DParticle();
	// 初期化処理
	void Init(void);
	// 終了処理
	void Uninit(void);
	// 更新処理
	void Update(void);
	// 描画処理
	void Draw(void);
#ifdef _DEBUG
	// デバッグ処理
	void Debug(void);
#endif // _DEBUG
	// パーティクルの生成
	//	nParticleId	: パーティクル番号
	//	origin		: 初期位置
	static C3DParticle * Create(
		int const &nParticleId,
		D3DXVECTOR3 const origin
	);
	// 全リソース情報の読み込み
	static HRESULT Load(void);
	// 全リソース情報の解放
	static void Unload(void);
	// パーティクル情報の全リソースセーブ
	static HRESULT Save(void);
	// パーティクル情報の要素の格納数取得
	static int GetParticleMax(void) { return (signed)m_ParticleOffset.size(); };
	// ツール処理
	static void Tool(int const &nOffsetId);
	// 新しいパーティクル情報の生成
	static void CreateParticle(void);
	// パーティクル情報の削除
	// nParticleId:パーティクル番号
	static void DeleteParticleOffset(
		int const &nParticleId
	);
	// 2D描画状態設定
	static void Set2D(bool const &b2D) { m_b2D = b2D; };
	// 2D描画状態取得
	static bool &Get2D(void) { return m_b2D; };
	// パーティクルの番号設定
	void SetParticle(int const &nOffsetId) { m_nParticleId = nOffsetId; };
	// 初期値設定
	void SetOrigin(CONST D3DXVECTOR3 &Origin) { m_Origin = Origin; };
private:
	/* 関数 */

	/* 変数 */
	static std::vector<PARTICLE_OFFSET*>	m_ParticleOffset;	// パーティクルのオフセット
	static bool								m_b2D;				// 2D描画状態
	int										m_nParticleId;		// パーティクル番号
	int										m_nFlameCount;		// フレームのカウント
	D3DXVECTOR3								m_Origin;			// 原点
};

#endif
